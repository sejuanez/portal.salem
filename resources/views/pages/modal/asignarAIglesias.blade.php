<div class="modal fade" id="modal-pastor" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="accion-modal">Asignar</span>
                    <span class="fw-light">Pastor</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if($iglesia->pastor!=null)
                    <h2>
                        Pastor actual:
                        <strong>
                            {{$iglesia->pastor==null?'Ninguno':$iglesia->pastor->persona->nombres.' '.$iglesia->pastor->persona->apellidos}}
                        </strong>
                    </h2>
                @endif

                <form id="pastor-iglesia-form">
                    @csrf
                    <input type="hidden" name='id' value="{{$iglesia->id}}">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="pastor_id">Pastor</label>
                                <select class="form-control" name="select_pastor" id="select-pastor"
                                        style="width: 100%">
                                    <option></option>
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger text-danger" id="error_pastor" style="display: none">
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="asignarPastor()" class="btn btn-primary">Asignar</button>
                @if($iglesia->pastor!=null)
                    <button type="button" onclick="eliminarPastor()" class="btn btn-warning">Eliminar</button>
                @endif
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-colaborador" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="accion-modal">Asignar</span>
                    <span class="fw-light">Colaborador</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if($iglesia->colaborador!=null)
                    <h2>
                        Colaborador actual:
                        <strong>
                            {{$iglesia->colaborador==null?'Ninguno':$iglesia->colaborador->persona->nombres.' '.$iglesia->colaborador->persona->apellidos}}
                        </strong>
                    </h2>
                @endif
                <form id="colaborador-iglesia-form">
                    @csrf
                    <input type="hidden" name='id' value="{{$iglesia->id}}">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="select-colaborador">Colaborador</label>
                                <select class="form-control" name="select_colaborador" id="select-colaborador"
                                        style="width: 100%">
                                    <option></option>

                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger text-danger" id="error_colaborador" style="display: none">
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="asignarColaborador()" class="btn btn-primary">Asignar</button>
                @if($iglesia->colaborador!=null)
                    <button type="button" onclick="eliminarColaborador()" class="btn btn-warning">Eliminar</button>
                @endif
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-coordinador" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="accion-modal">Asignar</span>
                    <span class="fw-light">Coordinador</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if($iglesia->coordinador!=null)
                    <h2>
                        Coordinador actual:
                        <strong>
                            {{$iglesia->coordinador==null?'Ninguno':$iglesia->coordinador->persona->nombres.' '.$iglesia->coordinador->persona->apellidos}}
                        </strong>
                    </h2>
                @endif

                <form id="coordinador-iglesia-form">
                    @csrf
                    <input type="hidden" name='id' value="{{$iglesia->id}}">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="select-coordinador">Coordinador</label>
                                <select class="form-control" name="select_coordinador" id="select-coordinador"
                                        style="width: 100%">
                                    <option></option>

                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger text-danger" id="error_coordinador" style="display: none">
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="asignarCoordinador()" class="btn btn-primary">Asignar</button>
                @if($iglesia->coordinador!=null)
                    <button type="button" onclick="eliminarCoordinador()" class="btn btn-warning">Eliminar</button>
                @endif
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

