<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Reporte Salem</title>
    <style>
        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #5D6975;
            text-decoration: underline;
        }

        body {
            position: relative;
            width: 21cm;
            height: 29.7cm;
            margin: 0 auto;
            color: #001028;
            background: #FFFFFF;
            font-family: Arial, sans-serif;
            font-size: 12px;
            font-family: Arial;
            width: 100%;
        }

        header {
            padding: 10px 0;
            margin-bottom: 30px;
        }

        #logo {
            text-align: center;
            margin-bottom: 10px;
        }

        #logo img {
            width: 150px;
        }

        h1 {
            border-top: 1px solid #5D6975;
            border-bottom: 1px solid #5D6975;
            color: #5D6975;
            font-size: 2.4em;
            line-height: 1.4em;
            font-weight: normal;
            text-align: center;
            margin: 0 0 20px 0;
            background: url(dimension.png);
        }

        #project {
            float: left;
        }

        #project span {
            color: #5D6975;
            text-align: right;
            width: 52px;
            margin-right: 10px;
            display: inline-block;
            font-size: 0.8em;
        }

        #company {
            float: right;
            text-align: right;
        }

        #project div,
        #company div {
            white-space: nowrap;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px;
        }

        table tr:nth-child(2n-1) td {
            background: #F5F5F5;
        }

        table th,
        table td {
            text-align: center;
        }

        table th {
            padding: 5px 20px;
            color: #5D6975;
            border-bottom: 1px solid #C1CED9;
            white-space: nowrap;
            font-weight: normal;
        }

        table .service,
        table .desc {
            text-align: left;
        }

        table td {
            padding: 20px;
            text-align: right;
        }

        table td.service,
        table td.desc {
            vertical-align: top;
        }

        table td.unit,
        table td.qty,
        table td.total {
            font-size: 1.2em;
        }

        table td.grand {
            border-top: 1px solid #5D6975;;
        }

        #notices .notice {
            color: #5D6975;
            font-size: 1.2em;
        }

        footer {
            color: #5D6975;
            width: 100%;
            height: 30px;
            position: absolute;
            bottom: 0;
            border-top: 1px solid #C1CED9;
            padding: 8px 0;
            text-align: center;
        }
    </style>
</head>
<body>
<header class="clearfix">
    <div id="logo">
        <img
            src="https://www.ministerioapostolicosalem.com/wp-content/uploads/2019/10/cropped-LOGO-SALEM_2018-1-245x91.png">
    </div>
    <h1>Asistencia E Ingresos</h1>
    <div id="project">
        <div><span>IGLESIA</span> {{$iglesias->nombre}}</div>
        <div><span>FECHA</span> {{$fecha}}</div>
    </div>
</header>
@php
    $numeroAdultos=0;
    $numeroNinos=0;
    foreach($reuniones as $reunion){
        $numeroAdultos+=$reunion->num_adultos;
        $numeroNinos+=$reunion->num_ninios;
    }
@endphp
<main>
    <table>
        <thead>
        <tr>
            <th class="service">REUNIONES REALIZADAS</th>
            <th class="desc">ASISTENCIA MAYORES</th>
            <th class="desc">ASISTENCIA MENORES</th>
            <th>INGRESOS</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="service">{{$reuniones->count()}}</td>
            <td class="desc">{{round($reuniones->count()==0?0:$numeroAdultos/$reuniones->count())}}</td>
            <td class="desc">{{round($reuniones->count()==0?0:$numeroNinos/$reuniones->count())}}</td>
            <td class="qty">${{number_format($ingresos->sum('diezmo')+$ingresos->sum('ofrenda'))}}</td>
        </tr>
        </tbody>
    </table>
    <div id="notices">
        <div>NOTA:</div>
        <div class="notice">la asistencia es calculada mediante el promedio de personas por reuniones realizadas numero
            de reunones.
        </div>
    </div>
</main>
<footer>
    Reporte generado en el {{date("d")}} del {{date("m")}} de {{date("Y")}} a través de la plataforma SALEM.
</footer>
</body>
</html>
