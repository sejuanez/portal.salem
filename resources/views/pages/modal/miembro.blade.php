<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="accion-modal">Nuevo</span>
                    <span class="fw-light">Siervos</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-miembro">
                    @csrf
                    <div class="row">
                        <input type="hidden" id="id" name="id">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name" class="text-right">
                                    Nombres
                                    <span class="required-label">*</span>
                                </label>
                                <input type="text" class="form-control" id="nombres" name="nombres"
                                       placeholder="Nombres" required="">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="apellidos" class="text-right">
                                    Apellidos
                                    <span class="required-label">*</span>
                                </label>
                                <input type="text" class="form-control" id="apellidos" name="apellidos"
                                       placeholder="Apellidos" required="">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="apellidos" class="text-right">
                                    Telefono
                                    <span class="required-label">*</span>
                                </label>
                                <input type="number" class="form-control" id="telefono" name="telefono"
                                       placeholder="Telefono" required="">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="alert alert-danger text-danger" id="error" style="display: none">
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="guardar" class="btn btn-primary">Guardar</button>
                <button type="button" id="actualizar" class="btn btn-primary">Actualizar</button>
                <button type="button" id="eliminar" class="btn btn-primary">Eliminar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-agregarMiembro" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="accion-modal">Asignar</span>
                    <span class="fw-light">Siervos</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form id="miembro-iglesia-form">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="miembro_id">Siervos</label>
                                <select class="form-control" name="select_miembro" id="select-miembro"
                                        style="width: 100%">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="select-tipo">Rol</label>
                                <select class="form-control" id="rol" name="rol">
                                    @foreach($roles as $rol)
                                        @if($rol->name!="Colaborador")
                                            <option value="{{$rol->name}}">{{$rol->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger text-danger" id="error_miembro" style="display: none">
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="asignarMiembro" onclick="asignarmiembro()" class="btn btn-primary">Agregar
                </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

