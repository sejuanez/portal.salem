<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Iglesias extends Model
{
    //
    use Notifiable;

    protected $fillable = [
        'nombre',
        'direccion',
        'barrio',
        'city_id',
        'tipo',
        'latitud',
        'longitud',
        'colaborador_id',
        'coordinador_id',
        'pastor_id',
        'id_old_iglesia',
    ];

    public function pastor()
    {
        return $this->belongsTo(User::class, 'pastor_id');
    }

    public function colaborador()
    {
        return $this->belongsTo(User::class, 'colaborador_id');
    }

    public function coordinador()
    {
        return $this->belongsTo(User::class, 'coordinador_id');
    }

    public function miembros()
    {
        return $this->hasMany(Miembros::class, 'iglesia_id');
    }

    public function ciudad()
    {
        return $this->belongsTo(Cities::class, 'city_id');
    }

    public function reuniones()
    {
        return $this->hasMany(Reuniones::class, 'iglesia_id');
    }

    public function ingresos()
    {
        return $this->hasMany(Ofrendasydiezmos::class, 'iglesia_id');
    }

}
