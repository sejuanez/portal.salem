@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="page-inner">

            <div class="page-header">
                <h4 class="page-title">Perfil: </h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="/iglesias">Iglesias</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        {{$iglesia->nombre}}
                    </li>
                </ul>
            </div>

            <div class="row">
                <div class="col-md-9">
                    <div class="card card-with-nav">
                        <div class="card-header">
                            <div class="row row-nav-line">
                                <ul class="nav nav-tabs nav-line nav-color-secondary" role="tabpanel">
                                    <li class="nav-item submenu">
                                        <a class="nav-link active show" data-toggle="tab" href="#home-tab" id="info-tab"
                                           role="tab" aria-selected="true">Informacion</a>
                                    </li>
                                    @can('ver.miembros')
                                        <li class="nav-item submenu">
                                            <a class="nav-link" data-toggle="tab" href="#profile-tab" role="tab"
                                               aria-selected="false">Miembros</a>
                                        </li>
                                    @endcan

                                    @can('ver.ofrendasYDiezmos')
                                        <li class="nav-item submenu">
                                            <a class="nav-link" data-toggle="tab" href="#image-tab" role="tab"
                                               aria-selected="false">Ofrendas</a>
                                        </li>
                                    @endcan

                                    @can('ver.reuniones')
                                        <li class="nav-item submenu">
                                            <a class="nav-link" data-toggle="tab" href="#reuniones-tab" role="tab"
                                               aria-selected="false">Reuniones</a>
                                        </li>
                                    @endcan
                                </ul>
                            </div>
                        </div>

                        <div class="card-body">

                            <div class="tab-content mt-2 mb-3" id="pills-tabContent">

                                <div class="tab-pane fade active show" id="home-tab" role="tabpanel"
                                     aria-labelledby="home-tab">

                                    <div class="row">
                                        <div class="col-lg-6 col-xl-6">
                                            <table class="table m-0">
                                                <tbody>
                                                <tr>
                                                    <th scope="row">Nombres</th>
                                                    <td>{{$iglesia->nombre}}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Direccion</th>
                                                    <td>{{$iglesia->direccion}}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Barrio</th>
                                                    <td>{{$iglesia->barrio}}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Ciudad</th>
                                                    <td>{{$iglesia->ciudad==null?'':$iglesia->ciudad->name}}</td>
                                                </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-lg-6 col-xl-6">
                                            <table class="table m-0">
                                                <tbody>
                                                <tr>
                                                    <th scope="row">Tipo</th>
                                                    <td>{{$iglesia->tipo}}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Pastor</th>
                                                    <td>
                                                        {{$iglesia->pastor==null?'Ninguno':$iglesia->pastor->persona->nombres.' '.$iglesia->pastor->persona->apellidos}}
                                                        @can('asignar.AIglesias')
                                                            <button type="button" id="edit-pastor"
                                                                    class="btn btn-icon btn-link pull-right editar"
                                                                    data-toggle="tooltip"
                                                                    data-placement="top" title="Actualizar"
                                                                    data-original-title="Edit"
                                                                    style="margin-right: 5px;"
                                                                    onclick="setPastor()">
                                                                <i class="fas fa-pen"></i>
                                                            </button>
                                                        @endcan

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Colaborador</th>
                                                    <td>
                                                        {{$iglesia->colaborador==null?'Ninguno':$iglesia->colaborador->persona->nombres.' '.$iglesia->colaborador->persona->apellidos}}
                                                        @can('asignar.AIglesias')
                                                            <button type="button" id="edit-colaborador"
                                                                    class="btn btn-icon btn-link pull-right editar"
                                                                    data-toggle="tooltip"
                                                                    data-placement="top" title="Actualizar"
                                                                    data-original-title="Edit"
                                                                    style="margin-right: 5px;"
                                                                    onclick="setColaborador()">
                                                                <i class="fas fa-pen"></i>
                                                            </button>
                                                        @endcan

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Coordinador</th>
                                                    <td>
                                                        {{$iglesia->coordinador==null?'Ninguno':$iglesia->coordinador->persona->nombres.' '.$iglesia->coordinador->persona->apellidos}}
                                                        @can('asignar.AIglesias')
                                                            <button type="button" id="edit-coordinador"
                                                                    class="btn btn-icon btn-link pull-right editar"
                                                                    data-toggle="tooltip"
                                                                    data-placement="top" title="Actualizar"
                                                                    data-original-title="Edit"
                                                                    style="margin-right: 5px;"
                                                                    onclick="setCoordinador()">
                                                                <i class="fas fa-pen"></i>
                                                            </button>
                                                        @endcan
                                                    </td>
                                                </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                @can('ver.miembros')
                                    <div class="tab-pane fade" id="profile-tab" role="tabpanel"
                                         aria-labelledby="profile-tab">
                                        <div class="row">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table id="tabla-miembros"
                                                           class="display table table-striped table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th>Nombre</th>
                                                            <th>Telefono</th>
                                                            <th>Rol</th>
                                                        </tr>
                                                        </thead>
                                                        <tfoot>
                                                        <tr>
                                                            <th>Nombre</th>
                                                            <th>Telefono</th>
                                                            <th>Rol</th>
                                                        </tr>
                                                        </tfoot>
                                                        <tbody>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endcan
                                @can('ver.ofrendasYDiezmos')
                                    <div class="tab-pane fade" id="image-tab" role="tabpanel"
                                         aria-labelledby="profile-tab">
                                        <div class="row">

                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table id="tabla-ofrendasydiezmos"
                                                           class="display table table-striped table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th>Descripcion</th>
                                                            <th>Fecha</th>
                                                            <th>Ofrenda</th>
                                                            <th>Diezmo</th>
                                                        </tr>
                                                        </thead>
                                                        <tfoot>
                                                        <tr>
                                                            <th>Descripcion</th>
                                                            <th>Fecha</th>
                                                            <th>Ofrenda</th>
                                                            <th>Diezmo</th>
                                                        </tr>
                                                        </tfoot>
                                                        <tbody>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endcan
                                @can('ver.reuniones')
                                    <div class="tab-pane fade" id="reuniones-tab" role="tabpanel"
                                         aria-labelledby="image-tab">
                                        <div class="row">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table id="tabla-reuniones"
                                                           class="display table table-striped table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th>Nombre</th>
                                                            <th>Tipo</th>
                                                            <th>#Adultos</th>
                                                            <th>#Niños</th>
                                                            <th>fecha</th>
                                                        </tr>
                                                        </thead>
                                                        <tfoot>
                                                        <tr>
                                                            <th>Nombre</th>
                                                            <th>Tipo</th>
                                                            <th>#Adultos</th>
                                                            <th>#Niños</th>
                                                            <th>fecha</th>
                                                        </tr>
                                                        </tfoot>
                                                        <tbody>
                                                        @foreach($iglesia->reuniones as $reunion)
                                                            <th>{{$reunion->nombre}}</th>
                                                            <th>{{$reunion->tipo}}</th>
                                                            <th>{{$reunion->num_adultos}}</th>
                                                            <th>{{$reunion->num_ninios}}</th>
                                                            <th>{{$reunion->fecha_realizado}}</th>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endcan
                            </div>

                        </div>

                    </div>
                </div>


                <div class="col-md-3">
                    <div class="card card-profile">
                        <div class="card-header" style="background-image: url('/assets/img/blogpost.jpg')">
                            <div class="profile-picture">
                                <div class="avatar avatar-xl">
                                    <img id="perfil" src="/images/perfil/1_1566394810.jpg" alt="..."
                                         class="avatar-img rounded-circle">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="user-profile text-center">
                                <div class="name">{{$iglesia->nombre}}</div>
                                <div class="job">Tipo: {{$iglesia->tipo}}</div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row user-stats text-center">
                                <div class="col">
                                    <div class="number">{{$iglesia->miembros->count()}}</div>
                                    <div class="title">Miembros</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @can('asignar.AIglesias')
        @include('pages.modal.asignarAIglesias')
    @endcan
@endsection

@section('scripts')



    <script type="text/javascript">

        let iglesia = {!! json_encode($iglesia)!!}
        console.log(iglesia);

        function ramdom() {
            return (Math.random() * 1000000) + 1;
        }


        $(document).ready(function () {

                    @can('ver.miembros')
            var TABLA_MIEMBROSs = $('#tabla-miembros').DataTable({
                    ajax: {
                        url: "{{ url('recurso/miembros') }}",
                        "data": {
                            "id": iglesia.id
                        },
                        "dataSrc": function (data) {
                            var json = [];
                            console.log(data);
                            for (var item in data.msg) {
                                var itemJson = {
                                    id: data.msg[item].id,
                                    nombre: data.msg[item].nombres + ' ' + data.msg[item].apellidos,
                                    telefono: data.msg[item].telefono,
                                    rol: data.msg[item].rol,
                                };
                                json.push(itemJson)
                            }
                            return json;
                        }
                    },
                    columns: [
                        {data: "nombre"},
                        {data: "telefono"},
                        {data: "rol"},
                    ],
                });
                    @endcan

                    @can('ver.ofrendasYDiezmos')
            var TABLA_OFRENDASYDIEZMOS = $('#tabla-ofrendasydiezmos').DataTable({
                    ajax: {
                        url: "{{ url('recurso/getOfrendasYdiezmos') }}",
                        "data": {
                            "id": iglesia.id
                        },
                        "dataSrc": function (data) {
                            var json = [];
                            console.log(data);
                            for (var item in data.msg) {
                                var itemJson = {
                                    id: data.msg[item].id,
                                    Fecha: data.msg[item].fecha,
                                    Descripcion: data.msg[item].descripcion,
                                    Ofrenda: data.msg[item].ofrenda,
                                    Diezmo: data.msg[item].diezmo,
                                };
                                json.push(itemJson)
                            }
                            return json;
                        }
                    },
                    columns: [
                        {data: "Descripcion"},
                        {data: "Fecha"},
                        {data: "Ofrenda"},
                        {data: "Diezmo"},
                    ],
                });
                    @endcan

                    @can('ver.reuniones')
            var TABLA_REUNIONES = $('#tabla-reuniones').DataTable();
                    @endcan


        });

        @can('asignar.AIglesias')

        function getPastores() {

            let PASTORES = [];

            $.ajax({
                url: "/recurso/getPastoresLibres",
                async: false
            }).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].nombre
                    };
                    PASTORES.push(itemSelect2)
                }
                $('#select-pastor').select2({
                    theme: "bootstrap",
                    dropdownParent: $('#modal-pastor'),
                    placeholder: "Selecciona un pastor",
                    allowClear: true,
                    data: PASTORES
                });

            });

        }

        function getCoordinador() {

            let COORDINADOR = [];

            $.ajax({
                url: "/recurso/getCoordinadoresLibres",
                async: false
            }).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].nombre
                    };
                    COORDINADOR.push(itemSelect2)
                }
                $('#select-coordinador').select2({
                    theme: "bootstrap",
                    dropdownParent: $('#modal-coordinador'),
                    placeholder: "Selecciona un coordinador",
                    allowClear: true,
                    data: COORDINADOR
                });

            });

        }

        function getColaborador() {

            let COLABORADOR = [];

            $.ajax({
                url: "/recurso/getColaboradoresLibres",
                async: false
            }).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].nombre
                    };
                    COLABORADOR.push(itemSelect2)
                }
                $('#select-colaborador').select2({
                    theme: "bootstrap",
                    dropdownParent: $('#modal-colaborador'),
                    placeholder: "Selecciona un colaborador",
                    allowClear: true,
                    data: COLABORADOR
                });

            });

        }

        getPastores();
        getCoordinador();
        getColaborador();

        function setPastor() {
            $("#modal-pastor").modal('show');
        }

        function setCoordinador() {
            $("#modal-coordinador").modal('show');
        }

        function setColaborador() {
            $("#modal-colaborador").modal('show');
        }

        function asignarPastor() {
            $.ajax({
                url: "/recurso/asignarPastor",
                data: $("#pastor-iglesia-form").serialize(),
                type: 'POST',
                async: false
            }).done(function (data) {
                console.log(data);
                $('#pastor-iglesia-form')[0].reset();
                $('#modal-pastor').modal('hide');
                location.reload();

            });

        }

        function asignarCoordinador() {
            $.ajax({
                url: "/recurso/asignarCoordinador",
                data: $("#coordinador-iglesia-form").serialize(),
                type: 'POST',
                async: false
            }).done(function (data) {
                console.log(data);
                $('#coordinador-iglesia-form')[0].reset();
                $('#modal-coordinador').modal('hide');

                location.reload();
            });

        }

        function asignarColaborador() {
            $.ajax({
                url: "/recurso/asignarColaborador",
                data: $("#colaborador-iglesia-form").serialize(),
                type: 'POST',
                async: false
            }).done(function (data) {
                console.log(data);
                $('#colaborador-iglesia-form')[0].reset();
                $('#modal-colaborador').modal('hide');
                location.reload();
            });

        }

        function eliminarPastor() {
            $.ajax({
                url: "/recurso/eliminarPastor",
                data: $("#pastor-iglesia-form").serialize(),
                type: 'POST',
                async: false
            }).done(function (data) {
                console.log(data);
                $('#pastor-iglesia-form')[0].reset();
                $('#modal-pastor').modal('hide');
                location.reload();

            });
        }

        function eliminarCoordinador() {
            $.ajax({
                url: "/recurso/eliminarCoordinador",
                data: $("#pastor-iglesia-form").serialize(),
                type: 'POST',
                async: false
            }).done(function (data) {
                console.log(data);
                $('#pastor-iglesia-form')[0].reset();
                $('#modal-pastor').modal('hide');
                location.reload();

            });
        }

        function eliminarColaborador() {
            $.ajax({
                url: "/recurso/eliminarColaborador",
                data: $("#pastor-iglesia-form").serialize(),
                type: 'POST',
                async: false
            }).done(function (data) {
                console.log(data);
                $('#pastor-iglesia-form')[0].reset();
                $('#modal-pastor').modal('hide');
                location.reload();

            });
        }

        @endcan
    </script>

@endsection
