<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Auth::routes();

Route::get('/', 'HomeController@index')->name('home')->middleware('auth');;

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/form', 'HomeController@form')->name('form');
Route::post('guardar', 'HomeController@guardar');

Route::get('getStates', 'LocalidadController@getStates');
Route::get('getCities', 'LocalidadController@getCity');
Route::get('getStatesOfCity', 'LocalidadController@getStatesOfCity');
Route::get('getActividades', 'ActividadesController@getActividades');

Route::any('/{any?}', 'HomeController@index')->where('any', '.*')->middleware('auth');
*/

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Auth::routes();
//Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/form', 'HomeController@form')->name('form');

Route::get('/empresas', 'HomeController@form')->name('registroEmpresa');
Route::get('/personas', 'HomeController@registroPersonas')->name('registroPersonas');

Route::post('guardar', 'PersonaController@store');
Route::post('actualizar', 'PersonaController@actualizarUsuario');
Route::post('actualizarPass', 'PersonaController@actualizarPass');
Route::post('verificarUsuario', 'PersonaController@verificarUsuario');

Route::post('guardar-iglesia', 'IglesiaController@store')->name('guardar-iglesia');
Route::get('getIglesias', 'IglesiaController@index')->name('getIglesias');
Route::post('verificarIglesia', 'IglesiaController@verificarIglesia')->name('verificarIglesia');

Route::get('getStatesOfCity', 'LocalidadController@getStatesOfCity')->name('getStatesOfCity');
Route::get('getCountrieOfState', 'LocalidadController@getCountrieOfState')->name('getCountrieOfState');

Route::get('getPastores', 'PersonaController@getPastores');
Route::get('getCoordinadores', 'PersonaController@getCoordinadores');
Route::get('getLideres', 'PersonaController@getLideres');
Route::get('getColaboradores', 'PersonaController@getColaboradores');

Route::get('getCountries', 'LocalidadController@getCountries');
Route::get('getStates', 'LocalidadController@getStates');
Route::get('getCities', 'LocalidadController@getCity');
Route::get('getStatesOfCity', 'LocalidadController@getStatesOfCity');
Route::get('envioDeCorreos', 'HomeController@email');

Route::group(['middleware' => ['auth']], function () {

    Route::get('/', 'HomeController@index')->name('inicio');
    Route::get('/usuarios/{id}', 'HomeController@usuarios')->name('usuarios')->middleware(['permission:ver.usuarios']);
    Route::get('/usuarios', 'HomeController@usuarios')->name('usuarios')->middleware(['permission:ver.usuarios']);
    Route::get('/roles', 'HomeController@roles')->name('roles')->middleware(['permission:ver.roles']);
    Route::get('/permisos', 'HomeController@permisos')->name('permisos')->middleware(['role:Super Admin']);
    Route::get('/iglesias/{id}', 'HomeController@iglesias');
    Route::get('/iglesias', 'HomeController@iglesias')->name('iglesias')->middleware(['permission:ver.iglesias']);
    Route::get('/visitas', 'HomeController@visitas')->name('visitas')->middleware(['permission:ver.visitas']);
    Route::get('/reuniones', 'HomeController@reuniones')->name('reuniones')->middleware(['permission:ver.reuniones']);
    Route::get('/miembros', 'HomeController@miembros')->name('miembros')->middleware(['permission:ver.miembros']);
    Route::get('/ofrendasYDiezmos', 'HomeController@ofrendasYDiezmos')->name('ofrendasYDiezmos')->middleware(['permission:ver.ofrendasYDiezmos']);
    Route::get('/reportes', 'HomeController@reportes')->name('reportes')->middleware(['permission:ver.reportes']);

    Route::get('/perfil', 'HomeController@perfil')->name('perfil');
    Route::get('/miIglesia', 'HomeController@miIglesia')->name('miIglesia');
    Route::get('/estadisticas', 'HomeController@estadisticas')->name('estadisticas');
    Route::get('/cuestionario/{id}', 'HomeController@cuestionario')->name('cuestionario');

    Route::get('/reporte_iglesias/{pais}/{departamento}/{ciudad}', 'HomeController@reporte_iglesias')->middleware(['permission:ver.reportes']);
    Route::get('/reporte_asistencia_ingresos/{fecha}', 'HomeController@reporte_asistencia_ingresos')->middleware(['permission:ver.reporte_asistencia_ingresos']);
    Route::get('/reporte_iglesias_asistencia_ingresos/{pais}/{departamento}/{ciudad}/{fecha}', 'HomeController@reporte_iglesias_asistencia_ingresos')->middleware(['permission:ver.reporte_iglesias_asistencia_ingresos']);;

});

Route::group(['prefix' => 'recurso', 'middleware' => ['auth']], function () {

    Route::apiResource('users', 'Admin\UserController');
    Route::apiResource('roles', 'Admin\RoleController');
    Route::apiResource('permissions', 'Admin\PermissionController');

    Route::apiResource('iglesias', 'IglesiaController');
    Route::get('verIglesias', 'IglesiaController@verIglesias');

    Route::post('cambiar-pass', 'Admin\UserController@actualizarPass');
    Route::post('cambiar-avatar', 'Admin\UserController@update_profile');

    Route::get('getReuniones', 'ReunionesController@reunionesPendientes');
    Route::get('getReunionesCompletadas', 'ReunionesController@reunionesCompletadas');
    Route::get('getReunionesOld', 'ReunionesController@verReunionesViejas');
    Route::post('guardarReunion', 'ReunionesController@store');
    Route::post('actualizarReuniones', 'ReunionesController@update');
    Route::post('cambiarEstadoReunion', 'ReunionesController@cambiarEstadoReunion');
    Route::post('asistentesReunion', 'ReunionesController@verAsistentesReunion');
    Route::post('agregarAsistente', 'ReunionesController@agregarAsistente');
    Route::post('eliminarAsistente', 'ReunionesController@eliminarAsistente');
    Route::post('eliminarReunion', 'ReunionesController@eliminarReunion');
    Route::get('graficaReuniones', 'ReunionesController@graficaReuniones');

    //ver todos los miembros de una iglesia mandar id de iglesia
    Route::get('miembros', 'IglesiaController@verMiembros');

    Route::post('agregarMimebro', 'IglesiaController@agregarMimebro');
    Route::post('actualizarMiembro', 'IglesiaController@actualizarMiembro');
    Route::post('eliminarMimebro', 'IglesiaController@eliminarMimebro');

    Route::post('asignarPastor', 'IglesiaController@asignarPastor');
    Route::post('asignarCoordinador', 'IglesiaController@asignarCoordinador');
    Route::post('asignarColaborador', 'IglesiaController@asignarColaborador');

    Route::post('eliminarPastor', 'IglesiaController@eliminarPastor');
    Route::post('eliminarCoordinador', 'IglesiaController@eliminarCoordinador');
    Route::post('eliminarColaborador', 'IglesiaController@eliminarColaborador');

    Route::get('getPastoresLibres', 'PersonaController@getPastoresLibres');
    Route::get('getCoordinadoresLibres', 'PersonaController@getCoordinadoresLibres');
    Route::get('getColaboradoresLibres', 'PersonaController@getColaboradoresLibres');
    Route::get('verMiembrosCoordinadores', 'IglesiaController@verMiembrosCoordinadores');

    Route::get('getOfrendasYdiezmos', 'OfrendasController@verOfrendasYDiezmos');
    Route::post('guardarOfrendasYDiezmos', 'OfrendasController@guardarOfrendasYDiezmos');
    Route::post('actualizarOfrendaYDiezmo', 'OfrendasController@actualizarOfrendaYDiezmo');
    Route::post('eliminarOfrendaYDiezmo', 'OfrendasController@eliminarOfrendaYDiezmo');

    // Seleccionar iglesia navbar
    Route::post('seleccionarIglesia', 'IglesiaController@seleccionarIglesia');
    Route::post('verIglesiaSeleccionada', 'IglesiaController@verIglesiaSeleccionada');

    Route::post('envioDeCorreos', 'HomeController@email');

    Route::get('getVisitasPendientes', 'VisitasController@getVisitasPendientes');
    Route::get('getVisitasTerminadas', 'VisitasController@getVisitasTerminadas');
    Route::post('crearVisita', 'VisitasController@crearVisita');
    Route::post('guardarVisita', 'VisitasController@guardarVisita');
    Route::post('terminarVisita', 'VisitasController@terminarVisita');
    Route::post('cambioPendienteVisita', 'VisitasController@cambioPendienteVisita');
    Route::post('actualizarVisita', 'VisitasController@actualizarVisita');
    Route::post('eliminarVisita', 'VisitasController@eliminarVisita');

});


