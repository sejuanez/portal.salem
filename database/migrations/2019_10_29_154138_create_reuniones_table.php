<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReunionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reuniones', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('nombre');// diario,semanal,mensual//
            $table->string('tipo');// diario,semanal,mensual//
            $table->integer('num_adultos')->default(0);
            $table->integer('num_ninios')->default(0);
            $table->longText('descripcion')->nullable();

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('iglesia_id')->unsigned()->nullable();
            $table->foreign('iglesia_id')->references('id')->on('iglesias');

            $table->date('fecha_realizado');
            $table->string('hora_realizado');
            $table->boolean('estado')->default(0);//Pendiente/Completada


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reuniones');
    }
}
