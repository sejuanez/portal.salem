@extends('layouts.dashboard')

@section('content')

    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Ofrendas y Diezmos</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Ofrendas y Diezmos</a>
                    </li>
                </ul>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">Ofrendas y Diezmos de la iglesia</h4>

                                @can('crear.ofrendasYDiezmos')
                                    <button class="btn btn-primary btn-round ml-auto" id="modalGuardar">
                                        <i class="fa fa-plus"></i>
                                        Nueva Ofrendas y Diezmos
                                    </button>
                                @endcan
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <table id="tablaOfrendasydiezmos" class="display table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>Descripcion</th>
                                        <th>Fecha</th>
                                        <th>Ingresos</th>
                                        <th class="text-right">Opciones</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Descripcion</th>
                                        <th>Fecha</th>
                                        <th>Ingresos</th>
                                        <th class="text-right">Opciones</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages.modal.ofrendasydiezmos')

@endsection

@section('scripts')
    <script type="text/javascript">

        $('#tablaUsuarios thead th').each(function () {
            var title = $(this).text();
            $(this).html(title + ' <input type="text" class="form-control" style="height: auto !important;" placeholder="Buscar ' + title + '" />');
        });

        var TABLA = $('#tablaOfrendasydiezmos').DataTable({
            ajax: {
                url: "{{ url('recurso/getOfrendasYdiezmos') }}",
                "dataSrc": function (data) {
                    var json = [];
                    console.log(data);
                    for (var item in data.msg) {
                        var itemJson = {
                            id: data.msg[item].id,
                            Fecha: data.msg[item].fecha,
                            Descripcion: data.msg[item].descripcion,
                            Ofrenda: data.msg[item].ofrenda,
                            Opciones: opciones(data.msg[item].activo)
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            columns: [
                {data: "Descripcion"},
                {data: "Fecha"},
                {data: "Ofrenda"},
                {data: "Opciones"},
            ],
        });

        TABLA.columns().every(function () {
            var table = this;
            $('input', this.header()).on('keyup change', function () {
                if (table.search() !== this.value) {
                    table.search(this.value).draw();
                }
            });
        });

        function opciones(estado) {
            var opciones = '';
            @can('editar.ofrendasYDiezmos')
                opciones += '<button type="button" class="btn btn-primary btn-xs editar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Actualizar" data-original-title="Edit"' +
                '           style="margin-right: 5px;">\n' +
                '           <i class="fas fa-pen"></i>\n' +
                ' </button>';
            @endcan
            @can('eliminar.ofrendasYDiezmos')
                opciones += '' +
                    '<button type="button" class="btn btn-danger btn-xs eliminar" ' +
                    '           data-toggle="tooltip" data-placement="top" title="Deshabilitar" data-original-title="Edit">' +
                    '           <i class="fas fa-trash"></i>\n' +
                    ' </button>';
            @endcan
                return opciones;
        }

        @can('crear.ofrendasYDiezmos')
        $("#modalGuardar").on('click', function () {
            $('#modal').modal('show');
            $('#form-ofrendasydiezmos')[0].reset();
            $('#actualizar').hide();
            $('#guardar').show();
            $('#eliminar').hide();
            $("#error").hide();
            $("#accion-modal").html('Registrar');

        });
        $("#guardar").on('click', function () {
            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#guardar").prop('disabled', true);
            $.ajax({
                url: '{{url('recurso/guardarOfrendasYDiezmos')}}',
                type: 'POST',
                data: $("#form-ofrendasydiezmos").serialize(),
            }).done(function (response) {
                $('#form-ofrendasydiezmos')[0].reset();
                $('#modal').modal('hide');
                $("#guardar").prop('disabled', false);
                TABLA.ajax.reload();

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.msg,
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#guardar").prop('disabled', false);
            });

        });

        @endcan

        @can('editar.ofrendasYDiezmos')
        TABLA.on('click', '.editar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            $("#modal").modal('show');
            $('#form-ofrendasydiezmos')[0].reset();
            $('#actualizar').show();
            $('#eliminar').hide();
            $('#guardar').hide();
            $("#accion-modal").html('Editar');

            $("#id").val(data.id);
            $("#descripcion").val(data.Descripcion);
            $("#fecha").val(data.Fecha);
            $("#ofrenda").val(data.Ofrenda);

        });
        $("#actualizar").on('click', function () {
            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#actualizar").prop('disabled', true);
            $.ajax({
                url: '{{url('recurso/actualizarOfrendaYDiezmo')}}',
                type: 'POST',
                data: $("#form-ofrendasydiezmos").serialize(),
            }).done(function (response) {
                $('#form-ofrendasydiezmos')[0].reset();
                $('#modal').modal('hide');
                TABLA.ajax.reload();

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.msg,
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#actualizar").prop('disabled', false);
            });

        });
        @endcan

        @can('eliminar.ofrendasYDiezmos')
        TABLA.on('click', '.eliminar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            swal({
                title: 'Estas seguro?',
                text: "Vas a Eliminar al este registro de ofrendas y diezmos? ",
                icon: 'warning',
                buttons: {
                    confirm: {
                        text: 'Si, Eliminar',
                        className: 'btn btn-success'
                    },
                    cancel: {
                        text: 'Cancelar',
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((Delete) => {
                if (Delete) {

                    $.ajax({
                            url: '{{url('recurso/eliminarOfrendaYDiezmo')}}',
                            type: 'POST',
                            data: {
                                id: data.id,
                                _token: $('meta[name="csrf-token"]').attr('content')
                            },

                        }
                    ).done(function (response) {
                        console.log(response);
                        if (response.status == "Error") {
                            //error
                        } else {
                            let msg = "";
                            TABLA.ajax.reload();
                            swal({
                                title: 'Ok!',
                                text: 'El registro de ofrendas y diezmos ha sido eliminado',
                                icon: 'success',
                                buttons: {
                                    confirm: {
                                        className: 'btn btn-success'
                                    }
                                }
                            });
                        }
                        //return response;
                    }).fail(function (error) {

                        console.log(error);

                    });


                } else {
                    swal.close();
                }
            });

        });
        @endcan

    </script>
@endsection
