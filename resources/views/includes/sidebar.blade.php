<div class="sidebar sidebar-style-2">
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-2">
                    <img src="/images/perfil/{{auth()->user()->imagen}}" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
								<span>
									{{ auth()->user()->persona->nombres }} {{ auth()->user()->persona->apellidos }}
									<span class="user-level">
                                    {{ auth()->user()->getRoleNames()}}
                                    </span>
									<span class="caret"></span>
								</span>
                    </a>
                    <div class="clearfix"></div>

                    <div class="collapse in" id="collapseExample">
                        <ul class="nav">
                            <li>
                                <a href="{{route('perfil')}}">
                                    <span class="link-collapse">Mi Cuenta</span>
                                </a>
                            </li>

                            <li>
                                <a href="#settings">
                                    <span class="link-collapse">Configuracion</span>
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('logout') }}">
                                    <span class="link-collapse">Salir</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="nav nav-primary">
                @if((auth()->user()->persona->miembro!=null && (session()->get('id_iglesia', function () {return 'default';})) !="default") || auth()->user()->getRoleNames()[0]=='Super Admin' || auth()->user()->getRoleNames()[0]=='Admin')

                    <li class="nav-item {{ request()->route()->getName() === 'inicio' ? ' active' : '' }}">
                        <a href="{{ route('inicio') }}">
                            <i class="fas fa-home"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>

                    @can('ver.iglesias')
                        <li class="nav-item {{ request()->is('iglesias') || request()->is('iglesias/*')? ' active' : '' }}">
                            <a href="{{ route('iglesias') }}">
                                <i class="fas fa-home"></i>
                                <p>Iglesias</p>
                            </a>
                        </li>
                    @endcan

                    @can('ver.miembros')
                        <li class="nav-item {{ request()->is('miembros') || request()->is('miembros/*')? ' active' : '' }}">
                            <a href="{{ route('miembros') }}">
                                <i class="fas fa-home"></i>
                                <p>Siervos</p>
                            </a>
                        </li>
                    @endcan

                    @can('ver.visitas')
                        <li class="nav-item {{ request()->is('visitas') || request()->is('visitas/*')? ' active' : '' }}">
                            <a href="{{ route('visitas') }}">
                                <i class="fas fa-home"></i>
                                <p>Visitas</p>
                            </a>
                        </li>
                    @endcan

                    @can('ver.reuniones')
                        <li class="nav-item {{ request()->is('reuniones') || request()->is('reuniones/*')? ' active' : '' }}">
                            <a href="{{ route('reuniones') }}">
                                <i class="fas fa-home"></i>
                                <p>Reuniones</p>
                            </a>
                        </li>
                    @endcan

                    @can('ver.ofrendasYDiezmos')
                        <li class="nav-item {{ request()->is('ofrendasYDiezmos') || request()->is('ofrendasYDiezmos/*')? ' active' : '' }}">
                            <a href="{{ route('ofrendasYDiezmos') }}">
                                <i class="fas fa-home"></i>
                                <p>Ofrendas y Diezmos</p>
                            </a>
                        </li>
                    @endcan

                    @can('ver.estadisticas')
                        <li class="nav-item {{ request()->is('estadisticas') || request()->is('estadisticas/*')? ' active' : '' }}">
                            <a href="{{ route('estadisticas') }}">
                                <i class="fas fa-home"></i>
                                <p>Estadisticas</p>
                            </a>
                        </li>
                    @endcan

                    @can('ver.reportes')
                        <li class="nav-item {{ request()->is('reportes') || request()->is('reportes/*')? ' active' : '' }}">
                            <a href="{{ route('reportes') }}">
                                <i class="fas fa-home"></i>
                                <p>Reportes</p>
                            </a>
                        </li>
                    @endcan

                    @can('ver.usuarios')
                        <li class="nav-item {{ request()->route()->getName() === 'usuarios' ? ' active' : '' }}">
                            <a href="{{ route('usuarios') }}">
                                <i class="fas fa-user"></i>
                                <p>Usuarios</p>
                            </a>
                        </li>
                    @endcan

                    @can('ver.roles')
                        <li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-ellipsis-h"></i>
							</span>
                            <h4 class="text-section">Administrador</h4>
                        </li>
                        <li class="nav-item {{ request()->route()->getName() === 'roles' ? ' active' : '' }}">
                            <a href="{{ route('roles') }}">
                                <i class="fas fa-user"></i>
                                <p>Roles</p>
                            </a>
                        </li>
                    @endcan

                    @role('Super Admin')
                    <li class="nav-item {{ request()->route()->getName() === 'permisos' ? ' active' : '' }}">
                        <a href="{{ route('permisos') }}">
                            <i class="fas fa-user"></i>
                            <p>Permisos</p>
                        </a>
                    </li>
                    @endrole

                @endif
            </ul>
        </div>
    </div>
</div>

