<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitas', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('coordinador_id')->unsigned();
            $table->unsignedBigInteger('iglesia_id')->unsigned();

            $table->date('fecha_visita')->nullable();

            $table->string('hora_inicio')->nullable();
            $table->string('hora_fin')->nullable();

            $table->string('limpio')->nullable();//si,no enum('s','n')
            $table->string('cartelera')->nullable();//si,no enum('s','n')
            $table->string('alfoli')->nullable();//si,no enum('s','n')
            $table->string('host_atten')->nullable();//si,no,ne enum('s','n','ne')
            $table->string('host_people_loc')->nullable();//si,no, enum('s','n','ne')
            $table->string('music')->nullable();//bueno,regular,malo
            $table->string('dance')->nullable();//bueno,regular,malo, ne
            $table->string('distr')->nullable();//bueno,regular,malo
            $table->string('music_love')->nullable();//si,no enum('s','n')
            $table->string('music_rep')->nullable();//si,no enum('s','n')
            $table->string('bible_use')->nullable();//si,no enum('s','n')
            $table->string('reflex')->nullable();//si,no enum('s','n')
            $table->string('pray_time')->nullable();//si,no enum('s','n')
            //$table->time('tiempo_inicio_predica')->nullable();
            $table->string('leader_vision')->nullable();//si,no enum('s','n')
            $table->string('balance')->nullable();//si,no enum('s','n')
            $table->string('unction_preach')->nullable();//si,no enum('s','n')
            $table->string('jesus_center')->nullable();//si,no enum('s','n')
            $table->string('pulpit_admin')->nullable();//bueno,regular,malo
            $table->string('preach_ability')->nullable();//bueno,regular,malo
            $table->string('god_depend')->nullable();//si,no enum('s','n')
            $table->string('leader_love_work')->nullable();//si,no enum('s','n')
            $table->string('personal_presentation')->nullable();//bueno,regular,malo
            $table->string('op_sex_behavior')->nullable();//bueno,regular,malo
            //$table->time('tiempo_fin_predica')->nullable();
            $table->string('child_site')->nullable();//si,no, ne enum('s','n','ne')
            //$table->boolean('child_security')->nullable();//si,no,ne enum('s','n','ne')
            $table->string('child_atten')->nullable();//si,no,ne enum('s','n','ne')
            $table->string('teacher_prep')->nullable();//si,no,ne enum('s','n','ne')
            $table->string('minis_unction')->nullable();//si,no
            $table->string('minis_music')->nullable();//si,no
            $table->string('minis_child')->nullable();//bueno,regular,malo, no encontrado
            //$table->time('fin_tiempo_alabanza')->nullable();
            $table->string('leader_atten')->nullable();//si,no,ne enum('s','n','ne')
            $table->string('people_close_to_leader')->nullable();//si,no,ne enum('s','n','ne')
            //Comportamiento del conyugue frente a la congregación
            $table->string('spouse_behavior')->nullable();//bueno,regular,malo,na

            //Sujeción del equipo de trabajo al Pastor/Lider
            $table->string('subjection')->nullable();//si, no

            //Equipo de trabajo en interseción
            $table->string('intersec')->nullable();//si, no

            //Inversiones
            $table->string('inversiones')->nullable();//si, no

            //Asistencia del grupo de trabajo a discipulado (sábado)
            $table->string('sat_assist')->nullable();//si, no
            //Inicio
            $table->string('inicio')->nullable();//si, no

            //Alabanza
            $table->string('alabanza')->nullable();

            //Oración
            $table->string('oracion')->nullable();//si, no

            //Predicación
            $table->string('predicacion')->nullable();//si, no

            //Maestro de niños
            $table->string('maestro_ninos')->nullable();//si, no

            //Ministración
            $table->string('ministracion')->nullable();//si, no

            //recomendaciones
            $table->longText('recomendaciones')->nullable();

            $table->foreign('iglesia_id')->references('id')->on('iglesias');
            $table->foreign('coordinador_id')->references('id')->on('users');

            $table->boolean('estado')->default(1);//si, no

            $table->timestamps();
        });
    }

    /**
     *
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitas');
    }
}
