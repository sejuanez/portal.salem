<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Logs extends Model
{
    //
    //
    use Notifiable;

    protected $fillable = [
        'descripcion',
        'iglesia_id',
        'user_id',
    ];
}
