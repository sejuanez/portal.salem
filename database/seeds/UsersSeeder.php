<?php


    use App\User;
    use App\Personas;
    use Illuminate\Database\Seeder;
    use Spatie\Permission\Models\Permission;
    use Spatie\Permission\Models\Role;

    class UsersSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {

            $user = User::create([
                'nombre_usuario' => 'admin',
                'email' => 'admin@admin.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Administrador',
                'apellidos' => 'del Sistema',
                'cedula' => '000000',
                'telefono' => '300000',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Ingeniero',
                'trabajo' => 'Ingeniero',
                'idiomas' => 'Español(Alto), Ingles(Medio)',
                'formacionMusical' => 'Guitarra(Alto), Canto(Medio)',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => 'Auxiliar',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Super Admin');


            ///////////////PASTORES///////////////////
            ///// 1
            $user = User::create([
                'nombre_usuario' => 'pastorleo28',
                'email' => 'pastorleo28@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Leonardo ',
                'apellidos' => 'Gamez',
                'cedula' => '1085263185',
                'telefono' => '3043507980',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');
            ///// 2
            $user = User::create([
                'nombre_usuario' => 'pghdez',
                'email' => 'pghdez@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'GERMAN ALONSO',
                'apellidos' => 'HERNANDEZ PANTOJA',
                'cedula' => '12988938',
                'telefono' => '3122088498',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');
            ///// 3
            $user = User::create([
                'nombre_usuario' => 'cathelinegc',
                'email' => 'cathelinegc@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Álvaro Andrés',
                'apellidos' => 'Gámez Araujo',
                'cedula' => '87069943',
                'telefono' => '3017883035',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');
            ///// 4
            $user = User::create([
                'nombre_usuario' => 'oscar28le',
                'email' => 'oscar28le@live.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'OSCAR DAVID',
                'apellidos' => 'LUCERO ERASO',
                'cedula' => '12747755',
                'telefono' => '3105446989',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');
            ///// 5
            $user = User::create([
                'nombre_usuario' => 'magdayohana',
                'email' => 'magdayohana@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Magda Yohana',
                'apellidos' => 'Bolaños',
                'cedula' => '36953177',
                'telefono' => '3163082867',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');
            ///// 6
            $user = User::create([
                'nombre_usuario' => 'leomigue777',
                'email' => 'leomigue777@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Leandro Miguel',
                'apellidos' => 'Reveló Bravo',
                'cedula' => '87067833',
                'telefono' => '3173686974',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');
            ///// 7
            $user = User::create([
                'nombre_usuario' => 'phasam',
                'email' => 'phasam@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Phanor Afranio',
                'apellidos' => 'Samaniego Cárdenas',
                'cedula' => '98384742',
                'telefono' => '3046836752',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');
            ///// 8

            ///// 9
            $user = User::create([
                'nombre_usuario' => 'continentesverdes',
                'email' => 'continentesverdes@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Luis Carlos',
                'apellidos' => 'Ojeda Gamboa',
                'cedula' => '12991551',
                'telefono' => '3127780045',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');
            ///// 10
            $user = User::create([
                'nombre_usuario' => 'pgermangt',
                'email' => 'pgermangt@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Germán',
                'apellidos' => 'Gámez',
                'cedula' => '12994533',
                'telefono' => '3165239236',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');
            ///// 11
            $user = User::create([
                'nombre_usuario' => 'jhongrandagomez74',
                'email' => 'jhongrandagomez74@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'jhon',
                'apellidos' => 'Granda',
                'cedula' => '79681090',
                'telefono' => '3104391198',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');
            ///// 12
            $user = User::create([
                'nombre_usuario' => 'edbarcen',
                'email' => 'edbarcen@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'EDGAR GIL',
                'apellidos' => 'BOSCO BÁRCENAS CASTILLO',
                'cedula' => '12970041',
                'telefono' => '3157000000',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');
            ///// 13
            $user = User::create([
                'nombre_usuario' => 'fer.cab7',
                'email' => 'fer.cab7@hotmail.es',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Oscar Fernando',
                'apellidos' => 'Cabrera Burbano',
                'cedula' => '12999642',
                'telefono' => '3222205669',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');
            ///// 14

            ///// 15
            $user = User::create([
                'nombre_usuario' => 'munoz-lili777',
                'email' => 'munoz-lili777@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Liliana',
                'apellidos' => 'Villota Muñoz',
                'cedula' => '30727651',
                'telefono' => '3176373584',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');
            ///// 16
            $user = User::create([
                'nombre_usuario' => 'ramandadelrosario',
                'email' => 'ramandadelrosario@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'AMANDA',
                'apellidos' => 'DEL ROSARIO',
                'cedula' => '30715692',
                'telefono' => '3177003906',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');

            ///// 17
            $user = User::create([
                'nombre_usuario' => 'diegofernando.paz',
                'email' => 'diegofernando.paz@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Diego Fernando',
                'apellidos' => 'Paz Toro',
                'cedula' => '98384355',
                'telefono' => '3154771746',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');

            ///// 18


            ///// 19
            $user = User::create([
                'nombre_usuario' => 'jackiev.ricaurte21',
                'email' => 'jackiev.ricaurte21@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Jackelin',
                'apellidos' => 'Vela Ricaurte',
                'cedula' => '36751349',
                'telefono' => '3163612147',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');

            ///// 20
            $user = User::create([
                'nombre_usuario' => 'victorevelov',
                'email' => 'victorevelov@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Victor Andrés',
                'apellidos' => 'Revelo Viteri',
                'cedula' => '87066396',
                'telefono' => '3133203829',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');

            ///// 21
            $user = User::create([
                'nombre_usuario' => 'dairo16lozano',
                'email' => 'dairo16lozano@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Dairo Alexander',
                'apellidos' => 'Lozano Males',
                'cedula' => '98380412',
                'telefono' => '3177924007',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');


            ///// 22


            ///// 23
            $user = User::create([
                'nombre_usuario' => 'jorgeayus84',
                'email' => 'jorgeayus84@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Jorge Alberto',
                'apellidos' => 'Ayus Araujo',
                'cedula' => '92694399',
                'telefono' => '3017855010',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');

            ///// 24
            $user = User::create([
                'nombre_usuario' => 'alfredolassom',
                'email' => 'alfredolassom@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Alfredo',
                'apellidos' => 'Lasso',
                'cedula' => '12958204',
                'telefono' => '3146803383',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');

            ///// 25
            $user = User::create([
                'nombre_usuario' => 'williambenavides74',
                'email' => 'williambenavides74@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'William',
                'apellidos' => 'Benavides',
                'cedula' => '98387362',
                'telefono' => '3225435775',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');

            ///// 26
            $user = User::create([
                'nombre_usuario' => 'nlarranieg',
                'email' => 'nlarranieg@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Nelson',
                'apellidos' => 'Larraniaga',
                'cedula' => '3135929147',
                'telefono' => '3225435775',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');

            ///// 27
            $user = User::create([
                'nombre_usuario' => 'jackbenavides',
                'email' => 'jackbenavides@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'JACK DANNY',
                'apellidos' => 'BENAVIDES SANCHEZ',
                'cedula' => '13067008',
                'telefono' => '3166534508',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');


            ///// 28
            $user = User::create([
                'nombre_usuario' => 'nogueraoscar050',
                'email' => 'nogueraoscar050@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'OSCAR ARMANDO',
                'apellidos' => 'NOGUERA ORTIZ',
                'cedula' => '87490908',
                'telefono' => '3113405225',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');


            ///// 29


            ///// 30
            $user = User::create([
                'nombre_usuario' => 'herden',
                'email' => 'herden@hotmail.es',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'LUIS HERMES',
                'apellidos' => 'DELGADO NARVÁEZ',
                'cedula' => '12980275',
                'telefono' => '3210000000',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');


            ///// 30
            $user = User::create([
                'nombre_usuario' => 'lanamorada',
                'email' => 'lanamorada@yahoo.es',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'María Alejandra',
                'apellidos' => 'Gamez Araujo',
                'cedula' => '108529460',
                'telefono' => '3043507978',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Pastor');


            ///////////////Lideres///////////////////
            ///
            ///
            ///
            ///
            /// ////////////////////////////////////
            /// lider 1
            $user = User::create([
                'nombre_usuario' => 'juancamiloospinazapata',
                'email' => 'juancamiloospinazapata@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Juan Camilo',
                'apellidos' => 'Ospina Zapata',
                'cedula' => '1017162666',
                'telefono' => '3216103874',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 2
            $user = User::create([
                'nombre_usuario' => 'rophe07',
                'email' => 'rophe07@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Adriana',
                'apellidos' => 'Jaramillo Martinez',
                'cedula' => '52006130',
                'telefono' => '3128140938',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 3
            $user = User::create([
                'nombre_usuario' => 'jlozano.gpcp',
                'email' => 'jlozano.gpcp@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Jaime Eduardo',
                'apellidos' => 'Lozano',
                'cedula' => '10118443',
                'telefono' => '3148764178',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 4
            $user = User::create([
                'nombre_usuario' => 'mariselamontoyapescador',
                'email' => 'mariselamontoyapescador@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Marisela',
                'apellidos' => 'Montoya',
                'cedula' => '42126845',
                'telefono' => '3205331823',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 5
            $user = User::create([
                'nombre_usuario' => 'dialejobecerra',
                'email' => 'dialejobecerra@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Alejandro',
                'apellidos' => 'Becerra',
                'cedula' => '4538895',
                'telefono' => '3206205763',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 6
            $user = User::create([
                'nombre_usuario' => 'edicosmos2008',
                'email' => 'edicosmos2008@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Ofelia',
                'apellidos' => 'Ovalle',
                'cedula' => '35328815',
                'telefono' => '3103923785',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 7
            /* $user = User::create([
                 'nombre_usuario' => 'munoz-lili777',
                 'email' => 'munoz-lili777@hotmail.com',
                 'password' => bcrypt('123456'),
             ]);
             $persona = Personas::create([
                 'nombres' => 'Liliana',
                 'apellidos' => 'Villota Muñoz',
                 'cedula' => '30727651',
                 'telefono' => '3176373584',
                 'fechaNacimiento' => '2000/02/20',
                 'nacimientoHijos' => '2014',
                 'estadoCivil' => 'Soltero',
                 'escolaridad' => '4',
                 'estudios' => 'Pendiente',
                 'trabajo' => 'Pendiente',
                 'idiomas' => '',
                 'formacionMusical' => '',
                 'fechaIngreso' => '2010/02/02',
                 'recorridoIglesia' => '',
                 'user_id' => $user->id,
             ]);
             $user->assignRole('Lider');
     */
            /// lider 8


            /// lider 9
            $user = User::create([
                'nombre_usuario' => 'neashalom',
                'email' => 'neashalom@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Andrea',
                'apellidos' => 'Viviana Muñoz',
                'cedula' => '1085661196',
                'telefono' => '3116578895',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 10
            $user = User::create([
                'nombre_usuario' => 'angelmeza_pasto',
                'email' => 'angelmeza_pasto@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'ANGEL DARIO',
                'apellidos' => 'MEZA ORTIZ',
                'cedula' => '5208693',
                'telefono' => '3178016939',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 11
            $user = User::create([
                'nombre_usuario' => 'alicia30721829',
                'email' => 'alicia30721829@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'BERTHA ALICIA',
                'apellidos' => 'ALVARADO VILLARREAL',
                'cedula' => '30721829',
                'telefono' => '3188409850',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 12


            /// lider 13
            $user = User::create([
                'nombre_usuario' => 'constanzah307',
                'email' => 'constanzah307@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Constanza',
                'apellidos' => 'Hernandez Pantoja',
                'cedula' => '30745951',
                'telefono' => '3183880014',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 14


            /// lider 15
            $user = User::create([
                'nombre_usuario' => 'alex-ander.7',
                'email' => 'alex-ander.7@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Diego Alexander',
                'apellidos' => 'Zambrano Lombana',
                'cedula' => '87066775',
                'telefono' => '3104748112',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 16
            $user = User::create([
                'nombre_usuario' => 'elsycasar',
                'email' => 'elsycasar@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'ELSY',
                'apellidos' => 'CASTRO ARBOLEDA',
                'cedula' => '59660880',
                'telefono' => '3104748112',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 16
            $user = User::create([
                'nombre_usuario' => 'fabiorgt041',
                'email' => 'fabiorgt041@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'FABIO ROBERTO',
                'apellidos' => 'GAMEZ TORRES',
                'cedula' => '12974271',
                'telefono' => '3041020936',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 16
            $user = User::create([
                'nombre_usuario' => 'gineth.obando',
                'email' => 'gineth.obando@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'GINETH ZOILA',
                'apellidos' => 'OBANDO CRUEL',
                'cedula' => '31225948',
                'telefono' => '3113231353',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 16
            $user = User::create([
                'nombre_usuario' => 'hansyocc',
                'email' => 'hansyocc@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'HANSY',
                'apellidos' => 'OBANDO CASTILLO',
                'cedula' => '98430937',
                'telefono' => '3104984553',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 16
            $user = User::create([
                'nombre_usuario' => 'lopezhari',
                'email' => 'lopezhari@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'HAROLD ARIEL',
                'apellidos' => 'LOPEZ CAICEDO',
                'cedula' => '98429647',
                'telefono' => '3163678530',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 16
            $user = User::create([
                'nombre_usuario' => 'andrea4077',
                'email' => 'andrea4077@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Heidi Andrea',
                'apellidos' => 'Jojoa Mora',
                'cedula' => '36954814',
                'telefono' => '3185698474',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 16
            $user = User::create([
                'nombre_usuario' => 'dannylopez1986',
                'email' => 'dannylopez1986@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Ladys Alicia',
                'apellidos' => 'Caicedo de López',
                'cedula' => '27122493',
                'telefono' => '3158617838',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 16
            $user = User::create([
                'nombre_usuario' => 'analili888',
                'email' => 'analili888@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'LILIANA',
                'apellidos' => 'ANDRADE ARÉVALO',
                'cedula' => '59815111',
                'telefono' => '3116429812',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 16
            $user = User::create([
                'nombre_usuario' => 'trejohelena060',
                'email' => 'trejohelena060@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Luis Edmundo ',
                'apellidos' => 'Gomez Cadena',
                'cedula' => '12974118',
                'telefono' => '3105182821',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');


            /// lider 16
            $user = User::create([
                'nombre_usuario' => 'monica.marcela.moreno.ortega',
                'email' => 'monica.marcela.moreno.ortega@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Maria Magdalena',
                'apellidos' => 'Ortega Zambrano',
                'cedula' => '31973781',
                'telefono' => '3787574724',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');


            /// lider 16
            $user = User::create([
                'nombre_usuario' => 'marioandres114',
                'email' => 'marioandres114@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Mario Andrés',
                'apellidos' => 'Rodríguez',
                'cedula' => '87067965',
                'telefono' => '3148715370',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');


            /// lider 16
            $user = User::create([
                'nombre_usuario' => 'neymarcas1954',
                'email' => 'neymarcas1954@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'NEYLA MARIA ',
                'apellidos' => 'CASTILLO CENTENO',
                'cedula' => '27500268',
                'telefono' => '3136530838',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');


            /// lider 16


            /// lider 16
            $user = User::create([
                'nombre_usuario' => 'diani30',
                'email' => 'diani30@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'ROBERT HERNANDO',
                'apellidos' => 'LOPEZ CAICEDO',
                'cedula' => '13055993',
                'telefono' => '3166730860',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');


            /// lider 16
            $user = User::create([
                'nombre_usuario' => 'silvia_r_07',
                'email' => 'silvia_r_07@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'SILVIA ROCIO ',
                'apellidos' => 'LOPEZ GONGORA',
                'cedula' => '59667784',
                'telefono' => '3156321248',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 16
            $user = User::create([
                'nombre_usuario' => 'ximenadulce',
                'email' => 'ximenadulce@misena.edu.co',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Ximena ',
                'apellidos' => 'Dulce',
                'cedula' => '59828369',
                'telefono' => '3148198400',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            /// lider 16
            $user = User::create([
                'nombre_usuario' => 'yonnyc72',
                'email' => 'yonnyc72@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Yonny Edgardo',
                'apellidos' => 'Criollo Cardenas',
                'cedula' => '98352439',
                'telefono' => '3164405056',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Lider');

            ///////////////Coordinadores///////////////////
            /// coordinador 1
            $user = User::create([
                'nombre_usuario' => 'kellyrociobelalcazarpabon',
                'email' => 'kellyrociobelalcazarpabon@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Kelly Rocio',
                'apellidos' => 'Belalcazar Pabon',
                'cedula' => '59826859',
                'telefono' => '3017252527',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Coordinador');

            /// coordinador 2
            $user = User::create([
                'nombre_usuario' => 'natis954',
                'email' => 'natis954@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Natalia',
                'apellidos' => 'Ojeda',
                'cedula' => '1085323073',
                'telefono' => '3043895007',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Coordinador');

            /// coordinador 3
            $user = User::create([
                'nombre_usuario' => 'leidy911209',
                'email' => 'leidy911209@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Leidy Dayana',
                'apellidos' => 'Andrade',
                'cedula' => '1086294394',
                'telefono' => '3108769065',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Coordinador');

            /// coordinador 4
            $user = User::create([
                'nombre_usuario' => 'vivianalejandra1085',
                'email' => 'vivianalejandra1085@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Vivian Alejandra',
                'apellidos' => 'Delgado Miranda',
                'cedula' => '1085262021',
                'telefono' => '3165487656',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Coordinador');

            /// coordinador 5
            $user = User::create([
                'nombre_usuario' => 'isabel.rosero',
                'email' => 'isabel.rosero@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Isabel ',
                'apellidos' => 'Rosero Enriquez',
                'cedula' => '67011877',
                'telefono' => '3117463963',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Coordinador');

            ///////////////Colaboradores///////////////////
            /// Colaborador 1
            $user = User::create([
                'nombre_usuario' => 'camiloalomia84',
                'email' => 'camiloalomia84@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'CAMILO ENRIQUE',
                'apellidos' => 'ALOMIA LOPEZ',
                'cedula' => '87064924',
                'telefono' => '3007844618',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 2


            /// Colaborador 3
            $user = User::create([
                'nombre_usuario' => 'florg88',
                'email' => 'florg88@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Flor Stella',
                'apellidos' => 'Guerrero Pasichana',
                'cedula' => '30744345',
                'telefono' => '3175054625',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 4
            $user = User::create([
                'nombre_usuario' => 'yyerushalayimm',
                'email' => 'yyerushalayimm@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Eduardo Fabio',
                'apellidos' => 'Legarda Tupaz',
                'cedula' => '12998878',
                'telefono' => '3173744101',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 5
            $user = User::create([
                'nombre_usuario' => 'manuel123',
                'email' => 'manuel123@outlook.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Manuel Agustín',
                'apellidos' => 'Potosí Vallejo',
                'cedula' => '12987210',
                'telefono' => '3127236498',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 6
            $user = User::create([
                'nombre_usuario' => 'caiteran',
                'email' => 'caiteran@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Carmen Isabel',
                'apellidos' => 'Terán Bolaños',
                'cedula' => '27210245',
                'telefono' => '3186627738',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 7


            /// Colaborador 8
            $user = User::create([
                'nombre_usuario' => 'maurarmo',
                'email' => 'maurarmo@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Maura',
                'apellidos' => 'Arciniegas Moreno',
                'cedula' => '27469546',
                'telefono' => '3106056768',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 9
            $user = User::create([
                'nombre_usuario' => 'adryperez4',
                'email' => 'adryperez4@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Adriana Patricia',
                'apellidos' => 'Pérez Criollo',
                'cedula' => '59313680',
                'telefono' => '3004244562',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 10
            $user = User::create([
                'nombre_usuario' => 'alejam',
                'email' => 'alejam@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Alejandra',
                'apellidos' => 'Monsalve',
                'cedula' => '1152691437',
                'telefono' => '3113029856',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 11
            $user = User::create([
                'nombre_usuario' => 'alejobaezsion',
                'email' => 'alejobaezsion@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'ALEJANDRO STEVEN',
                'apellidos' => 'BAEZ GETIAL',
                'cedula' => '1085305065',
                'telefono' => '3114074763',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 12
            $user = User::create([
                'nombre_usuario' => 'roseroalixmarina1980',
                'email' => 'roseroalixmarina1980@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Alix Marina',
                'apellidos' => 'Rosero',
                'cedula' => '34000700',
                'telefono' => '3114376575',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 12+1
            $user = User::create([
                'nombre_usuario' => 'anampazvela',
                'email' => 'anampazvela@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Ana María',
                'apellidos' => 'Paz Vela',
                'cedula' => '1053868544',
                'telefono' => '3156200138',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 14
            $user = User::create([
                'nombre_usuario' => 'aurat_',
                'email' => 'aurat_@hotmail',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Aura Alicia',
                'apellidos' => 'Timana',
                'cedula' => '30739573',
                'telefono' => '3117712922',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 15
            $user = User::create([
                'nombre_usuario' => 'camila.velez12',
                'email' => 'camila.velez12@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Camila',
                'apellidos' => 'Velez Bolaños',
                'cedula' => '1085341495',
                'telefono' => '3184537411',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 16
            $user = User::create([
                'nombre_usuario' => 'carolinacaraisa',
                'email' => 'carolinacaraisa@yahoo.es',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Carolina',
                'apellidos' => 'Carabali Isajar',
                'cedula' => '34606498',
                'telefono' => '3127917439',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 17
            $user = User::create([
                'nombre_usuario' => 'nogueramezacamilo',
                'email' => 'nogueramezacamilo@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'CRISTIAN CAMILO',
                'apellidos' => 'NOGUERA MEZA',
                'cedula' => '1085340002',
                'telefono' => '3007669079',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 18
            $user = User::create([
                'nombre_usuario' => 'Heyda',
                'email' => 'Heyda@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Heyda',
                'apellidos' => 'Nieves Rodriguez',
                'cedula' => '37657499',
                'telefono' => '3116338762',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');


            /// Colaborador 19
            $user = User::create([
                'nombre_usuario' => 'jeancarlogranda10.1',
                'email' => 'jeancarlogranda10.1@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Jean Carlo ',
                'apellidos' => 'Granda',
                'cedula' => '1053863929',
                'telefono' => '3178306987',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 19
            $user = User::create([
                'nombre_usuario' => 'yesus03',
                'email' => 'yesus03@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Jesús Armando ',
                'apellidos' => 'Rosero',
                'cedula' => '1060646969',
                'telefono' => '3168149536',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 20
            $user = User::create([
                'nombre_usuario' => 'leidymarcelaosorio84',
                'email' => 'leidymarcelaosorio84@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Leidy Marcela ',
                'apellidos' => 'Osorio',
                'cedula' => '43188461',
                'telefono' => '3185416963',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 21
            $user = User::create([
                'nombre_usuario' => 'linnamarc',
                'email' => 'linnamarc@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Marcela ',
                'apellidos' => 'Londoño',
                'cedula' => '24343800',
                'telefono' => '3108941998',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 22
            $user = User::create([
                'nombre_usuario' => 'majovalenciarodriguez',
                'email' => 'majovalenciarodriguez@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'María José ',
                'apellidos' => 'Valencia',
                'cedula' => '1054857811',
                'telefono' => '3205166736',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');

            /// Colaborador 23
            $user = User::create([
                'nombre_usuario' => 'npph27',
                'email' => 'npph27@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Nancy Patricia',
                'apellidos' => 'Pérez',
                'cedula' => '59812825',
                'telefono' => '3127933044',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');


            /// Colaborador 23
            $user = User::create([
                'nombre_usuario' => 'paolamunozcontabilidad',
                'email' => 'paolamunozcontabilidad@gmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'Paola Andrea ',
                'apellidos' => 'Muñoz',
                'cedula' => '59826697',
                'telefono' => '3107437868',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');


            /// Colaborador 24
            $user = User::create([
                'nombre_usuario' => 'pauch_sm',
                'email' => 'pauch_sm@hotmail.com',
                'password' => bcrypt('123456'),
            ]);
            $persona = Personas::create([
                'nombres' => 'PAOLA SALAZAR ',
                'apellidos' => 'MEZA',
                'cedula' => '27088968',
                'telefono' => '3007769497',
                'fechaNacimiento' => '2000/02/20',
                'nacimientoHijos' => '2014',
                'estadoCivil' => 'Soltero',
                'escolaridad' => '4',
                'estudios' => 'Pendiente',
                'trabajo' => 'Pendiente',
                'idiomas' => '',
                'formacionMusical' => '',
                'fechaIngreso' => '2010/02/02',
                'recorridoIglesia' => '',
                'user_id' => $user->id,
            ]);
            $user->assignRole('Colaborador');


        }
    }
