<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfrendasydiezmos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofrendasydiezmos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('descripcion');
            $table->date('fecha');
            $table->string('ofrenda');
            $table->string('diezmo');

            $table->integer('iglesia_id')->unsigned()->nullable();
            $table->foreign('iglesia_id')->references('id')->on('iglesias');

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofrendasydiezmos');
    }

}
