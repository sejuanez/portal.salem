<?php

namespace App\Http\Controllers;

use App\empresas;
use App\Iglesias;
use App\Personas;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'nombres' => 'required|string',
            'apellidos' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'cedula' => 'required|string|unique:personas,cedula',
            'telefono' => 'required|string',
            'fechaNacimiento' => 'required|date',
            'nacimientoHijos' => 'required|string',
            'escolaridad' => 'required|string',
            'estudios' => 'required|string',
            'trabajo' => 'required|string',
            'idiomas' => 'required|string',
            'formacionMusical' => 'required|string',
            'fechaIngreso' => 'required|date',
            'recorridoIglesia' => 'required|string',
            'password' => 'required|string|min:8'
        ], [
            'nombres.required' => 'Los nombres son requeridos',
            'apellidos.required' => 'Los apellidos son requeridos',
            'email.required' => 'Email Requerido',
            'email.unique' => 'Este email ya se encuentra registrado, intente con otro',
            'cedula.unique' => 'Esta cedula se encuentra registrada en nuestra base de datos',
            'telefono.required' => 'telefono Requerido',
            'fechaNacimiento.required' => 'Fecha de nacimiento requerida',
            'nacimientoHijos.required' => 'Nacimiento de hijos requerido',
            'escolaridad.required' => 'Escolaridad requerida',
            'trabajo.required' => 'Trabajo requerido',
            'idiomas.required' => 'Idiomas requerido',
            'formacionMusical.required' => 'Formacion musical requerida',
            'fechaIngreso.required' => 'Fecha de ingreso a la iglesia requerida',
            'recorridoIglesia.required' => 'Recorrido en la iglesia requerido',
            'password.required' => 'Contraseña requerida'
        ]);
        $user = null;
        try {
            $error = null;
            DB::beginTransaction();

            $user = User::create([
                'nombre_usuario' => $request->email,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);

            $persona = Personas::create([
                'nombres' => $request->nombres,
                'apellidos' => $request->apellidos,
                'cedula' => $request->cedula,
                'telefono' => $request->indicativo . " " . $request->telefono,
                'fechaNacimiento' => $request->fechaNacimiento,
                'nacimientoHijos' => $request->nacimientoHijos,
                'estadoCivil' => $request->estadoCivil,
                'escolaridad' => $request->escolaridad,
                'estudios' => $request->estudios,
                'trabajo' => $request->trabajo,
                'idiomas' => $request->idiomas,
                'formacionMusical' => $request->formacionMusical,
                'fechaIngreso' => $request->fechaIngreso,
                'recorridoIglesia' => $request->recorridoIglesia,
                'coordinadorObra' => $request->coordinadorObra,
                'ciudadObra' => $request->ciudadObra,
                'direccionObra' => $request->direccionObra,
                'barrioObra' => $request->barrioObra,
                'nombreObra' => $request->nombreObra,
                'fechaObra' => $request->fechaObra,
                'liderAnterior' => $request->liderAnterior,
                'user_id' => $user->id,
            ]);
            $user->assignRole($request->tipoUsuario);
            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'persona' => $persona,
                'usuario' => $user,
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,

            );
        }//error

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */


    public function destroy($id)
    {
        //
    }

    function actualizarUsuario(Request $request)
    {

        $usuario = Personas::findOrFail($request->id);

        $this->validate($request, [
            'nombres' => 'required|string',
            'apellidos' => 'required|string',
            'telefono' => 'required|string',
            'fechaNacimiento' => 'required|date',
            'nacimientoHijos' => 'required|string',
            'escolaridad' => 'required|string',
            'estudios' => 'required|string',
            'trabajo' => 'required|string',
            'idiomas' => 'required|string',
            'formacionMusical' => 'required|string',
            'fechaIngreso' => 'required|date',
            'recorridoIglesia' => 'required|string'
        ], [
            'nombres.required' => 'Los nombres son requeridos',
            'apellidos.required' => 'Los apellidos son requeridos',
            'telefono.required' => 'telefono Requerido',
            'fechaNacimiento.required' => 'Fecha de nacimiento requerida',
            'nacimientoHijos.required' => 'Nacimiento de hijos requerido',
            'escolaridad.required' => 'Escolaridad requerida',
            'trabajo.required' => 'Trabajo requerido',
            'idiomas.required' => 'Idiomas requerido',
            'formacionMusical.required' => 'Formacion musical requerida',
            'fechaIngreso.required' => 'Fecha de ingreso a la iglesia requerida',
            'recorridoIglesia.required' => 'Recorrido en la iglesia requerido'
        ]);

        $usuario->update([
            'nombres' => $request->nombres,
            'apellidos' => $request->apellidos,
            'telefono' => $request->indicativo . " " . $request->telefono,
            'fechaNacimiento' => $request->fechaNacimiento,
            'nacimientoHijos' => $request->nacimientoHijos,
            'estadoCivil' => $request->estadoCivil,
            'escolaridad' => $request->escolaridad,
            'estudios' => $request->estudios,
            'trabajo' => $request->trabajo,
            'idiomas' => $request->idiomas,
            'formacionMusical' => $request->formacionMusical,
            'fechaIngreso' => $request->fechaIngreso,
            'recorridoIglesia' => $request->recorridoIglesia,
        ]);
        $response = array(
            'status' => 'success',
            'msg' => $usuario,
        );

        return response()->json($response);
    }

    function actualizarPass(Request $request)
    {


        if ($request->id) {
            $usuario = User::findOrFail($request->id);
            $this->validate($request, [
                'password' => 'required|string|min:8',
            ]);

            if (auth()->user()->rol_id != 1 && ($usuario->rol_id == 1 || $usuario->rol_id == 2 || $usuario->rol_id == 3)) {
                $response = array(
                    'status' => 'Error',
                    'msg' => "Error, Falta de privilegios",
                );
                return response()->json($response);

            } else {
                $usuario->password = bcrypt($request->pass);
                $usuario->save();
                $response = array(
                    'status' => 'success',
                    'msg' => 'la Contraseña se ha cambiado correctamente',
                );
                return response()->json($response);

            }

        } else {

            $usuario = User::findOrFail(auth()->user()->id);
            $this->validate($request, [
                'passOld' => 'required|string',
                'pass' => 'required|string|min:6',
            ]);

            if (Hash::check($request->passOld, $usuario->password)) {

                $usuario->password = bcrypt($request->pass);
                $usuario->save();

                $response = array(
                    'status' => 'success',
                    'msg' => 'la Contraseña se ha cambiado correctamente',
                );

            } else {
                $response = array(
                    'status' => 'Error',
                    'status2' => Hash::check($request->passOld, $usuario->password),
                    'msg' => "la Contraseña actual no es correcta",
                );
            }

            return response()->json($response);

        }

    }

    function getPastores()
    {
        $users = User::get();
        //return $users;
        //return datatables()->collection(User::get())->toJson();
        $array = array();

        foreach ($users as $usuario) {

            $datos = [
                'id' => $usuario->id,
                'nombre' => $usuario->name,
                'correo' => $usuario->email,
                'rol' => $usuario->roles,
                'fecha_registro' => date("d/m/Y h:i A", strtotime($usuario->created_at)),
                'estado' => $usuario->active ? '<span class="help-block text-success">Activo</span>' : '<span class="help-block text-danger">Inactivo</span>',
            ];

            if (in_array("Pastor", $usuario->getRoleNames()->toArray())) {
                array_push($array, $datos);
            }

            //no dejar ver los super admin si no eres
        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    function getPastoresLibres()
    {
        $users = User::get();
        $array = array();

        foreach ($users as $usuario) {

            $datos = [
                'id' => $usuario->id,
                'nombre' => $usuario->persona->nombres . ' ' . $usuario->persona->apellidos,
                'correo' => $usuario->email,
                'rol' => $usuario->roles,
                'fecha_registro' => date("d/m/Y h:i A", strtotime($usuario->created_at)),
                'estado' => $usuario->active ? '<span class="help-block text-success">Activo</span>' : '<span class="help-block text-danger">Inactivo</span>',
            ];

            if (in_array("Pastor", $usuario->getRoleNames()->toArray()) && $usuario->persona->iglesia == null && $usuario->active == true) {
                array_push($array, $datos);
            }


            //no dejar ver los super admin si no eres
        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    function getCoordinadores()
    {
        $users = User::get();
        //return $users;
        //return datatables()->collection(User::get())->toJson();
        $array = array();

        foreach ($users as $usuario) {

            $datos = [
                'id' => $usuario->id,
                'nombre' => $usuario->name,
                'correo' => $usuario->email,
                'rol' => $usuario->roles,
                'fecha_registro' => date("d/m/Y h:i A", strtotime($usuario->created_at)),
                'estado' => $usuario->active ? '<span class="help-block text-success">Activo</span>' : '<span class="help-block text-danger">Inactivo</span>',
            ];

            if (in_array("Coordinador", $usuario->getRoleNames()->toArray())) {
                array_push($array, $datos);
            }

            //no dejar ver los super admin si no eres
        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    function getCoordinadoresLibres()
    {
        $users = User::get();
        $array = array();

        foreach ($users as $usuario) {

            $datos = [
                'id' => $usuario->id,
                'nombre' => $usuario->persona->nombres . ' ' . $usuario->persona->apellidos,
                'correo' => $usuario->email,
                'rol' => $usuario->roles,
                'fecha_registro' => date("d/m/Y h:i A", strtotime($usuario->created_at)),
                'estado' => $usuario->active ? '<span class="help-block text-success">Activo</span>' : '<span class="help-block text-danger">Inactivo</span>',
            ];

            if (in_array("Coordinador", $usuario->getRoleNames()->toArray()) && $usuario->persona->iglesia == null && $usuario->active == true) {
                array_push($array, $datos);
            }

            //no dejar ver los super admin si no eres
        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    function getLideres()
    {
        $users = User::get();
        //return $users;
        //return datatables()->collection(User::get())->toJson();
        $array = array();

        foreach ($users as $usuario) {

            $datos = [
                'id' => $usuario->id,
                'nombre' => $usuario->name,
                'correo' => $usuario->email,
                'rol' => $usuario->roles,
                'fecha_registro' => date("d/m/Y h:i A", strtotime($usuario->created_at)),
                'estado' => $usuario->active ? '<span class="help-block text-success">Activo</span>' : '<span class="help-block text-danger">Inactivo</span>',
            ];

            if (in_array("Lider", $usuario->getRoleNames()->toArray())) {
                array_push($array, $datos);
            }

            //no dejar ver los super admin si no eres
        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    function getColaboradores()
    {
        $users = User::get();
        //return $users;
        //return datatables()->collection(User::get())->toJson();
        $array = array();

        foreach ($users as $usuario) {

            $datos = [
                'id' => $usuario->id,
                'nombre' => $usuario->name,
                'correo' => $usuario->email,
                'rol' => $usuario->roles,
                'fecha_registro' => date("d/m/Y h:i A", strtotime($usuario->created_at)),
                'estado' => $usuario->active ? '<span class="help-block text-success">Activo</span>' : '<span class="help-block text-danger">Inactivo</span>',
            ];

            if (in_array("Colaborador", $usuario->getRoleNames()->toArray())) {
                array_push($array, $datos);
            }

            //no dejar ver los super admin si no eres
        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    function getColaboradoresLibres()
    {
        $users = User::get();
        $array = array();

        foreach ($users as $usuario) {

            $datos = [
                'id' => $usuario->id,
                'nombre' => $usuario->persona->nombres . ' ' . $usuario->persona->apellidos,
                'correo' => $usuario->email,
                'rol' => $usuario->roles,
                'fecha_registro' => date("d/m/Y h:i A", strtotime($usuario->created_at)),
                'estado' => $usuario->active ? '<span class="help-block text-success">Activo</span>' : '<span class="help-block text-danger">Inactivo</span>',
            ];

            if (in_array("Colaborador", $usuario->getRoleNames()->toArray()) && $usuario->persona->iglesia == null && $usuario->active == true) {
                array_push($array, $datos);
            }

            //no dejar ver los super admin si no eres
        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    function verificarUsuario(Request $request)
    {

        $usuario = User::findOrFail($request->id);

        $error = null;
        DB::beginTransaction();
        try {

            $usuario->active = !$usuario->active;
            $usuario->save();
            DB::commit();

            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => ($usuario),
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }

}
