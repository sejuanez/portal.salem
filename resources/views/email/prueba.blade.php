<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Correo de Bienvenida</title>
</head>
<body>
<h2>Bienvenid@, {{ $usuario->persona->nombres }} {{ $usuario->persona->apellidos }}!</h2>

<p>Estos son tus datos de ingresoa la plataforma:</p>
<ul>
    <li>Email: {{ $usuario->email }}</li>
    <li>Contraseña: 123456</li>
    <li>Plataforma: http://portal.ministerioapostolicosalem.com/</li>

</ul>
<p>Recuerda cambiar tu contraseña al ingresar a la plataforma</p>
<p>Esperemos que nuestra aplicación te sea de gran utilidad!</p>

Enviado automáticamente desde <a href='http://portal.ministerioapostolicosalem.com/' title="portalSalem">Portal
    Salem</a>

</body>
</html>
