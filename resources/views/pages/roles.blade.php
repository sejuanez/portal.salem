@extends('layouts.dashboard')

@section('content')

    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Roles</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Roles</a>
                    </li>
                </ul>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">Roles del sistema</h4>
                                @can('crear.roles')
                                <button class="btn btn-primary btn-round ml-auto" id="modalGuardar">
                                    <i class="fa fa-plus"></i>
                                    Nuevo Rol
                                </button>
                                @endcan
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <table id="tablaEmpleados" class="display table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Tipo</th>
                                        <th>Creado</th>
                                        <th>Actualizado</th>
                                        <th class="text-right">Opciones</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Tipo</th>
                                        <th>Creado</th>
                                        <th>Actualizado</th>
                                        <th class="text-right">Opciones</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages.modal.roles')

@endsection

@section('scripts')
    <script type="text/javascript">

        var TABLA = $('#tablaEmpleados').DataTable({
            "ajax": {
                "url": "{{ url('recurso/roles') }}",
                "type": "GET",
                "dataSrc": function (data) {
                    var json = [];
                    console.log(data);
                    for (var item in data.msg) {
                        var itemJson = {
                            Id: data.msg[item].id,
                            Nombre: data.msg[item].name,
                            Tipo: data.msg[item].guard_name,
                            Creado: data.msg[item].created_at,
                            Actualizado: data.msg[item].updated_at,
                            Permisos: data.msg[item].permisos,
                            Opciones: opciones()
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            columns: [
                {data: "Nombre"},
                {data: "Tipo"},
                {data: "Creado"},
                {data: "Actualizado"},
                {data: "Opciones"},
            ],
        });

        function opciones() {
            var opciones = '';
            @can('editar.roles')
            opciones = '<button type="button" class="btn btn-primary btn-xs editar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Editar" data-original-title="Edit"' +
                '           style="margin-right: 5px;">' +
                '           <i class="fas fa-pen"></i>' +
                ' </button>';
            @endcan
            @can('eliminar.roles')
            opciones += '' +
                '<button type="button" class="btn btn-danger btn-xs eliminar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Eliminar" data-original-title="Edit">' +
                '           <i class="fas fa-trash"></i>' +
                ' </button>';
            @endcan
            return opciones;
        }

        @can('eliminar.roles')
        TABLA.on('click', '.eliminar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            $("#modal").modal('show');
            $('#form-rol')[0].reset();
            $("#accion-modal").html('Eliminar');
            $("#id").val(data.Id);
        });
        @endcan

        @can('crear.roles')
        $("#modalGuardar").on('click', function () {
            $('#modal').modal('show');
            $('#form-rol')[0].reset();
            $('#actualizar').hide();
            $('#eliminar').hide();
            $('#guardar').show();
            $("#error").hide();
            $("#accion-modal").html('Registrar');
        });
        $("#guardar").on('click', function () {
            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#guardar").prop('disabled', true);
            $.ajax({
                url: '{{url('recurso/roles')}}',
                type: 'POST',
                data: $("#form-rol").serialize(),

            }).done(function (response) {
                console.log(response);
                $('#form-rol')[0].reset();
                $('#modal').modal('hide');
                $("#guardar").prop('disabled', false);
                TABLA.ajax.reload();

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.message,
                },{
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#guardar").prop('disabled', false);
            });

        });
        @endcan

        @can('editar.roles')
        TABLA.on('click', '.editar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            $("#modal").modal('show');
            $('#form-rol')[0].reset();
            $('#actualizar').show();
            $('#eliminar').hide();
            $('#guardar').hide();
            $("#accion-modal").html('Editar');

            $("#id").val(data.Id);
            $("#nombre").val(data.Nombre);
            console.log(data.Permisos);
            //señalar permisos
            for (var item in data.Permisos) {
                $("#permiso"+data.Permisos[item].id).prop('checked',true);
            }
        });
        $("#actualizar").on('click', function () {
            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#actualizar").prop('disabled', true);
            $.ajax({
                url: '{{url('recurso/roles')}}/'+$('#id').val(),
                type: 'PUT',
                data: $("#form-rol").serialize(),

            }).done(function (response) {
                $('#form-rol')[0].reset();
                $('#modal').modal('hide');
                TABLA.ajax.reload();

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.message,
                },{
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#actualizar").prop('disabled', false);
            });

        });
        @endcan

    </script>
@endsection
