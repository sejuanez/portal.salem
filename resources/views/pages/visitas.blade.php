@extends('layouts.dashboard')

@section('content')

    <div class="content container-full">

        <div class="page-navs bg-white">
            <div class="nav-scroller">
                <div
                    class="nav nav-tabs nav-line nav-color-secondary d-flex align-items-center justify-contents-center w-100">
                    <a class="nav-link active show" data-toggle="tab" href="#tab1" id="nav_visitas">Visitas Pendientes
                    </a>
                    <a class="nav-link mr-5" data-toggle="tab" href="#tab2" id="nav_visitas_finalizadas">Visitas
                        Finalizados</a>
                    <div class="ml-auto">
                        @if(session()->get('id_iglesia', function () {return 'default';}) !="default")
                            @can('crear.visitas')
                                <button class="btn btn-primary btn-round ml-auto" id="nueva-visita">
                                    <i class="fa fa-plus"></i>
                                    Nueva Visita
                                </button>
                            @endcan
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="page-inner">
            <div class="tab-content mt-2 mb-3" id="card-visitas">
                <div class="tab-pane fade active show" id="tab1" role="tabpanel" aria-labelledby="pills-home-tab">

                    <div>
                        <div class="card">
                            <div class="card-body">

                                <div class="table-responsive">
                                    <table id="tablaVisitasPendientes" class="display table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Iglesia</th>
                                            <th>Fecha</th>
                                            <th>Coordinador</th>
                                            <th>Actualizado</th>
                                            <th class="text-right">Opciones</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Iglesia</th>
                                            <th>Fecha</th>
                                            <th>Coordinador</th>
                                            <th>Actualizado</th>
                                            <th class="text-right">Opciones</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div>
                        <div class="card">
                            <div class="card-body">

                                <div class="table-responsive">
                                    <table id="tablaVisitasTerminadas" class="display table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Iglesia</th>
                                            <th>Fecha</th>
                                            <th>Coordinador</th>
                                            <th>Respuestas</th>
                                            <th>Opc</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Iglesia</th>
                                            <th>Fecha</th>
                                            <th>Coordinador</th>
                                            <th>Respuestas</th>
                                            <th>Opc</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="card-registro" style="display: none">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <h4 class="card-title">
                                <span class="fw-mediumbold" id="accion-modal">Nueva</span>
                                <span class="fw-light">Visita</span>
                            </h4>
                        </div>
                    </div>
                    <div class="card-body">

                        <form id="visita-form">
                            @csrf
                            <input type="hidden" id="id" name="id">

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="select_coordinador">Coordinador</label>
                                        <select class="form-control" name="select_coordinador" id="select_coordinador"
                                                style="width: 100%">
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="fecha_realizado">Fecha de realización</label>
                                        <input type="text" class="form-control" id="fecha_realizado"
                                               name="fecha_realizado"
                                               required="">
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger text-danger" id="error" style="display: none">
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>

                    <div class="card-footer text-right">
                        <button type="button" id="guardar" class="btn btn-primary">Guardar</button>
                        <button type="button" id="actualizar" class="btn btn-primary">Actualizar</button>
                        <button type="button" id="cancelar-registro" class="btn btn-danger">Cancelar</button>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        var ID_IGLESIA = null;
        $('#card-registro').hide();
        //graficos//
        $('#fecha_realizado').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        var TABLA = $('#tablaVisitasPendientes').DataTable({
            scrollY: 400,
            scrollCollapse: true,
            autofill: true,
            "ajax": {
                "url": "/recurso/getVisitasPendientes",
                "type": "GET",
                "dataSrc": function (data) {

                    var json = [];
                    console.log(data);
                    for (var item in data.msg) {
                        var itemJson = {
                            Id: data.msg[item].visita.id,
                            Fecha: data.msg[item].visita.fecha_visita,
                            Coordinador: data.msg[item].coordinador.nombres + ' ' + data.msg[item].coordinador.apellidos,
                            coordinador_id: data.msg[item].coordinador_id,
                            Actualizado: data.msg[item].visita.updated_at,
                            Iglesia: data.msg[item].iglesia,
                            Opciones: opciones(1)
                        };
                        json.push(itemJson)
                    }
                    return json;
                    $('#card-iglesias .card').removeClass("is-loading");
                }
            },
            columns: [
                {data: "Id"},
                {data: "Iglesia"},
                {data: "Fecha"},
                {data: "Coordinador"},
                {data: "Actualizado"},
                {data: "Opciones"},
            ],
        });

        var TABLA_TERMINADA = $('#tablaVisitasTerminadas').DataTable({
            scrollX: true,
            responsive: true,
            "ajax": {
                "url": "/recurso/getVisitasTerminadas",
                "type": "GET",
                "dataSrc": function (data) {

                    var json = [];
                    console.log(data);
                    for (var item in data.msg) {

                        var estado = (data.msg[item].estado == 0 ? '<span class="badge badge-danger">Inactiva</span>' : '<span class="badge badge-success">Activa</span>');
                        var preguntas = ['limpio',
                            'cartelera',
                            'alfoli',
                            'host_atten',
                            'host_people_loc',
                            'music',
                            'dance',
                            'distr',
                            'music_love',
                            'music_rep',
                            'bible_use',
                            'reflex',
                            'pray_time',
                            'leader_vision',
                            'balance',
                            'unction_preach',
                            'jesus_center',
                            'pulpit_admin',
                            'preach_ability',
                            'god_depend',
                            'leader_love_work',
                            'personal_presentation',
                            'op_sex_behavior',
                            'child_site',
                            'child_atten',
                            'teacher_prep',
                            'minis_unction',
                            'minis_music',
                            'minis_child',
                            'leader_atten',
                            'people_close_to_leader',
                            'spouse_behavior',
                            'subjection',
                            'intersec',
                            'inversiones',
                            'sat_assist',
                            'inicio',
                            'alabanza',
                            'oracion',
                            'predicacion',
                            'maestro_ninos',
                            'ministracion'
                        ];
                        var preguntas_largas = ['Aseo en la instalación',
                            'Hay cartelera con la información general de Iglesias y grupos familiares del ministerio',
                            'Hay Alfolí',
                            'El anfitrión o ujier atiende a la gente con amabilidad',
                            'El anfitrión o ujier ubica a las personas',
                            'Ejecución musical',
                            'Idoneidad danzas',
                            'Distribución espacial del equipo',
                            'disposición para hacer la obra con amor (Alabanza)',
                            'Repertorio musical conforme al ministerio',
                            'Al orar usa la Bíblia',
                            'La reflexión se hace conforme al versículo leído',
                            'El tiempo de oración es adecuado',
                            'Está identificado con la Visión de la Iglesia (Id y hacer discípulos a todas las naciones)',
                            'La predicación es balanceada, enseñanza centrada en la Bíblia, testimonios, ejémplos Bíblicos, revelación',
                            'Mueve la unción (Predicación)',
                            'El centro es el Señor Jesús',
                            'Sabe manejar el púlpito y la congregación:',
                            'Hay habilidad en la predicación (Dicción y Elocución)',
                            'Se nota dependencia de Dios y sujeción a las Autoridades',
                            'Hace su labor con amor y diligencia',
                            'Presentación personal',
                            'Relación con el sexo opuesto',
                            'Adecuación del sitio (Cuarto niños)',
                            'Dinamismo del maestro, atención y participación de los niños',
                            'Preparación del maestro',
                            'Unción y respaldo del Espiritu Santo en lo que declara',
                            'Acompañamiento musical',
                            'Comportamiento niños',
                            'Disposición del Pastor/Líder para atender al pueblo',
                            'Acercamiento del pueblo al Pastor/Líder al final de la reunión',
                            'Comportamiento del conyugue frente a la congregación',
                            'Sujeción del equipo de trabajo al Pastor/Lider',
                            'Equipo de trabajo en interseción',
                            'Inversiones',
                            'Asistencia del grupo de trabajo a discipulado (sábado)',
                            'inicio',
                            'alabanza',
                            'oracion',
                            'predicacion',
                            'Maestro de niños',
                            'ministracion'
                        ];
                        var respuestas = '<div class="row gutters-xs">';
                        for (var item2 in preguntas) {
                            if (data.msg[item].visita[preguntas[item2]] === "Si, Bueno") {
                                respuestas += '<span class="colorinput-color bg-black" style="background: green !important;" title="' + preguntas_largas[item2] + '"></span>';
                            } else if (data.msg[item].visita[preguntas[item2]] === "Regular") {
                                respuestas += '<span class="colorinput-color bg-black" style="background: orange !important;" title="' + preguntas_largas[item2] + '"></span>';
                            } else if (data.msg[item].visita[preguntas[item2]] === "No, Insatisfactorio") {
                                respuestas += '<span class="colorinput-color bg-black" style="background: red !important;" title="' + preguntas_largas[item2] + '"></span>';
                            } else if (data.msg[item].visita[preguntas[item2]] === "No Aplica") {
                                respuestas += '<span class="colorinput-color bg-black" style="background: gray !important;" title="' + preguntas_largas[item2] + '"></span>';
                            } else if (data.msg[item].visita[preguntas[item2]] === "No Evaluado") {
                                respuestas += '<span class="colorinput-color bg-black" style="background: gray !important;" title="' + preguntas_largas[item2] + '"></span>';
                            }

                        }
                        respuestas += '</div>';
                        var itemJson = {
                            Id: data.msg[item].visita.id,
                            Fecha: data.msg[item].visita.fecha_visita,
                            Coordinador: data.msg[item].coordinador.nombres + ' ' + data.msg[item].coordinador.apellidos,
                            Actualizado: data.msg[item].visita.updated_at,
                            Respuestas: respuestas,
                            Opciones: opciones2(),
                            Iglesia: data.msg[item].iglesia,
                        };
                        json.push(itemJson)
                    }
                    return json;
                    $('#card-iglesias .card').removeClass("is-loading");
                }
            },
            columns: [
                {data: "Id"},
                {data: "Iglesia"},
                {data: "Fecha"},
                {data: "Coordinador"},
                {data: 'Respuestas'},
                {data: 'Opciones'},
            ],
        });

        function opciones() {
            var opciones = '';
            @can('editar.visitas')
                opciones += '<button type="button" class="btn btn-primary btn-xs editar" ' +
                '           title="Actualizar"' +
                '           style="margin-right: 5px;">\n' +
                '           <i class="fas fa-pen"></i>\n' +
                ' </button>';
            @endcan
                opciones += '' +
                '<button type="button" class="btn btn-success btn-xs encuesta" ' +
                '           title="Ver Encuesta"' +
                '           style="margin-right: 5px;">\n' +
                '           <i class="fas fa-eye"></i>\n' +
                ' </button>';

            @can('eliminar.visitas')
                opciones += '' +
                '<button type="button" class="btn btn-danger btn-xs cambiarEstado" ' +
                '             title="Eliminar">' +
                '           <i class="fas fa-trash"></i>\n' +
                ' </button>';
            @endcan
                return opciones;
        }

        function opciones2() {
            var opciones = '';
            @can('reEditar.visitas')
                opciones += '<button type="button" class="btn btn-primary btn-xs editar" ' +
                '           title="Re Editar Visita"' +
                '           style="margin-right: 5px;">\n' +
                '           <i class="fas fa-pen"></i>\n' +
                ' </button>';
            @endcan
                @can('realizar.visitas')
                opciones += '' +
                '<button type="button" class="btn btn-success btn-xs encuesta" ' +
                '           title="Ver Encuesta"' +
                '           style="margin-right: 5px;">\n' +
                '           <i class="fas fa-eye"></i>\n' +
                ' </button>';
            @endcan

                return opciones;
        }

        $("#cancelar-registro").on('click', function () {

            $('#card-registro').hide();
            $('#card-visitas').show();
            $('#card-visitas').addClass('animated fadeIn');

            $('#visita-form')[0].reset();

        });

        TABLA.on('click', '.ver', function () {

            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            console.log(data);

            window.location.href = '{{url('iglesias')}}/' + data.Id;

        });

        @can('crear.visitas')

        $("#nueva-visita").on('click', function () {
            $('#card-visitas').hide();
            $('#card-registro').show();
            $('#card-registro').addClass('animated fadeIn');
            $('#select_coordinador').val('').trigger("change");

            $('#visita-form')[0].reset();
            $('#actualizar').hide();
            $('#eliminar').hide();
            $('#guardar').show();
            $("#error").hide();
            $("#accion-modal").html('Registrar');

        });

        $("#guardar").on('click', function () {

            let formData = new FormData(document.getElementById("visita-form"));
            //formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
            let csrfToken = '{{ csrf_token() }}';
            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#guardar").prop('disabled', true);
            $.ajax({
                url: '/recurso/crearVisita',
                type: 'POST',
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                data: formData,
                processData: false,
                contentType: false,
                cache: false

            }).done(function (response) {
                $('#visita-form')[0].reset();
                $("#guardar").prop('disabled', false);
                TABLA.ajax.reload();

                $('#card-registro').hide();
                $('#card-visitas').show();
                $('#card-visitas').addClass('animated fadeIn');

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: 'Visita Generada Correctamente',
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#guardar").prop('disabled', false);
            });


        });

        function getCoordinador() {

            let COORDINADOR = [];

            $.ajax({
                url: "/recurso/verMiembrosCoordinadores",
                async: false
            }).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].nombres + ' ' + data.msg[item].apellidos
                    };
                    COORDINADOR.push(itemSelect2)
                }
                $('#select_coordinador').select2({
                    theme: "bootstrap",
                    placeholder: "Selecciona un coordinador",
                    allowClear: true,
                    data: COORDINADOR
                });

            });

        }

        @if(session()->get('id_iglesia', function () {return 'default';}) !="default")
        getCoordinador();
        @endif

        @endcan

        @can('editar.visitas')

        TABLA.on('click', '.editar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();

            $("#actualizar").prop('disabled', false);
            $('#visita-form')[0].reset();

            $('#actualizar').show();
            $('#eliminar').hide();
            $('#guardar').hide();
            $("#error").hide();
            $("#accion-modal").html('Editar');

            $('#card-visitas').hide();
            $('#card-registro').show();
            $('#card-registro').addClass('animated fadeIn');
            ID_IGLESIA = data.Id;
            $("#id").val(data.Id);
            $('#fecha_realizado').val(data.Fecha);
            $('#select_coordinador').val(data.coordinador_id).trigger('change');

        });

        $("#actualizar").on('click', function () {

            console.log(ID_IGLESIA);
            let formData = new FormData(document.getElementById("visita-form"));

            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#actualizar").prop('disabled', true);
            $.ajax({
                url: "recurso/actualizarVisita",
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                cache: false
            }).done(function (response) {

                console.log(response);

                TABLA.ajax.reload();

                $('#card-registro').hide();
                $('#card-visitas').show();
                $('#card-visitas').addClass('animated fadeIn');

                $('#visita-form')[0].reset();

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.msg,
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#actualizar").prop('disabled', false);
            });

        });

        @endcan

        @can('realizar.visitas')
        @endcan

        TABLA.on('click', '.encuesta', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            window.location.href = '{{url('cuestionario')}}/' + data.Id;

        });
        TABLA_TERMINADA.on('click', '.encuesta', function () {
            $tr = $(this).closest('tr');
            var data = TABLA_TERMINADA.row($tr).data();
            window.location.href = '{{url('cuestionario')}}/' + data.Id;

        });


        @can('eliminar.visitas')

        TABLA.on('click', '.cambiarEstado', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            let opcion = "";

            swal({
                title: 'Estas seguro?',
                text: "Vas a eliminar esta visita",
                icon: 'warning',
                buttons: {
                    confirm: {
                        text: 'Si, Eliminar',
                        className: 'btn btn-success'
                    },
                    cancel: {
                        text: 'Cancelar',
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((Delete) => {
                if (Delete) {

                    $.ajax({
                            url: 'recurso/eliminarVisita',
                            type: 'POST',
                            data: {
                                id: data.Id,
                                _token: $('meta[name="csrf-token"]').attr('content')
                            },

                        }
                    ).done(function (response) {
                        console.log(response);
                        if (response.status == "Error") {
                            //error
                        } else {
                            TABLA.ajax.reload();
                            swal({
                                title: 'Ok!',
                                text: 'La visita ha sido eliminada con exito',
                                icon: 'success',
                                buttons: {
                                    confirm: {
                                        className: 'btn btn-success'
                                    }
                                }
                            });
                        }
                        //return response;
                    }).fail(function (error) {

                        console.log(error);

                    });


                } else {
                    swal.close();
                }
            });

        });
        @endcan

        @can('reEditar.visitas')
        TABLA_TERMINADA.on('click', '.editar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA_TERMINADA.row($tr).data();

            swal({
                title: 'Estas seguro?',
                text: "Vas a marcar pendiente esta visita ",
                icon: 'warning',
                buttons: {
                    confirm: {
                        text: 'Si',
                        className: 'btn btn-success'
                    },
                    cancel: {
                        text: 'Cancelar',
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((Delete) => {
                if (Delete) {

                    $.ajax({
                            url: 'recurso/cambioPendienteVisita',
                            type: 'POST',
                            data: {
                                id: data.Id,
                                _token: $('meta[name="csrf-token"]').attr('content')
                            },

                        }
                    ).done(function (response) {
                        console.log(response);
                        if (response.status == "Error") {
                            //error
                        } else {

                            TABLA.ajax.reload();
                            TABLA_TERMINADA.ajax.reload();
                            swal({
                                title: 'Ok!',
                                text: 'La visita se ha pasado a pendiente',
                                icon: 'success',
                                buttons: {
                                    confirm: {
                                        className: 'btn btn-success'
                                    }
                                }
                            });
                        }
                        //return response;
                    }).fail(function (error) {
                        console.log(error);
                    });
                } else {
                    swal.close();
                }
            });

        });

        @endcan

        $('#nav_visitas_finalizadas').on('click', function () {
            setTimeout(function () {
                TABLA_TERMINADA.draw();
                console.log('draw');
            }, 500);
        });

        $('#nav_visitas').on('click', function () {
            setTimeout(function () {
                TABLA.draw();
                console.log('draw');
            }, 500);
        });
    </script>
@endsection
