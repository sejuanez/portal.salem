<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="accion-modal">Nuevo</span>
                    <span class="fw-light">Usuario</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-usuario">
                    @csrf
                    <input type="hidden" id="id">
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name" class="text-right">
                                    Nombre
                                    <span class="required-label">*</span>
                                </label>
                                <input type="text" class="form-control" id="nombre" name="name"
                                       placeholder="Nombres Y Apellidos" required="">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name" class="text-right">
                                    Correo
                                    <span class="required-label">*</span>
                                </label>
                                <input type="email" class="form-control" id="correo" name="email"
                                       placeholder="Correo Electronico" required="">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name" class="text-right">Contraseña</label>
                                <input type="password" class="form-control" id="password" name="password"
                                       required="">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="select-tipo">Rol</label>
                                <select class="form-control" id="rol" name="rol">
                                    @foreach($roles as $rol)
                                        <option value="{{$rol->name}}">{{$rol->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="alert alert-danger text-danger" id="error" style="display: none">
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="guardar" class="btn btn-primary">Guardar</button>
                <button type="button" id="actualizar" class="btn btn-primary">Actualizar</button>
                <button type="button" id="eliminar" class="btn btn-primary">Eliminar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
