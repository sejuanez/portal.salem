@extends('layouts.dashboard')

@section('content')


    <div class="content">


        @if((auth()->user()->persona->miembro!=null && (session()->get('id_iglesia', function () {return 'default';})) !="default")  || auth()->user()->getRoleNames()[0]=='Super Admin' || auth()->user()->getRoleNames()[0]=='Admin')
            <div class="panel-header bg-primary-gradient">
                <div class="page-inner py-5">
                    <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                        <div>
                            <h2 class="text-white pb-2 fw-bold">Dashboard</h2>
                            <h5 class="text-white op-7 mb-2">Bienvenido al portal salem</h5>
                        </div>

                        {{--                        <div class="ml-md-auto py-2 py-md-0">--}}
                        {{--                            <a href="#" class="btn btn-white btn-border btn-round mr-2">Manage</a>--}}
                        {{--                            <a href="#" class="btn btn-secondary btn-round">Add Customer</a>--}}
                        {{--                        </div>--}}

                    </div>
                </div>
            </div>
            <div class="page-inner mt--5">

                <div class="dashboard">
                    <div class="row mt--2">
                        <div class="col-md-4">
                            <div class="card full-height">
                                <div class="card-body">
                                    <div class="card-title">Estadísticas generales</div>
                                    <div class="card-category">Información diaria sobre estadísticas en el sistema.
                                    </div>
                                    <div class="d-flex flex-wrap justify-content-around pb-2 pt-4">
                                        <div class="px-2 pb-2 pb-md-0 text-center">
                                            <div id="circles-1"></div>
                                            <h6 class="fw-bold mt-3 mb-0">Usuarios</h6>
                                        </div>
                                        <div class="px-2 pb-2 pb-md-0 text-center">
                                            <div id="circles-2"></div>
                                            <h6 class="fw-bold mt-3 mb-0">Reuniones Pendientes</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card full-height">
                                <div class="card-body">
                                    <div class="card-title">Total ingresos de este mes</div>
                                    <div class="row py-3">
                                        <div class="col-md-4 d-flex flex-column justify-content-around">
                                            <div>
                                                <h6 class="fw-bold text-uppercase text-success op-8">Total Ingresos</h6>
                                                <h3 class="fw-bold">$ {{number_format($diezmo+$ofrenda)}}</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card full-height">
                                <div class="card-body">
                                    <div class="card-title">Pastores y Coordinadores</div>
                                    <div class="row py-3">
                                        <div class="col-md-12 d-flex flex-column justify-content-around">
                                            <div>
                                                <h6 class="fw-bold text-uppercase text-success op-8">Pastores</h6>
                                                <h3 class="fw-bold">
                                                    @foreach($pastores as $persona)
                                                        <p>{{$persona->nombres}} {{$persona->apellidos}}</p>
                                                    @endforeach
                                                </h3>
                                                <h6 class="fw-bold text-uppercase text-success op-8">Coordinadores</h6>
                                                <h3 class="fw-bold">
                                                    @foreach($coordinadores as $persona)
                                                        <p>{{$persona->nombres}} {{$persona->apellidos}}</p>
                                                    @endforeach
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="card full-height">
                                <div class="card-header">
                                    <div class="card-title">Reuniones Pendientes</div>
                                </div>
                                <div class="card-body">
                                    <ol class="activity-feed">

                                        @foreach($reuniones as $reunion)

                                            <li class="feed-item feed-item-primary">
                                                <time class="date"
                                                      datetime="">{{  \Carbon\Carbon::parse($reunion->created_at)->diffForHumans()}}
                                                </time>
                                                <span class="text">{{$reunion->nombre}} </span>
                                            </li>
                                        @endforeach

                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card full-height">
                                <div class="card-header">
                                    <div class="card-title">Reuniones Finalizadas</div>
                                </div>
                                <div class="card-body">
                                    <ol class="activity-feed">

                                        @foreach($reunionesCompletadas as $reunion)

                                            <li class="feed-item feed-item-secondary">
                                                <time class="date"
                                                      datetime="">{{  \Carbon\Carbon::parse($reunion->created_at)->diffForHumans()}}
                                                </time>
                                                <span class="text">{{$reunion->nombre}} </span>
                                            </li>
                                        @endforeach


                                    </ol>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <div class="card full-height">
                                <div class="card-header">
                                    <div class="card-title">Visitas Pendientes</div>
                                </div>
                                <div class="card-body">
                                    <ol class="activity-feed">

                                        @foreach($visitasPendientes as $visita)

                                            <li class="feed-item feed-item-primary">
                                                <time class="date"
                                                      datetime="">{{  \Carbon\Carbon::parse($visita->created_at)->diffForHumans()}}
                                                </time>
                                                <span
                                                    class="text">Encargado: {{$visita->coordinador->persona->nombres." ".$visita->coordinador->persona->apellidos}} </span>
                                            </li>
                                        @endforeach

                                    </ol>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="card full-height">
                                <div class="card-header">
                                    <div class="card-title">Visitas Finalizadas</div>
                                </div>
                                <div class="card-body">
                                    <ol class="activity-feed">

                                        @foreach($visitasTerminadas as $visita)

                                            <li class="feed-item feed-item-secondary">
                                                <time class="date"
                                                      datetime="">{{  \Carbon\Carbon::parse($visita->created_at)->diffForHumans()}}
                                                </time>
                                                <span class="text">Recomendaicon: {{$reunion->recomendaciones}} </span>
                                            </li>
                                        @endforeach


                                    </ol>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        @else

            <div class="panel-header bg-primary-gradient">
                <div class="page-inner py-5">
                    <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                        <div>

                            <div id="" class="animated bounce infinite">
                                <button class="btn infiniteAnimate bg-transparent text-white">
                                    <i class="fa fa-2x fa-arrow-circle-up"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page-inner mt--5">
                <div class="container">
                    <div class="row justify-content-center align-items-center" style="height: 73vh">
                        <div class="col-lg-12">

                            <div class="card card-profile">
                                <div class="card-header"
                                     style="background-image: url('../assets/img/blogpost.jpg');
                                            padding: 5rem 1.25rem;
                                            background-color: transparent;
                                            border-bottom: 1px solid #ebecec !important;">
                                    miembro    <div class="profile-picture">
                                        <div class="avatar avatar-xxl">
                                            <img id="perfil" src="/images/perfil/{{auth()->user()->imagen}}"
                                                 alt="..."
                                                 class="avatar-img rounded-circle">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="user-profile text-center">
                                        <div class="name">{{auth()->user()->persona->nombres}}</div>
                                        <div class="job">{{auth()->user()->email}}</div>
                                        <div class="desc">{{ auth()->user()->getRoleNames()[0]}}</div>

                                        <div class="view-profile">
                                            <a href="/perfil" class="btn btn-primary btn-lg">Ver Perfil</a>
                                        </div>

                                    </div>


                                </div>
                                <div class="card-footer">
                                    <div class="row user-stats text-center">
                                        <div class="col">
                                            <div class="number">{{auth()->user()->created_at}}</div>
                                            <div class="title">Fecha Registro.</div>
                                        </div>
                                        <div class="col">
                                            <div class="number">{{auth()->user()->persona->telefono}}</div>
                                            <div class="title">Telefono</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        @endif

    </div>




@endsection

@section('scripts')


    <script type="text/javascript">


        let MIEMBROS = '{!! $miembros!!}';
        let REUNIONES = '{!! $numeroReunionesPendientes!!}';

        Circles.create({
            id: 'circles-1',
            radius: 45,
            value: 60,
            maxValue: 100,
            width: 7,
            text: MIEMBROS,
            colors: ['#f1f1f1', '#FF9E27'],
            duration: 400,
            wrpClass: 'circles-wrp',
            textClass: 'circles-text',
            styleWrapper: true,
            styleText: true
        })

        Circles.create({
            id: 'circles-2',
            radius: 45,
            value: REUNIONES === 0 ? 45 : 100,
            maxValue: 100,
            width: 7,
            text: REUNIONES,
            colors: ['#f1f1f1', '#2BB930'],
            duration: 400,
            wrpClass: 'circles-wrp',
            textClass: 'circles-text',
            styleWrapper: true,
            styleText: true
        })


    </script>

@endsection
