<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //documentacion https://www.hoclabs.com/2018/01/14/laravel-roles-y-permisos/
        //https://github.com/spatie/laravel-permission

        // create permissions

        Permission::create(['name' => 'ver.usuarios']);
        Permission::create(['name' => 'crear.usuarios']);
        Permission::create(['name' => 'editar.usuarios']);
        Permission::create(['name' => 'eliminar.usuarios']);

        Permission::create(['name' => 'ver.roles']);
        Permission::create(['name' => 'crear.roles']);
        Permission::create(['name' => 'editar.roles']);
        Permission::create(['name' => 'eliminar.roles']);

        Permission::create(['name' => 'ver.categorias']);
        Permission::create(['name' => 'crear.categorias']);
        Permission::create(['name' => 'editar.categorias']);
        Permission::create(['name' => 'eliminar.categorias']);

        Permission::create(['name' => 'ver.iglesias']);
        Permission::create(['name' => 'crear.iglesias']);
        Permission::create(['name' => 'editar.iglesias']);
        Permission::create(['name' => 'eliminar.iglesias']);

        Permission::create(['name' => 'ver.visitas']);
        Permission::create(['name' => 'crear.visitas']);
        Permission::create(['name' => 'editar.visitas']);
        Permission::create(['name' => 'eliminar.visitas']);

        Permission::create(['name' => 'ver.reuniones']);
        Permission::create(['name' => 'crear.reuniones']);
        Permission::create(['name' => 'editar.reuniones']);
        Permission::create(['name' => 'eliminar.reuniones']);
        Permission::create(['name' => 'agregar.asistente']);
        Permission::create(['name' => 'eliminar.asistente']);
        Permission::create(['name' => 'terminar.reuniones']);

        Permission::create(['name' => 'ver.miembros']);
        Permission::create(['name' => 'crear.miembros']);
        Permission::create(['name' => 'editar.miembros']);
        Permission::create(['name' => 'eliminar.miembros']);

        Permission::create(['name' => 'ver.ofrendasYDiezmos']);
        Permission::create(['name' => 'crear.ofrendasYDiezmos']);
        Permission::create(['name' => 'editar.ofrendasYDiezmos']);
        Permission::create(['name' => 'eliminar.ofrendasYDiezmos']);

        ///especiales//
        Permission::create(['name' => 'asignar.AIglesias']);

        // this can be done as separate statements
        $role = Role::create(['name' => 'Admin']);
        $role = Role::create(['name' => 'Pastor']);
        $role = Role::create(['name' => 'Lider']);
        $role = Role::create(['name' => 'Coordinador']);
        $role = Role::create(['name' => 'Colaborador']);
        $role = Role::create(['name' => 'Miembro']);


        //$role->givePermissionTo(Permission::all());
        //$role->givePermissionTo('ver.usuarios');

        $role = Role::create(['name' => 'Super Admin']);
        $role->givePermissionTo(Permission::all());

        //asignar rol a usuario
        //$user = User::find(1);
        //$user->assignRole('Super Admin');
    }

}
