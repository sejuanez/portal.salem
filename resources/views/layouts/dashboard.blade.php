<!doctype html>

<html lang="es">

<head>
    @include('includes.head')
</head>

<body>


<div class="wrapper">

    @include('includes.navar')
    @include('includes.sidebar')


    <div class="main-panel">


        @yield('content')


        @include('includes.footer')

    </div>

</div>

</body>

@include('includes.script')
</html>