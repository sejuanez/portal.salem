<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class PermissionController extends Controller
{
    /**
     * @var Role
     */
    private $permission;
    function __construct(Permission $permission)
    {
        $this->permission = $permission;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return $this->permission->get();
        $response = array(
            'status' => 'success',
            'msg' => $this->permission->get(),
        );

        return response()->json($response);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);
        $role = $this->permission->create([
            'name' => $request->name,
            'description' => $request->description,
        ]);
        return response(['message' => 'Permiso Creado']);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Permission $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        $request->validate([
            'name'     => 'required',
        ]);
        $permission->update([
            'name'     => $request->name,
            'description' => $request->description,
        ]);
        return response(['message' => 'Permiso Actualizado']);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->permission->destroy($id);
        return response(['message' => 'Permiso Eliminado']);
    }
}
