<?php

namespace App\Http\Controllers;

use App\Cities;
use App\States;
use App\Countries;
use Illuminate\Http\Request;

class LocalidadController extends Controller
{
    //

    function getCountries()
    {
        $state = Countries::get();

        $response = array(
            'status' => 'success',
            'msg' => $state,
        );

        return response()->json($response);
    }

    function getStates(Request $request)
    {
        $state = States::where('country_id', '=', $request->countrie)->get();

        $response = array(
            'status' => 'success',
            'msg' => $state,
        );

        return response()->json($response);
    }

    function getCity(Request $request)
    {

        $cities = Cities::where('state_id', '=', $request->state)->get();
        $response = array(
            'status' => 'success',
            'msg' => $cities,
        );

        return response()->json($response);
    }

    function getStatesOfCity(Request $request)
    {
        $city = Cities::where('id', '=', $request->city)->first();

        $response = array(
            'status' => 'success',
            'msg' => $city->state_id,
        );

        return response()->json($response);
    }

    function getCountrieOfState(Request $request)
    {
        $city = States::where('id', '=', $request->state)->first();

        $response = array(
            'status' => 'success',
            'msg' => $city,
        );

        return response()->json($response);
    }

}
