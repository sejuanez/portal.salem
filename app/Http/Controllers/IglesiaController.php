<?php

namespace App\Http\Controllers;

use App\Iglesias;
use App\Miembros;
use App\Personas;
use App\Reuniones;
use App\Slm_people;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class IglesiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $response = array(
            'status' => 'success',
            'msg' => DB::table('iglesias')->get()
        );


        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'nombre' => 'required|string',
            'direccion' => 'required|string',
            'barrio' => 'required|string',
            'city_id' => 'required|string',
            'tipo' => 'required|string',
        ], [
            'nombre.required' => 'El nombre de la iglesia es obligatorio',
            'direccion.required' => 'La direccion de la iglesia es obligatoria',
            'city_id.required' => 'Por favor seleccione una ciudad',
        ]);

        try {
            $error = null;
            DB::beginTransaction();


            $iglesia = Iglesias::create([
                'nombre' => $request->nombre,
                'direccion' => $request->direccion,
                'barrio' => $request->barrio,
                'city_id' => $request->city_id,
                'tipo' => $request->tipo,
                'estado' => true,
                'latitud' => $request->latitud,
                'longitud' => $request->longitud,
                'nota' => $request->nota,
            ]);

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => $iglesia,
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,

            );
        }//error


        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Iglesias $iglesia
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Iglesias $iglesia)
    {
        //

        $this->validate($request, [
            'nombre' => 'required|string',
            'direccion' => 'required|string',
            'barrio' => 'required|string',
            'city_id' => 'required|string',
            'tipo' => 'required|string',
            'latitud' => 'required|string',
            'longitud' => 'required|string',
        ], [
            'nombre.required' => 'El nombre de la iglesia es obligatorio',
            'direccion.required' => 'La direccion de la iglesia es obligatoria',
            'state_id.required' => 'Por favor seleccione una ciudad',
        ]);


        $iglesia->update([
            'nombre' => $request->nombre,
            'direccion' => $request->direccion,
            'barrio' => $request->barrio,
            'city_id' => $request->city_id,
            'tipo' => $request->tipo,
            'latitud' => $request->latitud,
            'longitud' => $request->longitud,
            'nota' => $request->nota,
        ]);

        return response(
            ['message' => 'Iglesia Actualizada']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verIglesias()
    {
        $array = array();
        //if (auth()->user()->roles[0]['name'] == 'Super Admin' or auth()->user()->roles[0]['name'] == 'Super Admin') {
        if (in_array("Super Admin", auth()->user()->getRoleNames()->toArray()) or in_array("Admin", auth()->user()->getRoleNames()->toArray())) {
            $iglesias = Iglesias::all();
            foreach ($iglesias as $iglesia) {
                $datos = [
                    'iglesia' => $iglesia,
                    'ciudad' => $iglesia->ciudad,
                    'state' => $iglesia->ciudad?$iglesia->ciudad->state:'',
                ];
                array_push($array, $datos);
            }
        } else {
            $miembro = auth()->user()->persona->miembro;
            foreach ($miembro as $lugar) {
                $datos = [
                    'iglesia' => $lugar->iglesia,
                    'ciudad' => $lugar->iglesia->ciudad,
                    'state' => $lugar->iglesia->ciudad?$lugar->iglesia->ciudad->state:'',
                ];
                array_push($array, $datos);
            }
        }

        $response = array(
            'status' => 'success',
            'msg' => $array
        );

        return response()->json($response);
    }

    function verificarIglesia(Request $request)
    {

        $iglesia = Iglesias::findOrFail($request->id);

        $error = null;
        DB::beginTransaction();
        try {

            $iglesia->estado = !$iglesia->estado;
            $iglesia->save();
            DB::commit();

            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => ($iglesia),
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }

    ////Asignar//
    function asignarPastor(Request $request)
    {
        $iglesia = Iglesias::findOrFail($request->id);
        $usuario = User::findOrFail($request->select_pastor);

        $miembro = Miembros::create([
            'persona_id' => $usuario->persona->id,
            'iglesia_id' => $iglesia->id,
        ]);

        $iglesia->pastor_id = $request->select_pastor;
        //$usuario->persona->iglesia_id = $request->id;
        //$usuario->persona->save();
        $iglesia->save();

        //guardar log de registro de actividad
        return response(
            [
                'message' => 'Pastor Asignado con exito',
            ]);
    }

    function asignarCoordinador(Request $request)
    {
        $iglesia = Iglesias::findOrFail($request->id);
        $usuario = User::findOrFail($request->select_coordinador);

        $miembro = Miembros::create([
            'persona_id' => $usuario->persona->id,
            'iglesia_id' => $iglesia->id,
        ]);

        $iglesia->coordinador_id = $request->select_coordinador;
        //$usuario->persona->iglesia_id = $request->id;
        //$usuario->persona->save();
        $iglesia->save();
        //guardar log de registro de actividad

        return response(
            ['message' => 'Coordinador asignado con exito']);
    }

    function asignarColaborador(Request $request)
    {
        $iglesia = Iglesias::findOrFail($request->id);
        $usuario = User::findOrFail($request->select_colaborador);

        $miembro = Miembros::create([
            'persona_id' => $usuario->persona->id,
            'iglesia_id' => $iglesia->id,
        ]);

        $iglesia->colaborador_id = $request->select_colaborador;
        //$usuario->persona->iglesia_id = $request->id;
        //$usuario->persona->save();
        $iglesia->save();
        //guardar log de registro de actividad

        return response(
            ['message' => 'Colaborador asignado con exito']);
    }

    ////Eliminar////
    function eliminarPastor(Request $request)
    {
        $iglesia = Iglesias::findOrFail($request->id);
        $usuario = User::findOrFail($iglesia->pastor_id);

        Miembros::where('persona_id', $usuario->persona->id)->where('iglesia_id', $iglesia->id)->delete();

        $iglesia->pastor_id = null;
        //$usuario->persona->iglesia_id = null;
        //$usuario->persona->save();
        $iglesia->save();

        //guardar log de registro de actividad
        return response(
            [
                'message' => 'Pastor eliminado de la iglesia con exito',
            ]);
    }

    function eliminarCoordinador(Request $request)
    {
        $iglesia = Iglesias::findOrFail($request->id);
        $usuario = User::findOrFail($iglesia->coordinador_id);
        Miembros::where('persona_id', $usuario->persona->id)->where('iglesia_id', $iglesia->id)->delete();

        $iglesia->coordinador_id = null;
        //$usuario->persona->iglesia_id = null;
        //$usuario->persona->save();
        $iglesia->save();

        //guardar log de registro de actividad
        return response(
            [
                'message' => 'Coordinador eliminado de la iglesia con exito',
            ]);
    }

    function eliminarColaborador(Request $request)
    {
        $iglesia = Iglesias::findOrFail($request->id);
        $usuario = User::findOrFail($iglesia->colaborador_id);
        Miembros::where('persona_id', $usuario->persona->id)->where('iglesia_id', $iglesia->id)->delete();

        $iglesia->colaborador_id = null;
        //$usuario->persona->iglesia_id = null;
        //$usuario->persona->save();
        $iglesia->save();

        //guardar log de registro de actividad
        return response(
            [
                'message' => 'Colaborador eliminado de la iglesia con exito',
            ]);
    }

    /////////////
    function agregarMimebro(Request $request)
    {
        if ($request->select_miembro != null) {
            $usuario = User::findOrFail($request->select_miembro);
            $persona = $usuario->persona;
        } else {
            $this->validate($request, [
                'nombres' => 'required|string',
                'apellidos' => 'required|string',
            ], [
                'nombres.required' => 'Los nombres son requeridos',
                'apellidos.required' => 'Los apellidos son requeridos',
            ]);

            $persona = Personas::create([
                'nombres' => $request->nombres,
                'apellidos' => $request->apellidos,
                'telefono' => $request->telefono,
            ]);
        }
        $rol = $request->rol == null ? 'Miembro' : $request->rol;
        $miembro = Miembros::create([
            'persona_id' => $persona->id,
            'iglesia_id' => $request->iglesia_id == null ? session()->get('id_iglesia') : $request->iglesia_id,
            'rol' => $rol
        ]);

        return response(
            ['message' => 'Miembro agregado con exito', 'rol' => $rol]);
    }

    function eliminarMimebro(Request $request)
    {
        $persona = Personas::findOrFail($request->persona_id);
        //$persona->delete();
        Miembros::where('persona_id', $persona->id)->where('iglesia_id', session()->get('id_iglesia'))->delete();

        return response(
            ['message' => 'Miembro eliminado con exito']);
    }

    function verMiembros(Request $request)
    {

        if ($request->id == null) {
            $iglesia = Iglesias::findOrFail(session()->get('id_iglesia'));
        } else {
            $iglesia = Iglesias::findOrFail($request->id);
        }
        $array = array();
        foreach ($iglesia->miembros as $miembro) {
            $datos = [
                'id' => $miembro->persona->id,
                'id_old' => null,
                'nombres' => $miembro->persona->nombres,
                'apellidos' => $miembro->persona->apellidos,
                'telefono' => $miembro->persona->telefono,
                'rol' => $miembro->rol,
            ];
            array_push($array, $datos);
        }
        /////////////////////////////////
        $miembros_old=Slm_people::where('church_id',$iglesia->id_old_iglesia)->get();
        foreach ($miembros_old as $miembro) {
            $datos = [
                'id' => null,
                'id_old' => $miembro->id,
                'nombres' => $miembro->first_name,
                'apellidos' => $miembro->last_name,
                'telefono' => 000000,
                'rol' => 'Miembro_old',
            ];
            array_push($array, $datos);
        }
        ////////////////////////////////
        $response = array(
            'status' => 'success',
            'old' => $iglesia->id_old_iglesia,
            'msg' => $array,
        );
        return response()->json($response);

    }

    function actualizarMiembro(Request $request)
    {

        $persona = Personas::findOrFail($request->id);
        $persona->update($request->all());
        $persona->save();

        $response = array(
            'status' => 'success',
            'msg' => 'Miembro actualizado con exito',
        );
        return response()->json($response);
    }

    ///// Seleccionar iglesia navbar
    function seleccionarIglesia(Request $request)
    {

        session()->put('id_iglesia', $request->idIglesia);
        $miembro = Miembros::where('iglesia_id', $request->idIglesia)
            ->where('persona_id', auth()->user()->persona->id)->first();

        if (in_array("Super Admin", auth()->user()->getRoleNames()->toArray()) Or in_array("Admin", auth()->user()->getRoleNames()->toArray())) {

        } else {
            auth()->user()->syncRoles($miembro->rol);
        }
        //guardar log de registro de actividad
        return response(
            [
                'message' => session()->get('id_iglesia', function () {
                    return 'default';
                }),
            ]);
    }

    function verIglesiaSeleccionada(Request $request)
    {

        $value = $request->session()->get('id_iglesia');

        //guardar log de registro de actividad
        return response(
            [
                'message' => $value,
            ]);
    }

    function verMiembrosCoordinadores(Request $request)
    {

        if ($request->id == null) {
            $iglesia = Iglesias::findOrFail(session()->get('id_iglesia'));
        } else {
            $iglesia = Iglesias::findOrFail($request->id);
        }
        $array = array();
        foreach ($iglesia->miembros as $miembro) {
            if ($miembro->rol=='Coordinador'){
                $datos = [
                    'id' => $miembro->persona->user->id,
                    'id_old' => null,
                    'nombres' => $miembro->persona->nombres,
                    'apellidos' => $miembro->persona->apellidos,
                    'telefono' => $miembro->persona->telefono,
                    'rol' => $miembro->rol,
                ];
                array_push($array, $datos);
            }
        }
        $response = array(
            'status' => 'success',
            'old' => $iglesia->id_old_iglesia,
            'msg' => $array,
        );
        return response()->json($response);

    }

}
