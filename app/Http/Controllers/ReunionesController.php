<?php

namespace App\Http\Controllers;

use App\Asistentes;
use App\Iglesias;
use App\Ofrendasydiezmos;
use App\Reuniones;
use App\Slm_report;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReunionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $response = array(
            'status' => 'success',
            'msg' => DB::table('reuniones')->get()
        );

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //
        $this->validate($request, [
            'nombre' => 'required|string',
            'tipo' => 'required|string',
            'num_adultos' => 'numeric',
            'num_ninios' => 'numeric',
            'fecha_realizado' => 'required|date',
            'hora_realizado' => 'required',
        ], [
            'nombre.required' => 'El nombre de la reunion es obligatorio',
            'tipo.required' => 'El tipo de reunion es obligatorio',
            'fecha_realizado.required' => 'Por favor seleccione la fecha de la reunion',
            'hora_realizado.required' => 'Por favor seleccione la hora de la reunion',
        ]);

        try {
            $error = null;
            DB::beginTransaction();

            $reunion = Reuniones::create([

                'nombre' => $request->nombre,
                'tipo' => $request->tipo,
                'num_adultos' => $request->num_adultos,
                'num_ninios' => $request->num_ninios,
                'descripcion' => $request->descripcion,
                'user_id' => auth()->user()->id,
                'iglesia_id' => session()->get('id_iglesia'),
                'fecha_realizado' => $request->fecha_realizado,
                'hora_realizado' => $request->hora_realizado

            ]);

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => $reunion,
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,

            );
        }//error


        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Reuniones $reunion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate($request, [
            'tipo' => 'required|string',
            'num_adultos' => 'numeric',
            'num_ninios' => 'numeric',
            'fecha_realizado' => 'required|date',
            'hora_realizado' => 'required',
        ], [
            'tipo.required' => 'El tipo de reunion es obligatorio',
            'hora_realizado.required' => 'Por favor seleccione la hora de la reunion',
        ]);

        $reunion = Reuniones::findOrFail($request->id);

        $reunion->update([
            'tipo' => $request->tipo,
            'nombre' => $request->nombre,
            'num_adultos' => $request->num_adultos,
            'num_ninios' => $request->num_ninios,
            'descripcion' => $request->descripcion,
            'user_id' => auth()->user()->id,
            'iglesia_id' => session()->get('id_iglesia'),
            'fecha_realizado' => $request->fecha_realizado,
            'hora_realizado' => $request->hora_realizado,
        ]);

        return response(
            [
                'message' => 'Reunion Actualizada',

            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Eliminar la reunion solo son el permiso
        //se eliminaran los asistentes primero despues la reunion

    }

    public function cambiarEstadoReunion(Request $request)
    {

        $reunion = Reuniones::findOrFail($request->id);

        $reunion->update([
            'estado' => !($reunion->estado),
        ]);

        return response(
            [
                'message' => 'Estado de reunion Actualizado',
                'estado' => ($reunion->estado),

            ]);

    }

    public function reunionesPendientes()
    {

        if (session()->get('id_iglesia', function () {
                return 'default';
            }) != "default") {
            $reuniones = Reuniones::where('estado', false)->where('iglesia_id', session()->get('id_iglesia'))->get();

        } else {
            $reuniones = Reuniones::where('estado', false)->get();
        }
        $response = array(
            'status' => 'success',
            'msg' => $reuniones,
        );

        return response()->json($response);
    }

    public function reunionesCompletadas()
    {

        if (session()->get('id_iglesia', function () {
                return 'default';
            }) != "default") {
            $reuniones = Reuniones::where('estado', true)->where('iglesia_id', session()->get('id_iglesia'))->get();

        } else {
            $reuniones = Reuniones::where('estado', true)->get();
        }

        $response = array(
            'status' => 'success',
            'msg' => $reuniones,
        );
        return response()->json($response);
    }

    public function agregarAsistente(Request $request)
    {
        $status = 'success';
        $respuesta = explode(":", $request->persona_id);
        if ($respuesta[1] == 'old') {
            //agregar id viejo
            $asistentes = Asistentes::where('reunion_id', $request->reunion_id)->where('old_id', $respuesta[0])->get();
            if (count($asistentes) == 0) {
                $reunion = Asistentes::create([
                    'reunion_id' => $request->reunion_id,
                    'old_id' => $respuesta[0],
                ]);
                $mensaje = 'Asistente agregado con exito';
            } else {
                $mensaje = 'Asistente ya se encuentra agregado';
                $status = 'danger';
            }
        } else {
            $asistentes = Asistentes::where('reunion_id', $request->reunion_id)->where('persona_id', $respuesta[0])->get();
            if (count($asistentes) == 0) {
                $reunion = Asistentes::create([
                    'reunion_id' => $request->reunion_id,
                    'persona_id' => $respuesta[0],
                ]);
                $mensaje = 'Asistente agregado con exito';
            } else {
                $mensaje = 'Asistente ya se encuentra agregado';
                $status = 'danger';
            }
        }

        return response(
            ['status' => $status, 'message' => $mensaje, 'resp' => $asistentes]);

    }

    public function eliminarAsistente(Request $request)
    {
        //validar si es old
        $respuesta = explode(":", $request->persona_id);
        if ($respuesta[1] == 'old') {
            $reunion = Asistentes::where('reunion_id', '=', $request->reunion_id)->where('old_id', '=', $respuesta[0])->first();
        } else {
            $reunion = Asistentes::where('reunion_id', '=', $request->reunion_id)->where('persona_id', '=', $respuesta[0])->first();
        }
        Asistentes::destroy($reunion->id);

        return response(
            ['message' => 'Asistente eximinado con exito']);
    }

    function verAsistentesReunion(Request $request)
    {
        $array = array();

        if ($request->id == null) {
            //no manda id
        } else {
            $asistentes = Asistentes::where('reunion_id', $request->id)->get();
            foreach ($asistentes as $asistente) {
                if ($asistente->persona == null) {
                    $datos = [
                        'id' => $asistente->persona_old->id . ':old',
                        'nombre' => $asistente->persona_old->first_name,
                        'apellido' => $asistente->persona_old->last_name,
                        'cedula' => 0,
                        'creado' => $asistente->created_at,
                    ];
                } else {
                    $datos = [
                        'id' => $asistente->persona->id . ':new',
                        'nombre' => $asistente->persona->nombres,
                        'apellido' => $asistente->persona->apellidos,
                        'cedula' => $asistente->persona->cedula,
                        'creado' => $asistente->created_at,
                    ];
                }

                array_push($array, $datos);
            }
        }
        $response = array(
            'status' => 'success',
            'msg' => $array,
        );
        return response()->json($response);

    }

    public function eliminarReunion(Request $request)
    {
        // $reunion = Asistentes::where('reunion_id', '=', $request->reunion_id)->get();
        // Asistentes::destroy($reunion->id);
        return response(
            ['message' => 'Reunion eliminado con exito']);
    }

    function verReunionesViejas(Request $request)
    {
        $iglesia = Iglesias::find(session()->get('id_iglesia'));
        $old = "";
        if ($iglesia == null) {
            set_time_limit(0);
            // set_memory_size(0);
            if (in_array("Super Admin", auth()->user()->getRoleNames()->toArray()) Or in_array("Admin", auth()->user()->getRoleNames()->toArray())) {
                $old = Slm_report::limit(20000)->get();
            }
        } else {
            $old = Slm_report::where('church_id', $iglesia->id_old_iglesia)->get();
        }


        $response = array(
            'status' => 'success',
            'msg' => $old,
        );
        return response()->json($response);
    }

    function graficaReuniones(Request $request)
    {
        setlocale(LC_ALL, 'es_ES');
        if ($request->anio == null) {
            $respuesta = explode("-", $request->fechaInicio);

            if (session()->get('id_iglesia', function () {
                    return 'default';
                }) != "default") {
                $iglesia = Iglesias::find(session()->get('id_iglesia'));
                if ($respuesta[0] >= 2019) {
                    //consultar actual
                    $old = Reuniones::select(DB::raw('date_format(fecha_realizado,"%d") as mes'), DB::raw('COUNT(id) as reuniones'), DB::raw('SUM(num_adultos) as num_adult'), DB::raw('SUM(num_ninios) as num_child'), DB::raw('SUM(0) as ingresos'))
                        ->where('iglesia_id', $iglesia->id)
                        ->whereBetween('fecha_realizado', [$request->fechaInicio, $request->fechaFin])
                        ->groupBy('mes')->get();
                    $ingresos = Ofrendasydiezmos::select(DB::raw('date_format(fecha,"%d") as mes'), DB::raw('COUNT(id) as reg'), DB::raw('SUM(ofrenda)+SUM(diezmo) as ingresos'))
                        ->where('iglesia_id', $iglesia->id)
                        ->whereBetween('fecha', [$request->fechaInicio, $request->fechaFin])
                        ->groupBy('mes')->get();
                } else {
                    //consultar Antigua
                    $old = Slm_report::select(DB::raw('date_format(reg_date,"%d") as mes'), DB::raw('COUNT(id) as reuniones'), DB::raw('SUM(num_adult) as num_adult'), DB::raw('SUM(num_child) as num_child'), DB::raw('SUM(income) as ingresos'))
                        ->where('church_id', $iglesia->id_old_iglesia)
                        ->whereBetween('reg_date', [$request->fechaInicio, $request->fechaFin])
                        ->groupBy('mes')->get();
                    $ingresos = null;
                }

            } else {
                if ($respuesta[0] >= 2019) {
                    //consultar actual
                    $old = Reuniones::select(DB::raw('date_format(fecha_realizado,"%d") as mes'), DB::raw('COUNT(id) as reuniones'), DB::raw('SUM(num_adultos) as num_adult'), DB::raw('SUM(num_ninios) as num_child'), DB::raw('SUM(0) as ingresos'))
                        ->whereBetween('fecha_realizado', [$request->fechaInicio, $request->fechaFin])
                        ->groupBy('mes')->get();
                    $ingresos = Ofrendasydiezmos::select(DB::raw('date_format(fecha,"%d") as mes'), DB::raw('COUNT(id) as reg'), DB::raw('SUM(ofrenda)+SUM(diezmo) as ingresos'))
                        ->whereBetween('fecha', [$request->fechaInicio, $request->fechaFin])
                        ->groupBy('mes')->get();
                } else {
                    //consultar Antigua
                    $old = Slm_report::select(DB::raw('date_format(reg_date,"%d") as mes'), DB::raw('COUNT(id) as reuniones'), DB::raw('SUM(num_adult) as num_adult'), DB::raw('SUM(num_child) as num_child'), DB::raw('SUM(income) as ingresos'))
                        ->whereBetween('reg_date', [$request->fechaInicio, $request->fechaFin])
                        ->groupBy('mes')->get();
                    $ingresos = null;
                }
            }
        } else {
            $respuesta = explode("-", $request->anio);
            $formato = (count($respuesta) > 1 ? 'v' : 'b');

            if (session()->get('id_iglesia', function () {
                    return 'default';
                }) != "default") {

                $iglesia = Iglesias::find(session()->get('id_iglesia'));
                if ($respuesta[0] >= 2019) {
                    $old = Reuniones::select(DB::raw('MONTH(fecha_realizado) as mes_id'), DB::raw('date_format(fecha_realizado,"%' . $formato . '") as mes'), DB::raw('COUNT(id) as reuniones'), DB::raw('SUM(num_adultos) as num_adult'), DB::raw('SUM(num_ninios) as num_child'), DB::raw('SUM(0) as ingresos'))
                        ->where('iglesia_id', $iglesia->id)
                        ->whereDate('fecha_realizado', 'like', $request->anio . '%')
                        ->groupBy('mes_id', 'mes')->get();
                    $ingresos = Ofrendasydiezmos::select(DB::raw('date_format(fecha,"%' . $formato . '") as mes'), DB::raw('COUNT(id) as reg'), DB::raw('SUM(ofrenda)+SUM(diezmo) as ingresos'))
                        ->where('iglesia_id', $iglesia->id)
                        ->whereDate('fecha', 'like', $request->anio . '%')
                        ->groupBy('mes')->get();
                } else {
                    $old = Slm_report::select(DB::raw('MONTH(reg_date) as mes_id'), DB::raw('date_format(reg_date,"%' . $formato . '") as mes'), DB::raw('COUNT(id) as reuniones'), DB::raw('SUM(num_adult) as num_adult'), DB::raw('SUM(num_child) as num_child'), DB::raw('SUM(income) as ingresos'))
                        ->where('church_id', $iglesia->id_old_iglesia)
                        ->whereDate('reg_date', 'like', $request->anio . '%')
                        ->groupBy('mes_id', 'mes')->get();
                    $ingresos = null;

                }

            } else {

                if ($respuesta[0] >= 2019) {
                    $old = Reuniones::select(DB::raw('MONTH(fecha_realizado) as mes_id'), DB::raw('date_format(fecha_realizado,"%' . $formato . '") as mes'), DB::raw('COUNT(id) as reuniones'), DB::raw('SUM(num_adultos) as num_adult'), DB::raw('SUM(num_ninios) as num_child'), DB::raw('SUM(0) as ingresos'))
                        ->whereDate('fecha_realizado', 'like', $request->anio . '%')
                        ->groupBy('mes_id', 'mes')->get();
                    $ingresos = Ofrendasydiezmos::select(DB::raw('MONTH(fecha) as mes_id'), DB::raw('date_format(fecha,"%' . $formato . '") as mes'), DB::raw('COUNT(id) as reg'), DB::raw('SUM(ofrenda)+SUM(diezmo) as ingresos'))
                        ->whereDate('fecha', 'like', $request->anio . '%')
                        ->groupBy('mes_id', 'mes')->get();
                } else {
                    $old = Slm_report::select(DB::raw('MONTH(reg_date) as mes_id'), DB::raw('date_format(reg_date,"%' . $formato . '") as mes'), DB::raw('COUNT(id) as reuniones'), DB::raw('SUM(num_adult) as num_adult'), DB::raw('SUM(num_child) as num_child'), DB::raw('SUM(income) as ingresos'))
                        ->whereDate('reg_date', 'like', $request->anio . '%')
                        ->groupBy('mes_id', 'mes')->get();
                    $ingresos = null;

                }

            }
        }

        if ($ingresos != null) {
            foreach ($old as $dato) {
                foreach ($ingresos as $ingreso) {
                    if ($dato->mes == $ingreso->mes) {
                        //agregar campo
                        $dato->ingresos = $ingreso->ingresos;
                    }
                }
            }
        }

        $response = array(
            'status' => 'success',
            'ano' => $request->anio ? $request->anio : $request->fechaInicio,
            'mes' => $request->anio ? $request->anio : $request->fechaFin,
            'msg' => $old,
            'msg2' => $ingresos,
        );

        return response()->json($response);
    }

}
