@extends('layouts.dashboard')

@section('content')

    <div class="content container-full">

        <div class="page-navs bg-white">
            <div class="nav-scroller">
                <div
                    class="nav nav-tabs nav-line nav-color-secondary d-flex align-items-center justify-contents-center w-100">
                    <a class="nav-link mr-5 active show" data-toggle="tab" href="#tab1" id="tabR1">
                        Iglesias
                    </a>
                    @if(session()->get('id_iglesia', function () {return 'default';}) !="default")
                        @can('ver.reporte_asistencia_ingresos')

                            <a class="nav-link mr-4" data-toggle="tab" href="#tab2" id="tabR2">
                                Asistencia e Ingresos
                            </a>
                        @endcan
                    @endif
                    @can('ver.reporte_iglesias_asistencia_ingresos')
                        <a class="nav-link mr-4" data-toggle="tab" href="#tab3" id="tabR3">
                            Asistencia e Ingresos General
                        </a>
                    @endcan
                    <div class="ml-auto">
                    </div>
                </div>
            </div>
        </div>

        <div class="page-inner">

            <div class="tab-content mt-2 mb-3" id="card-reuniones">
                <div class="tab-pane fade active show" id="tab1" role="tabpanel" aria-labelledby="pills-home-tab">

                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="select-pais">Pais</label>
                                        <select class="form-control" name="select-pais" id="select-pais"
                                                style="width: 100%">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="select-departamento">Departamento/Estado</label>
                                        <select class="form-control" name="select-departamento"
                                                id="select-departamento"
                                                style="width: 100%">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="select-ciudad">Ciudad</label>
                                        <select class="form-control" name="select-ciudad" id="select-ciudad"
                                                style="width: 100%">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary btn-block"
                                                onclick="generarReporte()">Generar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="card">
                                <div class="card-body">

                                    <div id="cargando">
                                        <img src="/assets/img/Spinner-1s-200px.gif" alt=""
                                             style="display:flex; margin:0 auto;">
                                    </div>
                                    <div id="reporte1" style="display:none;">
                                        <iframe id="iframe-reporte1" frameborder="0"
                                                style="width: 100%; height: 800px;"></iframe>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                @if(session()->get('id_iglesia', function () {return 'default';}) !="default" )
                    @can('ver.reporte_asistencia_ingresos')
                        <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="pills-home-tab">

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="card">
                                        <div class="card-body">

                                            <div style="overflow:hidden;">
                                                <div class="form-group">
                                                    <div id="fechaMes"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="card">
                                        <div class="card-body">

                                            <div id="cargando2">
                                                <img src="/assets/img/Spinner-1s-200px.gif" alt=""
                                                     style="display:flex; margin:0 auto;">
                                            </div>
                                            <div id="reporte2" style="display:none;">
                                                <iframe id="iframe-reporte2" frameborder="0"
                                                        style="width: 100%; height: 800px;"></iframe>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    @endcan
                @endif

                @can('ver.reporte_iglesias_asistencia_ingresos')
                    <div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="pills-home-tab">

                        <div class="row">
                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="select-pais">Pais</label>
                                            <select class="form-control" name="select-pais" id="select-pais2"
                                                    style="width: 100%">
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="select-departamento">Departamento/Estado</label>
                                            <select class="form-control" name="select-departamento"
                                                    id="select-departamento2"
                                                    style="width: 100%">
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="select-ciudad">Ciudad</label>
                                            <select class="form-control" name="select-ciudad" id="select-ciudad2"
                                                    style="width: 100%">
                                            </select>
                                        </div>
                                        <div style="overflow:hidden;">
                                            <div class="form-group">
                                                <div id="fechaMesG"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary btn-block"
                                                    onclick="generarReporte2()">Generar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="card">
                                    <div class="card-body">

                                        <div id="cargando3">
                                            <img src="/assets/img/Spinner-1s-200px.gif" alt=""
                                                 style="display:flex; margin:0 auto;">
                                        </div>
                                        <div id="reporte3" style="display:none;">
                                            <iframe id="iframe-reporte3" frameborder="0"
                                                    style="width: 100%; height: 800px;"></iframe>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                @endcan
            </div>
        </div>

    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            @if(session()->get('id_iglesia', function () {return 'default';}) !="default")
            @can('ver.reporte_asistencia_ingresos')
            $('#fechaMes').datetimepicker({
                inline: true,
                sideBySide: true,
                viewMode: 'months',
                format: 'YYYY-MM'
            });

            verReporte2($("#fechaMes").data("date"));
            $('#fechaMes').on('dp.change', function (e) {
                console.log($("#fechaMes").data("date"));
                verReporte2($("#fechaMes").data("date"));
                //consultarDatosGrafica($("#fechaMes").data("date"), null, null);
            });

            @endcan

            @endif

            $('#select-pais').select2({
                theme: "bootstrap",
                placeholder: "Selecciona un pais...",
                allowClear: true,
            });

            $("#select-departamento").select2({
                theme: "bootstrap",
                placeholder: "Selecciona un departamento...",
                allowClear: true,
            });

            $("#select-ciudad").select2({
                allowClear: true,
                theme: "bootstrap",
                placeholder: "Selecciona una ciudad...",
            });


            getPaises();

            $('#select-pais').on('change', function (e) {
                getDepartamentos($("#select-pais").val());
            });

            $('#select-departamento').on('change', function (e) {
                getMunicipios($("#select-departamento").val());
            });

            @can('ver.reporte_iglesias_asistencia_ingresos')
            $('#fechaMesG').datetimepicker({
                inline: true,
                sideBySide: true,
                viewMode: 'months',
                format: 'YYYY-MM'
            });
            $('#select-pais2').select2({
                theme: "bootstrap",
                placeholder: "Selecciona un pais...",
                allowClear: true,
            });

            $("#select-departamento2").select2({
                theme: "bootstrap",
                placeholder: "Selecciona un departamento...",
                allowClear: true,
            });

            $("#select-ciudad2").select2({
                allowClear: true,
                theme: "bootstrap",
                placeholder: "Selecciona una ciudad...",
            });

            $('#select-pais2').on('change', function (e) {
                getDepartamentos2($("#select-pais2").val());
            });

            $('#select-departamento2').on('change', function (e) {
                getMunicipios2($("#select-departamento2").val());
            });

            getPaises2();


            @endcan
        });

        function getPaises() {
            var COUNTRIES = [];
            $.ajax({
                url: "/getCountries",
                async: false
            }).done(function (data) {

                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    COUNTRIES.push(itemSelect2)
                }
                $('#select-pais').select2({
                    theme: "bootstrap",
                    placeholder: "Selecciona un pais...",
                    allowClear: true,
                    data: COUNTRIES
                });
                $("#select-pais").val('').trigger('change');
                //getDepartamentos(47);
                //verReporte(47, null, null);
                verReporte(null, null, null);
            });

        }

        function getDepartamentos(codPais) {

            let STATES = [];
            $('#select-departamento').empty();

            $.get(
                "/getStates", {'countrie': codPais},
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    STATES.push(itemSelect2)
                }

                $("#select-departamento").select2({
                    theme: "bootstrap",
                    placeholder: "Selecciona un departamento...",
                    allowClear: true,
                    data: STATES
                });

                $("#select-departamento").val("").trigger('change')

            });

        }

        function getMunicipios(codDepartamento) {
            var cities = [];

            $('#select-ciudad').empty();

            $.get(
                "/getCities", {'state': codDepartamento}
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    cities.push(itemSelect2)
                }

                $("#select-ciudad").select2({
                    allowClear: true,
                    theme: "bootstrap",
                    placeholder: "Selecciona una ciudad...",
                    data: cities
                });

                $("#select-ciudad").val("").trigger('change')
            });

        }

        function generarReporte() {
            verReporte($("#select-pais").val(), $("#select-departamento").val(), $("#select-ciudad").val());
        }

        function verReporte(pais, departamento, ciudad) {
            $('#cargando').show();
            $('#reporte1').hide();
            $("#iframe-reporte1").attr("src", "/reporte_iglesias/" + pais + '/' + departamento + '/' + ciudad);
        }

        $('#iframe-reporte1').on('load', function () {
            $('#cargando').hide();
            $('#reporte1').show();
            console.log('iframe loaded successfully');
        });

        @if(session()->get('id_iglesia', function () {return 'default';}) !="default")
        @can('ver.reporte_asistencia_ingresos')

        function verReporte2(fecha) {
            $('#cargando2').show();
            $('#reporte2').hide();
            $("#iframe-reporte2").attr("src", "/reporte_asistencia_ingresos/" + fecha);
        }

        $('#iframe-reporte2').on('load', function () {
            $('#cargando2').hide();
            $('#reporte2').show();
            console.log('iframe loaded successfully');
        });

        @endcan
        @endif



        @can('ver.reporte_iglesias_asistencia_ingresos')

        function getPaises2() {
            var COUNTRIES = [];
            $.ajax({
                url: "/getCountries",
                async: false
            }).done(function (data) {

                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    COUNTRIES.push(itemSelect2)
                }
                $('#select-pais2').select2({
                    theme: "bootstrap",
                    placeholder: "Selecciona un pais...",
                    allowClear: true,
                    data: COUNTRIES
                });
                $("#select-pais2").val('').trigger('change');
                //getDepartamentos(47);
                //verReporte(47, null, null);
                verReporte3(null, null, null, $("#fechaMesG").data("date"));
            });

        }

        function getDepartamentos2(codPais) {

            let STATES = [];
            $('#select-departamento').empty();

            $.get(
                "/getStates", {'countrie': codPais},
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    STATES.push(itemSelect2)
                }

                $("#select-departamento2").select2({
                    theme: "bootstrap",
                    placeholder: "Selecciona un departamento...",
                    allowClear: true,
                    data: STATES
                });

                $("#select-departamento2").val("").trigger('change')

            });

        }

        function getMunicipios2(codDepartamento) {
            var cities = [];

            $('#select-ciudad').empty();

            $.get(
                "/getCities", {'state': codDepartamento}
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    cities.push(itemSelect2)
                }

                $("#select-ciudad2").select2({
                    allowClear: true,
                    theme: "bootstrap",
                    placeholder: "Selecciona una ciudad...",
                    data: cities
                });

                $("#select-ciudad2").val("").trigger('change')
            });

        }

        function generarReporte2() {
            verReporte3($("#select-pais2").val(), $("#select-departamento2").val(), $("#select-ciudad2").val(), $("#fechaMesG").data("date"));
        }

        function verReporte3(pais, departamento, ciudad, fecha) {
            $('#cargando3').show();
            $('#reporte3').hide();
            $("#iframe-reporte3").attr("src", "/reporte_iglesias_asistencia_ingresos/" + pais + '/' + departamento + '/' + ciudad + '/' + fecha);
        }

        $('#iframe-reporte3').on('load', function () {
            $('#cargando3').hide();
            $('#reporte3').show();
            console.log('iframe loaded successfully');
        });

        @endcan

    </script>
@endsection
