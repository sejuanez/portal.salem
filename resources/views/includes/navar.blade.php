@php
    use App\Reuniones;
$reunionesG=null;
if (session()->get('id_iglesia', function () {return 'default';}) != "default") {
$reunionesG = Reuniones::where("iglesia_id", "=", session()->get('id_iglesia'))
                ->where("estado", "=", 0)
                ->orderBy('created_at', 'desc')
                ->take(5)
                ->get();
}
 if (Auth::user()->getRoleNames()[0] == 'Super Admin') {
     $reunionesG = Reuniones::where("estado", "=", 0)
                ->orderBy('created_at', 'desc')
                ->take(5)
                ->get();
 }
@endphp
<div class="main-header">
    <!-- Logo Header -->
    <div class="logo-header" data-background-color="blue">

        <a href="/" class="logo">
            <!--
            <img src="/assets/img/logo.svg" alt="navbar brand" class="navbar-brand">
            -->
            <h1 class="navbar-brand" style="color: #fffFFF;font-size: 10px;">MINISTERIO APOSTOLICO SALEM</h1>
        </a>
        <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse"
                data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="icon-menu"></i>
					</span>
        </button>
        <button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
        <div class="nav-toggle">
            <button class="btn btn-toggle toggle-sidebar">
                <i class="icon-menu"></i>
            </button>
        </div>
    </div>
    <!-- End Logo Header -->

    <!-- Navbar Header -->
    <nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">

        <div class="container-fluid">
            <div class="collapse" id="search-nav">


                <div class="form-group form-show-validation row">
                    <label for="select-iglesias-pastor"
                           class="col-lg-4 col-md-4 col-sm-4 mt-sm-2 text-right text-white">
                        Seleccionar Iglesia
                    </label>
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <select id="select-iglesias-pastor" name="select-iglesias-pastor" class="form-control"
                                style="width: 100%;">
                            <option value="default">Seleccionar...</option>
                        </select>
                    </div>
                </div>


            </div>
            <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
                <li class="nav-item toggle-nav-search hidden-caret">
                    <a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false"
                       aria-controls="search-nav">
                        <i class="fa fa-search"></i>
                    </a>
                </li>
                {{--
                                <li class="nav-item dropdown hidden-caret">
                                    <a class="nav-link dropdown-toggle" href="#" id="messageDropdown" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-envelope"></i>
                                    </a>
                                    <ul class="dropdown-menu messages-notif-box animated fadeIn" aria-labelledby="messageDropdown">
                                        <li>
                                            <div class="dropdown-title d-flex justify-content-between align-items-center">
                                                Messages
                                                <a href="#" class="small">Mark all as read</a>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="message-notif-scroll scrollbar-outer">
                                                <div class="notif-center">
                                                    <a href="#">
                                                        <div class="notif-img">
                                                            <img src="/assets/img/jm_denis.jpg" alt="Img Profile">
                                                        </div>
                                                        <div class="notif-content">
                                                            <span class="subject">Jimmy Denis</span>
                                                            <span class="block">
                                                                        How are you ?
                                                                    </span>
                                                            <span class="time">5 minutes ago</span>
                                                        </div>
                                                    </a>
                                                    <a href="#">
                                                        <div class="notif-img">
                                                            <img src="/assets/img/chadengle.jpg" alt="Img Profile">
                                                        </div>
                                                        <div class="notif-content">
                                                            <span class="subject">Chad</span>
                                                            <span class="block">
                                                                        Ok, Thanks !
                                                                    </span>
                                                            <span class="time">12 minutes ago</span>
                                                        </div>
                                                    </a>
                                                    <a href="#">
                                                        <div class="notif-img">
                                                            <img src="/assets/img/mlane.jpg" alt="Img Profile">
                                                        </div>
                                                        <div class="notif-content">
                                                            <span class="subject">Jhon Doe</span>
                                                            <span class="block">
                                                                        Ready for the meeting today.
                                                                    </span>
                                                            <span class="time">12 minutes ago</span>
                                                        </div>
                                                    </a>
                                                    <a href="#">
                                                        <div class="notif-img">
                                                            <img src="/assets/img/talha.jpg" alt="Img Profile">
                                                        </div>
                                                        <div class="notif-content">
                                                            <span class="subject">Talha</span>
                                                            <span class="block">
                                                                        Hi, Apa Kabar ?
                                                                    </span>
                                                            <span class="time">17 minutes ago</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <a class="see-all" href="javascript:void(0);">See all messages<i
                                                    class="fa fa-angle-right"></i> </a>
                                        </li>
                                    </ul>
                                </li>
                --}}

                <li class="nav-item dropdown hidden-caret">
                    <a class="nav-link dropdown-toggle" id="notifDropdown" role="button" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bell"></i>
                        @if($reunionesG!=null)
                            @if($reunionesG->count()>0)
                                <span class="notification">{{$reunionesG->count()}}</span>
                            @endif
                        @endif
                    </a>
                    <ul class="dropdown-menu notif-box animated fadeIn" aria-labelledby="notifDropdown">
                        <li>
                            <div class="dropdown-title">Tienes {{$reunionesG!=null?$reunionesG->count():0}} Reuniones
                                Pendientes
                            </div>
                        </li>
                        <li>
                            <div class="notif-scroll scrollbar-outer">
                                <div class="notif-center" id="reunionesPendientesNotificacion">
                                    @if($reunionesG!=null)
                                        @foreach($reunionesG as $reunion)
                                            <a href="{{route('reuniones')}}">
                                                <div class="notif-icon notif-primary"><i class="fa fa-bell"></i></div>
                                                <div class="notif-content">
													<span class="block">
														{{$reunion->nombre}}
													</span>
                                                    <span
                                                        class="time">{{  \Carbon\Carbon::parse($reunion->created_at)->diffForHumans()}}</span>
                                                </div>
                                            </a>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </li>
                        <li>
                            <a class="see-all" href="{{route('reuniones')}}">Ver Todas<i
                                    class="fa fa-angle-right"></i> </a>
                        </li>
                    </ul>
                </li>

                {{--<li class="nav-item dropdown hidden-caret">
                    <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fas fa-layer-group"></i>
                    </a>
                    <div class="dropdown-menu quick-actions quick-actions-info animated fadeIn">
                        <div class="quick-actions-header">
                            <span class="title mb-1">Quick Actions</span>
                            <span class="subtitle op-8">Shortcuts</span>
                        </div>
                        <div class="quick-actions-scroll scrollbar-outer">
                            <div class="quick-actions-items">
                                <div class="row m-0">
                                    <a class="col-6 col-md-4 p-0" href="#">
                                        <div class="quick-actions-item">
                                            <i class="flaticon-file-1"></i>
                                            <span class="text">Generated Report</span>
                                        </div>
                                    </a>
                                    <a class="col-6 col-md-4 p-0" href="#">
                                        <div class="quick-actions-item">
                                            <i class="flaticon-database"></i>
                                            <span class="text">Create New Database</span>
                                        </div>
                                    </a>
                                    <a class="col-6 col-md-4 p-0" href="#">
                                        <div class="quick-actions-item">
                                            <i class="flaticon-pen"></i>
                                            <span class="text">Create New Post</span>
                                        </div>
                                    </a>
                                    <a class="col-6 col-md-4 p-0" href="#">
                                        <div class="quick-actions-item">
                                            <i class="flaticon-interface-1"></i>
                                            <span class="text">Create New Task</span>
                                        </div>
                                    </a>
                                    <a class="col-6 col-md-4 p-0" href="#">
                                        <div class="quick-actions-item">
                                            <i class="flaticon-list"></i>
                                            <span class="text">Completed Tasks</span>
                                        </div>
                                    </a>
                                    <a class="col-6 col-md-4 p-0" href="#">
                                        <div class="quick-actions-item">
                                            <i class="flaticon-file"></i>
                                            <span class="text">Create New Invoice</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>--}}
                <li class="nav-item dropdown hidden-caret">
                    <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
                        <div class="avatar-sm">
                            <img src="/images/perfil/{{auth()->user()->imagen}}" class="avatar-img rounded-circle">
                        </div>
                    </a>
                    <ul class="dropdown-menu dropdown-user animated fadeIn">
                        <div class="dropdown-user-scroll scrollbar-outer">
                            <li>
                                <div class="user-box">
                                    <div class="avatar-lg">
                                        <img src="/images/perfil/{{auth()->user()->imagen}}" alt="image profile"
                                             class="avatar-img rounded">
                                    </div>
                                    <div class="u-text">
                                        <h4>
                                            {{ auth()->user()->persona->nombres }} {{ auth()->user()->persona->apellidos }}
                                        </h4>
                                        <p class="text-muted">
                                            {{ auth()->user()->getRoleNames()}}
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{route('perfil')}}">Mi Perfil</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}">Salir</a>
                            </li>
                        </div>
                    </ul>
                </li>
            </ul>
        </div>

    </nav>
    <!-- End Navbar -->
</div>

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function () {


            $("#select-iglesias-pastor").select2({
                theme: "bootstrap",
            });

            let SELECCIONADA = '{!!
                    session()->get('id_iglesia', function () {
                        return 'default';})
             !!}';

            cargarIglesias(SELECCIONADA)


            $('#select-iglesias-pastor').on('change', function (e) {
                seleccionarIglesia($('#select-iglesias-pastor').val());
            });


        });

        function cargarIglesias(seleccionada) {

            let IGLESIAS = [];
            $.get(
                "/recurso/verIglesias",
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].iglesia.id,
                        text: data.msg[item].iglesia.nombre
                    };
                    IGLESIAS.push(itemSelect2)
                }

                $("#select-iglesias-pastor").select2({
                    theme: "bootstrap",
                    allowClear: true,
                    data: IGLESIAS
                });


                $('#select-iglesias-pastor').val(seleccionada).trigger('change.select2');


            });

        }

        function seleccionarIglesia(id) {

            console.log(id)

            $.ajax({
                url: 'recurso/seleccionarIglesia',
                type: 'POST',
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    idIglesia: id
                },

            }).done(function (response) {

                console.log(response)

                location.href = '/'

            });

        }

    </script>

@append
