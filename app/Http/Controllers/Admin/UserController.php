<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::get();
        //return $users;
        //return datatables()->collection(User::get())->toJson();
        $array = array();

        foreach ($users as $usuario) {
            if ($usuario->persona == null) {
                $datos = [
                    'id' => $usuario->id,
                    'nombres' => "Error",
                    'apellidos' => "Error",
                    'correo' => $usuario->email,
                    'rol' => $usuario->roles,
                    'fecha_registro' => date("d/m/Y h:i A", strtotime($usuario->created_at)),
                    'estado' => $usuario->active ? '<span class="badge badge-success">Activo</span>' : '<span class="badge badge-danger">Inactivo</span>',
                    'activo' => $usuario->active,
                ];
            } else {
                $datos = [
                    'id' => $usuario->id,
                    'nombres' => $usuario->persona->nombres,
                    'apellidos' => $usuario->persona->apellidos,
                    'correo' => $usuario->email,
                    'rol' => $usuario->roles,
                    'fecha_registro' => date("d/m/Y h:i A", strtotime($usuario->created_at)),
                    'estado' => $usuario->active ? '<span class="badge badge-success">Activo</span>' : '<span class="badge badge-danger">Inactivo</span>',
                    'activo' => $usuario->active,
                ];
            }


            // if (Auth::user()->roles[0]['name'] == 'Super Admin' && $usuario->roles[0]['name'] == 'Super Admin') {
            if (in_array("Super Admin", auth()->user()->getRoleNames()->toArray()) Or in_array("Admin", auth()->user()->getRoleNames()->toArray())) {
                array_push($array, $datos);
            }
            //if ($usuario->roles[0]['name'] != 'Super Admin') {
            if (!in_array("Super Admin", auth()->user()->getRoleNames()->toArray())) {
                array_push($array, $datos);
            }

            //no dejar ver los super admin si no eres
        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'rol' => 'required',
        ]);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        if ($request->has('rol')) {
            $user->assignRole($request->rol);
        }
        if ($request->has('permissions')) {
            $user->givePermissionTo($request->permissions);
        }
        return response(['message' => 'Usuario Creado', 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'rol' => 'required',
        ]);
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);
        if ($request->has('password')) {
            $user->password = bcrypt($request->password);
            $user->save();
        }
        if ($request->has('rol')) {
            $user->syncRoles($request->rol);
        }
        if ($request->has('permissions')) {
            $user->syncPermissions(collect($request->permissions)->pluck('id')->toArray());
        }
        return response(['message' => 'Usuario Actualizado', 'user' => $user]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return response(['message' => 'Usuario Eliminado']);

    }

    /**
     * Actualizar contrase単a del usuario.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function actualizarPass(Request $request)
    {

        if ($request->id) {
            $usuario = User::findOrFail($request->id);
            $this->validate($request, [
                'pass' => 'required|string|min:6',
            ]);

            if (auth()->user()->rol_id != 1) {
                $response = array(
                    'status' => 'Error',
                    'msg' => "Error, Falta de privilegios",
                );
                return response()->json($response);

            } else {
                $usuario->password = bcrypt($request->pass);
                $usuario->save();
                $response = array(
                    'status' => 'success',
                    'msg' => 'la Contrase単a se ha cambiado correctamente',
                );
                return response()->json($response);

            }

        } else {

            $usuario = User::findOrFail(auth()->user()->id);
            $this->validate($request, [
                'passOld' => 'required|string',
                'pass' => 'required|string|min:6',
            ]);

            if (Hash::check($request->passOld, $usuario->password)) {

                $usuario->password = bcrypt($request->pass);
                $usuario->save();

                $response = array(
                    'status' => 'success',
                    'msg' => 'la Contrase単a se ha cambiado correctamente',
                );

            } else {
                $response = array(
                    'status' => 'Error',
                    'msg' => "la Contrase単a actual no es correcta",
                );
            }

            return response()->json($response);

        }

    }

    /**
     * Actualizar foto de perfil.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update_profile(Request $request)
    {

        $this->validate($request, [
            'avatar' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);
        //mensaje la imagen no se puede subir
        $path = "images/perfil/";

        $filename = Auth::id() . '_' . time() . '.' . $request->avatar->getClientOriginalExtension();
        move_uploaded_file($request->file('avatar'), $path . $filename);

        $user = Auth::user();
        $user->imagen = $filename;
        $user->save();

        $response = array(
            'status' => 'success',
            'imagen' => $filename,
            'msg' => "La Imagen Se Cargo Correctamente",
        );
        return response()->json($response);

    }


}
