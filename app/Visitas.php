<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitas extends Model
{
    //
    protected $fillable = [
        'coordinador_id',
        'iglesia_id',
        'fecha_visita',
        'hora_inicio',
        'hora_fin',
        'limpio',
        'cartelera',
        'alfoli',
        'host_atten',
        'host_people_loc',
        'music',
        'dance',
        'distr',
        'music_love',
        'music_rep',
        'bible_use',
        'reflex',
        'pray_time',
        'leader_vision',
        'balance',
        'unction_preach',
        'jesus_center',
        'pulpit_admin',
        'preach_ability',
        'god_depend',
        'leader_love_work',
        'personal_presentation',
        'op_sex_behavior',
        'child_site',
        'child_atten',
        'teacher_prep',
        'minis_unction',
        'minis_music',
        'minis_child',
        'leader_atten',
        'people_close_to_leader',
        'spouse_behavior',
        'subjection',
        'intersec',
        'inversiones',
        'sat_assist',
        'inicio',
        'alabanza',
        'oracion',
        'predicacion',
        'maestro_ninos',
        'ministracion',
        'recomendaciones',
        'estado',
    ];


    public function coordinador()
    {
        return $this->belongsTo(User::class, 'coordinador_id');
    }


    public function iglesia()
    {
        return $this->belongsTo(Iglesias::class, 'iglesia_id');
    }
}
