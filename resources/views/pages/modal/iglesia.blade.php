<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="accion-modal">Nueva</span>
                    <span class="fw-light">Iglesia</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="iglesia-form">
                    @csrf
                    <input type="hidden" id="id">
                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="name" class="text-right">
                                    Nombre
                                    <span class="required-label">*</span>
                                </label>
                                <input type="text" class="form-control" id="nombre" name="nombre"
                                       placeholder="Nombre de la iglesia" required="">
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="name" class="text-right">
                                    Direccion
                                    <span class="required-label">*</span>
                                </label>
                                <input type="text" class="form-control" id="direccion" name="direccion"
                                       autocomplete="nio"
                                       placeholder="Direccion de la iglesia" required="">
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="name" class="text-right">
                                    Barrio
                                    <span class="required-label">*</span>
                                </label>
                                <input type="text" class="form-control" id="barrio" name="barrio"
                                       placeholder="Barrio de la iglesia" required="">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="select-pais">Pais</label>
                                <select class="form-control" name="select-pais" id="select-pais" style="width: 100%">
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="select-departamento">Departamento/Estado</label>
                                <select class="form-control" name="select-departamento" id="select-departamento"
                                        style="width: 100%">
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="city_id">Ciudad</label>
                                <select class="form-control" name="city_id" id="city_id" style="width: 100%">
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="select-tipo">Tipo de iglesia</label>
                                <select class="form-control" id="tipo" name="tipo">
                                    <option value="Grupo">Grupo</option>
                                    <option value="Iglesia">Iglesia</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="name" class="text-right">
                                    Latitud
                                    <span class="required-label">*</span>
                                </label>
                                <input type="text" class="form-control" id="latitud" name="latitud"
                                       placeholder="Latitud de la iglesia" required="">
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="name" class="text-right">
                                    Longitud
                                    <span class="required-label">*</span>
                                </label>
                                <input type="text" class="form-control" id="longitud" name="longitud"
                                       placeholder="Longitud de la iglesia" required="">
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger text-danger" id="error" style="display: none">
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="guardar" class="btn btn-primary">Guardar</button>
                <button type="button" id="actualizar" class="btn btn-primary">Actualizar</button>
                <button type="button" id="eliminar" class="btn btn-warning">Eliminar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
