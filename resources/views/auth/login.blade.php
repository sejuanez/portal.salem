@extends('layouts.app')

@section('content')

    @php

        session()->forget('id_iglesia');

    @endphp

    <div class="container container-login container-transparent animated fadeIn">
        <h3 class="text-center">{{ __('¡Hola!') }} </br> {{ __('Ingresa tu correo y contraseña') }}</h3>
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="login-form">
                <div class="form-group">
                    <label for="email" class="placeholder"><b>{{ __('Correo') }}</b></label>
                    <input id="email" type="email"
                           class="form-control @error('email') is-invalid @enderror" name="email"
                           value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="password" class="placeholder"><b>{{ __('Contraseña') }}</b></label>
                    @if (Route::has('password.request'))
                        <a class="link float-right" href="{{ route('password.request') }}">
                            {{ __('Olvidaste tu contraseña?') }}
                        </a>
                    @endif

                    <div class="position-relative">
                        <input id="password" type="password"
                               class="form-control @error('password') is-invalid @enderror" name="password"
                               required autocomplete="current-password">
                        <div class="show-password">
                            <i class="icon-eye"></i>
                        </div>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>
                <div class="form-group form-action-d-flex mb-3">
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" name="remember"
                               id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="custom-control-label m-0" for="remember">{{ __('Recordar') }}</label>
                    </div>
                    <button type="submit" class="btn btn-secondary col-md-5 float-right mt-3 mt-sm-0 fw-bold">
                        {{ __('Ingresar') }}
                    </button>
                </div>
                <div class="login-account">
                    <span class="msg">{{ __('No tienes una cuenta ?') }}</span>
                    <a href="{{ route('registroPersonas') }}" id="show-signup" class="link">{{ __('Registrate') }}</a>
                </div>
            </div>
        </form>

    </div>
@endsection
