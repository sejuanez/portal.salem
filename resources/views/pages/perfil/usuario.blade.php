@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Perfil: </h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="/usuarios">Usuarios</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">{{$usuario->persona->nombres}}</a>
                    </li>
                </ul>
            </div>


            <div class="row">
                <div class="col-md-9">
                    <div class="card card-with-nav">
                        <div class="card-header">
                            <div class="row row-nav-line">
                                <ul class="nav nav-tabs nav-line nav-color-secondary" role="tabpanel">
                                    <li class="nav-item">
                                        <a class="nav-link active show" data-toggle="tab" href="#home-tab" id="info-tab"
                                           role="tab" aria-selected="true">Informacion</a>
                                    </li>
                                    @if(auth()->user()->id==$usuario->id)
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#profile-tab" role="tab"
                                               aria-selected="false">Contraseña</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#image-tab" role="tab"
                                               aria-selected="false">Imagen</a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>

                        <div class="card-body">

                            <div class="tab-content mt-2 mb-3" id="pills-tabContent">

                                <div class="tab-pane fade show active" id="home-tab" role="tabpanel"
                                     aria-labelledby="home-tab">
                                    <div class="row">

                                        <div class="col-sm-10">
                                            <h2>Informacion de perfil</h2>
                                        </div>

                                        <div class="col-sm-2">
                                            <button type="button" id="edit-profile"
                                                    class="btn btn-icon btn-link pull-right editar"
                                                    data-toggle="tooltip"
                                                    data-placement="top" title="Actualizar" data-original-title="Edit"
                                                    style="margin-right: 5px;">
                                                <i class="fas fa-pen"></i>
                                            </button>
                                            <button type="button" id="btn-cancel"
                                                    onclick="cancelar()"
                                                    class="btn btn-icon btn-link pull-right editar"
                                                    data-toggle="tooltip"
                                                    data-placement="top" title="Cancelar" data-original-title="Edit"
                                                    style="margin-right: 5px; display: none">
                                                <i class="fas fa-times"></i>
                                            </button>
                                        </div>


                                    </div>

                                    <div id="div-informacion">
                                        <div class="row">
                                            <div class="col-lg-12 col-xl-6">
                                                <table class="table m-0">
                                                    <tbody>
                                                    <tr>
                                                        <th scope="row">Nombres</th>
                                                        <td>{{$usuario->persona->nombres}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Apellidos</th>
                                                        <td>{{$usuario->persona->apellidos}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Correo</th>
                                                        <td>{{$usuario->email}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Identificacion</th>
                                                        <td>{{$usuario->persona->cedula}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Telefono</th>
                                                        <td>{{$usuario->persona->telefono}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Fecha Nacimiento</th>
                                                        <td>{{$usuario->persona->fechaNacimiento}}</td>
                                                    </tr>

                                                    <tr>
                                                        <th scope="row">Año Nacimiento Hijos</th>
                                                        <td>{{$usuario->persona->nacimientoHijos}}</td>
                                                    </tr>

                                                    <tr>
                                                        <th scope="row">Estado Civil</th>
                                                        <td>{{$usuario->persona->estadoCivil}}</td>
                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- end of table col-lg-6 -->
                                            <div class="col-lg-12 col-xl-6">
                                                <table class="table">
                                                    <tbody>

                                                    <tr>
                                                        <th scope="row">Grado Escolaridad</th>
                                                        <td>{{$usuario->persona->escolaridad}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Estudios/Profesión</th>
                                                        <td>{{$usuario->persona->estudios}}</td>
                                                    </tr>

                                                    <tr>
                                                        <th scope="row">Trabajo Actual u Oficio</th>
                                                        <td>{{$usuario->persona->trabajo}}</td>
                                                    </tr>

                                                    <tr>
                                                        <th scope="row">Idiomas</th>
                                                        <td>{{$usuario->persona->idiomas}}</td>
                                                    </tr>

                                                    <tr>
                                                        <th scope="row">Formacion Musical</th>
                                                        <td>{{$usuario->persona->formacionMusical}}</td>
                                                    </tr>

                                                    <tr>
                                                        <th scope="row">Fecha de Ingreso a Iglesia</th>
                                                        <td>{{$usuario->persona->fechaIngreso}}</td>
                                                    </tr>

                                                    <tr>
                                                        <th scope="row">Recorrido en la Iglesia</th>
                                                        <td>{{$usuario->persona->recorridoIglesia}}</td>
                                                    </tr>


                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- end of table col-lg-6 -->
                                        </div>
                                    </div>

                                    <div id="div-form" style="display: none">
                                        <form novalidate="novalidate" autocomplete="off" id="form-datos">
                                            <div class="tab-pane active show" id="about">

                                                <div class="row">
                                                    <!--
                                                    <div class="col-md-12">
                                                        <h4 class="info-text">Tell us who you are.</h4>
                                                    </div>
                                                    -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Nombres:</label>
                                                            <input name="nombres" autocomplete="nio" type="text"
                                                                   value="{{$usuario->persona->nombres}}"
                                                                   required
                                                                   class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Apellidos:</label>
                                                            <input name="apellidos" autocomplete="nio" type="text"
                                                                   value="{{$usuario->persona->apellidos}}"
                                                                   required
                                                                   class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Correo:</label>
                                                            <input name="email" autocomplete="nio" type="email"
                                                                   value="{{$usuario->email}}"
                                                                   required
                                                                   class="form-control success">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Cedula:</label>
                                                            <input autocomplete="nio" type="number"
                                                                   value="{{$usuario->persona->cedula}}"
                                                                   required
                                                                   disabled
                                                                   class="form-control success">
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="col-md-4">
                                                        <div class="form-group ">
                                                            <label>Telefono: </label>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <select name="indicativo" id="indicativo"
                                                                            class="form-control" required>
                                                                        <option selected="" value="+57"> +57 Colombia
                                                                        </option>
                                                                        <option value="+58"> +58 Venezuela</option>
                                                                        <option value="+507"> +507 Panama</option>
                                                                        <option value="+593"> +593 Ecuador</option>
                                                                        <option value="+54"> +54 Argentida</option>
                                                                        <option value="+1"> +1 Estados Unidos</option>
                                                                    </select>
                                                                    <label id="indicativo-error" class="error"
                                                                           for="indicativo"
                                                                           style="display: none;">
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <input name="telefono" id="telefono"
                                                                           autocomplete="nio"
                                                                           type="number"
                                                                           required
                                                                           class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Fecha de Nacimiento</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control"
                                                                       value="{{$usuario->persona->fechaNacimiento}}"
                                                                       required
                                                                       id="fechaNacimiento"
                                                                       name="fechaNacimiento">

                                                                <div class="input-group-append">
														<span class="input-group-text">
															<i class="fa fa-calendar-check"></i>
														</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label>Año Nacimiento Hijos:</label>
                                                            <input name="nacimientoHijos" id="nacimientoHijos"
                                                                   value="{{$usuario->persona->nacimientoHijos}}"
                                                                   required
                                                                   data-toggle="tooltip" data-html="true" title=""
                                                                   autocomplete="new-password" type="text"
                                                                   class="form-control" data-original-title="
                                                       <br>
                                                <span>
                                                Si NO tiene Hijos escriba NINGUNO
                                                <br><br>
                                                Si tiene hijos escriba el AÑO de nacimiento (con 4 digitos) de cada uno de ellos
                                                separado por comas y sin espacios. No escriba comas ni puntos al final del campo.
                                                <br><br>
                                                <u>Ejemplo: 1997,2005</u>
                                                <br><br>
                                                </span>
                                                ">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group ">
                                                            <label>Estado Civil: </label>

                                                            <select name="estadoCivil" id="estadoCivil"
                                                                    class="form-control" required="">
                                                                <option selected="" value="Soltero">Soltero</option>
                                                                <option value="Casado">Casado</option>
                                                                <option value="Viudo">Viudo</option>
                                                                <option value="Divorciado">Divorciado</option>
                                                            </select><label id="estadoCivil-error" class="error"
                                                                            for="estadoCivil"
                                                                            style="display: none;"></label>
                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="row">

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Grado de escolaridad: </label>

                                                            <select name="escolaridad" id="escolaridad"
                                                                    class="form-control" required="">
                                                                <option value="Primaria">Primaria</option>
                                                                <option selected value="Bachiller">Bachiller</option>
                                                                <option value="Tecnólogo">Tecnólogo</option>
                                                                <option value="Profesional">Profesional</option>
                                                                <option value="Postgrado">Postgrado</option>
                                                            </select><label id="escolaridad-error" class="error"
                                                                            for="escolaridad"
                                                                            style="display: none;"></label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Estudios/Profesión:</label>
                                                            <input name="estudios" autocomplete="nio" type="text"
                                                                   value="{{$usuario->persona->estudios}}"
                                                                   required
                                                                   class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Trabajo Actual u Oficio:</label>
                                                            <input name="trabajo" autocomplete="nio" type="text"
                                                                   value="{{$usuario->persona->trabajo}}"
                                                                   required
                                                                   class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Idiomas:</label>
                                                            <input name="idiomas" id="idiomas" data-toggle="tooltip"
                                                                   value="{{$usuario->persona->idiomas}}"
                                                                   required
                                                                   data-html="true" title="" autocomplete="new-password"
                                                                   type="text" class="form-control"
                                                                   data-original-title="
                                                       <br>
                                                <span>
                                                Escriba los idiomas que conoce INCLUIDO EL NATIVO separado por comas (,) indicando el
                                                nivel entre parentesis (Bajo, Medio, Alto).
                                                <br><br>
                                                Ejemplo:
                                                <br><br>
                                                <u>Español (Alto), Ingles (Bajo)</u>
                                                <br><br>
                                                </span>
                                                ">
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Formacion Musical:</label>
                                                            <input name="formacionMusical" id="formacionMusical"
                                                                   value="{{$usuario->persona->formacionMusical}}"
                                                                   required
                                                                   data-toggle="tooltip" data-html="true" title=""
                                                                   autocomplete="new-password" type="text"
                                                                   class="form-control" data-original-title="
                                                       <br>
                                                <span>
                                                Si NO toca instrumentos escriba NINGUNO
                                                <br><br>
                                                Escriba los instrumentos que toca separado por comas (,) indicando el
                                                nivel entre parentesis (Bajo, Medio, Alto).
                                                <br><br>
                                                Ejemplo:
                                                <br><br>
                                                <u>Guitarra (Medio), Canto (Alto)</u>
                                                <br><br>
                                                </span>
                                                ">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Fecha de Ingreso a Iglesia</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control"
                                                                       value="{{$usuario->persona->fechaIngreso}}"
                                                                       required
                                                                       id="fechaIngreso"
                                                                       name="fechaIngreso">
                                                                <div class="input-group-append">
														<span class="input-group-text">
															<i class="fa fa-calendar-check"></i>
														</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Recorrido en la Iglesia:</label>
                                                            <input name="recorridoIglesia" id="recorridoIglesia"
                                                                   value="{{$usuario->persona->recorridoIglesia}}"
                                                                   required
                                                                   data-toggle="tooltip" data-html="true" title=""
                                                                   autocomplete="new-password" type="text"
                                                                   class="form-control" data-original-title="
                                                       <br>
                                                <span>
                                                Escriba los instrumentos que toca separado por comas (,) indicando el
                                                nivel entre parentesis (Bajo, Medio, Alto).
                                                <br><br>
                                                Ejemplo:
                                                <br><br>
                                                <u>Líder (Grupo familiar Aquine),Pastor (Iglesia Bogotá Norte) </u>
                                                <br><br>
                                                </span>
                                                ">
                                                        </div>
                                                    </div>

                                                </div>


                                            </div>
                                        </form>

                                        <div class="text-center" style="padding-top: 10px">
                                            <a href="#!" onclick="actualizarUsuario()"
                                               class="btn btn-primary waves-effect waves-light m-r-20">Actualizar</a>
                                            <a href="#!" id="edit-cancel"
                                               onclick="cancelar()"
                                               class="btn btn-danger waves-effect">Cancelar</a>
                                        </div>

                                    </div>


                                </div>
                                @if(auth()->user()->id==$usuario->id)
                                    <div class="tab-pane fade" id="profile-tab" role="tabpanel"
                                         aria-labelledby="profile-tab">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h6 class="card-header-text">Cambio de Contraseña:</h6>


                                                <form id="form-password">
                                                    <div class="form-group align-items-center">
                                                        <label class="col-form-label">Contraseña Actual:</label>
                                                        <input type="text" class="form-control" id="passOld">

                                                        <label class="col-form-label">Contraseña nueva:</label>
                                                        <input type="text" class="form-control" id="pass">

                                                        <label class="col-form-label">Contraseña nueva
                                                            (Confirmacion):</label>
                                                        <input type="text" class="form-control" id="pass2">
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="alert alert-danger" id="Perror"
                                                             style="display: none">
                                                        </div>
                                                    </div>

                                                </form>
                                                <button type="button" class="btn btn-primary waves-effect waves-light"
                                                        onclick="cambiarPass()">Confirmar
                                                </button>


                                                <!-- end of project table -->
                                            </div>
                                            <div class="col-sm-6"></div>
                                            <!-- end of col-lg-12 -->
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="image-tab" role="tabpanel"
                                         aria-labelledby="image-tab">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h6 class="card-header-text">Cambio de Imagen:</h6>
                                                <div class="input-file input-file-image">
                                                    <img class="img-upload-preview img-circle" width="200" height="200"
                                                         src="/images/perfil/{{$usuario->imagen}}" alt="preview">
                                                    <input type="file" class="form-control form-control-file"
                                                           id="uploadImg1" name="avatar" accept="image/*" required="">
                                                    <label for="uploadImg1"
                                                           class="  label-input-file btn btn-default btn-round">
													<span class="btn-label">
														<i class="fa fa-file-image"></i>
													</span>
                                                        Cambiar Imagen
                                                    </label>
                                                </div>
                                                <div class="form-group">
                                                    <div class="alert alert-danger" id="error"
                                                         style="display: none">
                                                    </div>
                                                </div>
                                                <!-- end of project table -->
                                            </div>
                                            <div class="col-sm-6"></div>
                                            <!-- end of col-lg-12 -->
                                        </div>
                                    </div>
                                @endif

                            </div>

                        </div>

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-profile">
                        <div class="card-header" style="background-image: url('/assets/img/blogpost.jpg')">
                            <div class="profile-picture">
                                <div class="avatar avatar-xl">
                                    <img id="perfil" src="/images/perfil/{{$usuario->imagen}}" alt="..."
                                         class="avatar-img rounded-circle">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="user-profile text-center">
                                <div class="name">{{$usuario->persona->nombres}}</div>
                                <div class="job">{{$usuario->email}}</div>
                                <div class="desc">{{ $usuario->getRoleNames()[0]}}</div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row user-stats text-center">
                                <div class="col">
                                    <div class="number">{{$usuario->created_at}}</div>
                                    <div class="title">Fecha Registro.</div>
                                </div>
                                <div class="col">
                                    <div class="number">{{$usuario->persona->telefono}}</div>
                                    <div class="title">Telefono</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')

    <script src="/assets/js/plugin/form/jquery.validate.js"></script>

    <script type="text/javascript">

        var USUARIO = {!! $usuario!!};

        var $validator = $('#form-datos').validate({

            validClass: "success",
            highlight: function (element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            }
        });

        function actualizarUsuario() {
            let formData = new FormData(document.getElementById("form-datos"));
            formData.append('id', USUARIO.persona.id);

            console.log('printing data...');
            let csrfToken = '{{ csrf_token() }}';


            $.ajax({
                url: '/actualizar',
                type: 'POST',
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                data: formData,
                processData: false,
                contentType: false,
                cache: false
            }).done(function (response) {

                console.log(response);

                swal({
                    title: "OK!",
                    text: "Actualizado correctamente!",
                    icon: "success",
                    onClose: reload,
                    buttons: {
                        confirm: {
                            className: 'btn btn-success'
                        }
                    },
                }).then(
                    function () {
                        location.reload()
                    }
                );


            }).fail(function (error) {
                console.log(error);

                let obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {

                    swal("Error!", value[0], {
                        icon: "error",
                        buttons: {
                            confirm: {
                                className: 'btn btn-danger'
                            }
                        },
                    });

                });

            });


        }

        function cambiarPass() {

            if ($("#pass").val() != $("#pass2").val()) {
                $("#Perror").html("Las Contraseña No Son Iguales");
                $("#Perror").show();
                return;
            }
            $("#Perror").hide();

            $.post(
                "/actualizarPass", {
                    pass: $("#pass").val(),
                    passOld: $("#passOld").val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            ).done(function (data) {
                console.log(data);
                if (data.status == "Error") {
                    $("#Perror").html(data.msg);
                    $("#Perror").show();
                } else {

                    $.notify({
                        icon: 'flaticon-success',
                        title: 'Felicidades',
                        message: data.msg,
                    }, {
                        type: 'success',
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        time: 1000,
                    });

                    //$('#info-tab').click();

                    $("#form-password")[0].reset();
                    $("#Perror").hide();
                }

            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Perror").html(value[0]);
                    $("#Perror").show();
                });

            });
        }

        $('#uploadImg1').change(function () {
            var data = new FormData();
            jQuery.each(jQuery('#uploadImg1')[0].files, function (i, file) {
                data.append('avatar', file);
            });
            data.append('_token', $('meta[name="csrf-token"]').attr('content'));
            $.ajax({
                url: '/recurso/cambiar-avatar',
                type: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false
            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {
                    $("#error").hide();
                    $("#perfil").attr("src", '/images/perfil/' + response.imagen);
                    $.notify({
                        icon: 'flaticon-success',
                        title: 'Felicidades',
                        message: response.msg,
                    }, {
                        type: 'success',
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        time: 1000,
                    });
                }
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });

            });
        });

        $("#edit-profile").on('click', function () {

            $('#div-informacion').hide()
            $('#edit-profile').hide()
            $('#btn-cancel').show()
            $('#div-form').show()
            $('#div-form').addClass('animated fadeIn');

        });

        function cancelar() {
            $('#div-form').hide();
            $('#btn-cancel').hide();
            $('#div-informacion').show();
            $('#edit-profile').show();
            $('#div-informacion').addClass('animated fadeIn');
        }

        function cargarDatos(usuario) {
            let arrayTel = (usuario.persona.telefono).split(" ")

            $('#indicativo').val(arrayTel[0]);
            $('#telefono').val(arrayTel[1]);
            $('#estadoCivil').val(usuario.persona.estadoCivil);
            $('#escolaridad').val(usuario.persona.escolaridad);

        }

        function reload() {
            location.reload()
        }

        $(document).ready(function () {

            console.log(USUARIO)
            cargarDatos(USUARIO)

            $('#fechaNacimiento').datetimepicker({
                format: 'YYYY/MM/DD',
            });

            $('#fechaIngreso').datetimepicker({
                format: 'YYYY/MM/DD',
            });

            $('#fechaObra').datetimepicker({
                format: 'YYYY/MM/DD',
            });

        });

    </script>

@endsection
