@extends('layouts.dashboard')

@section('content')

    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Siervos</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Siervos</a>
                    </li>
                </ul>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">Siervos de la iglesia</h4>

                                @can('crear.miembros')
                                    <button class="btn btn-primary btn-round ml-auto" id="modalAgregarMiembro">
                                        <i class="fa fa-plus"></i>
                                        Agregar Siervos
                                    </button>
                                    {{--                                    <button class="btn btn-primary btn-round" id="modalGuardar">--}}
                                    {{--                                        <i class="fa fa-plus"></i>--}}
                                    {{--                                        Nuevo Siervos--}}
                                    {{--                                    </button>--}}
                                @endcan
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <table id="tablaUsuarios" class="display table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Telefono</th>
                                        <th>Rol</th>
                                        <th class="text-right">Opciones</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Telefono</th>
                                        <th>Rol</th>
                                        <th class="text-right">Opciones</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages.modal.miembro')

@endsection

@section('scripts')
    <script type="text/javascript">

        $('#tablaUsuarios thead th').each(function () {
            var title = $(this).text();
            $(this).html(title + ' <input type="text" class="form-control" style="height: auto !important;" placeholder="Buscar ' + title + '" />');
        });
        var TABLA = $('#tablaUsuarios').DataTable({
            ajax: {
                url: "{{ url('recurso/miembros') }}",
                "dataSrc": function (data) {
                    var json = [];
                    console.log(data);
                    for (var item in data.msg) {
                        var itemJson = {
                            id: data.msg[item].id,
                            id_old: data.msg[item].id_old,
                            nombres: data.msg[item].nombres,
                            apellidos: data.msg[item].apellidos,
                            NombresYApellidos: data.msg[item].nombres + ' ' + data.msg[item].apellidos,
                            telefono: data.msg[item].telefono,
                            rol: data.msg[item].rol,
                            Opciones: opciones(data.msg[item].rol)
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            columns: [
                {data: "NombresYApellidos"},
                {data: "telefono"},
                {data: "rol"},
                {data: "Opciones"},
            ],
        });

        TABLA.columns().every(function () {
            var table = this;
            $('input', this.header()).on('keyup change', function () {
                if (table.search() !== this.value) {
                    table.search(this.value).draw();
                }
            });
        });

        function opciones(rol) {
            if (rol == 'Miembro_old') {
                return "";
            }
            var opciones = '';
            @can('editar.miembros')
                opciones += '<button type="button" class="btn btn-primary btn-xs editar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Actualizar" data-original-title="Edit"' +
                '           style="margin-right: 5px;">\n' +
                '           <i class="fas fa-pen"></i>\n' +
                ' </button>';
            @endcan
            /*   opciones += '' +
               '<button type="button" class="btn btn-success btn-xs ver" ' +
               '           data-toggle="tooltip" data-placement="top" title="Ver Perfil" data-original-title="Edit"' +
               '           style="margin-right: 5px;">\n' +
               '           <i class="fas fa-eye"></i>\n' +
               ' </button>';*/

            @can('eliminar.miembros')

            if (rol !== 'Pastor') {
                opciones += '' +
                    '<button type="button" class="btn btn-danger btn-xs cambiarEstado" ' +
                    '           data-toggle="tooltip" data-placement="top" title="Deshabilitar" data-original-title="Edit">' +
                    '           <i class="fas fa-trash"></i>\n' +
                    ' </button>';
            } else {
                @can('eliminar.pastor')
                    opciones += '' +
                    '<button type="button" class="btn btn-danger btn-xs cambiarEstado" ' +
                    '           data-toggle="tooltip" data-placement="top" title="Deshabilitar" data-original-title="Edit">' +
                    '           <i class="fas fa-trash"></i>\n' +
                    ' </button>';
                @endcan
            }


            @endcan
                return opciones;
        }

        @can('crear.miembros')
        $("#modalGuardar").on('click', function () {
            $('#modal').modal('show');
            $('#form-miembro')[0].reset();
            $('#actualizar').hide();
            $('#guardar').show();
            $('#eliminar').hide();
            $("#error").hide();
            $("#accion-modal").html('Registrar');

        });

        $("#guardar").on('click', function () {
            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#guardar").prop('disabled', true);
            $.ajax({
                url: '{{url('recurso/agregarMimebro')}}',
                type: 'POST',
                data: $("#form-miembro").serialize(),
            }).done(function (response) {
                $('#form-miembro')[0].reset();
                $('#modal').modal('hide');
                $("#guardar").prop('disabled', false);
                TABLA.ajax.reload();

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.message,
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#guardar").prop('disabled', false);
            });

        });

        $("#modalAgregarMiembro").on('click', function () {
            $('#modal-agregarMiembro').modal('show');
            $('#miembro-iglesia-form')[0].reset();
            $("#error_miembro").hide();
            $("#accion-modal").html('Registrar');
            $("#select-miembro").trigger('change')
        });

        function asignarmiembro() {
            $("#error_miembro").hide();
            $('#modal-agregarMiembro .modal-content').addClass("is-loading");
            $("#asignarMiembro").prop('disabled', true);
            $.ajax({
                url: '{{url('recurso/agregarMimebro')}}',
                type: 'POST',
                data: $("#miembro-iglesia-form").serialize(),
            }).done(function (response) {
                $('#miembro-iglesia-form')[0].reset();
                $('#modal-agregarMiembro').modal('hide');
                $("#asignarMiembro").prop('disabled', false);
                $("#select-miembro").trigger('change')
                TABLA.ajax.reload();

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.message,
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error_miembro").html(value[0]);
                    $("#error_miembro").show();
                });


            }).always(function () {
                $('#modal-agregarMiembro .modal-content').removeClass("is-loading");
                $("#asignarMiembro").prop('disabled', false);
            });

        }

        function getMiembross() {

            let MIEMBROS = [];

            $.ajax({
                url: "/recurso/users",
                async: false
            }).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].nombres + ' ' + data.msg[item].apellidos
                    };
                    MIEMBROS.push(itemSelect2)
                }
                $('#select-miembro').select2({
                    theme: "bootstrap",
                    dropdownParent: $('#modal-agregarMiembro'),
                    placeholder: "Selecciona un Miembro",
                    allowClear: true,
                    data: MIEMBROS
                });
                $("#select-miembro").trigger('change')

            });

        }

        getMiembross();

        @endcan

        TABLA.on('click', '.cambiarEstado', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            swal({
                title: 'Estas seguro?',
                text: "Vas a Eliminar el miembro : " + data.NombresYApellidos,
                icon: 'warning',
                buttons: {
                    confirm: {
                        text: 'Si, Eliminar',
                        className: 'btn btn-success'
                    },
                    cancel: {
                        text: 'Cancelar',
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((Delete) => {
                if (Delete) {
                    $.ajax({
                            url: 'recurso/eliminarMimebro',
                            type: 'POST',
                            data: {
                                persona_id: data.id,
                                _token: $('meta[name="csrf-token"]').attr('content')
                            },
                        }
                    ).done(function (response) {
                        console.log(response);
                        if (response.status == "Error") {
                            //error
                        } else {
                            TABLA.ajax.reload();
                            swal({
                                title: 'Ok!',
                                text: 'El miembro ' + data.NombresYApellidos + ' ha sido eliminado',
                                icon: 'success',
                                buttons: {
                                    confirm: {
                                        className: 'btn btn-success'
                                    }
                                }
                            });
                        }
                        //return response;
                    }).fail(function (error) {
                        console.log(error);
                    });
                } else {
                    swal.close();
                }
            });

        });

        @can('editar.miembros')
        TABLA.on('click', '.editar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            $("#modal").modal('show');
            $('#form-miembro')[0].reset();
            $('#actualizar').show();
            $('#eliminar').hide();
            $('#guardar').hide();
            $("#accion-modal").html('Editar');

            $("#id").val(data.id);
            $("#nombres").val(data.nombres);
            $("#apellidos").val(data.apellidos);
            $("#telefono").val(data.telefono);
        });

        $("#actualizar").on('click', function () {
            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#actualizar").prop('disabled', true);
            $.ajax({
                url: '{{url('recurso/actualizarMiembro')}}',
                type: 'POST',
                data: $("#form-miembro").serialize(),
            }).done(function (response) {
                $('#form-miembro')[0].reset();
                $('#modal').modal('hide');
                TABLA.ajax.reload();

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.message,
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#actualizar").prop('disabled', false);
            });

        });

        @endcan


    </script>
@endsection
