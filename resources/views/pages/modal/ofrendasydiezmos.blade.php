<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="accion-modal">Nueva</span>
                    <span class="fw-light">Ofrendas y diezmos</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-ofrendasydiezmos">
                    @csrf
                    <input type="hidden" id="id" name="id">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="name" class="text-right">
                                    Descripcion
                                    <span class="required-label">*</span>
                                </label>
                                <input type="text" class="form-control" id="descripcion" name="descripcion"
                                       placeholder="Ingresa una descripcion del momento" required="">
                            </div>
                        </div>

                        {{--                        <div class="col-sm-6">--}}
                        {{--                            <div class="form-group">--}}
                        {{--                                <label for="name" class="text-right">--}}
                        {{--                                    Diezmo--}}
                        {{--                                    <span class="required-label">*</span>--}}
                        {{--                                </label>--}}
                        {{--                                <input type="number" class="form-control" id="diezmo" name="diezmo" required="">--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name" class="text-right">
                                    Ingresos
                                    <span class="required-label">*</span>
                                </label>
                                <input type="number" class="form-control" id="ofrenda" name="ofrenda" required="">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name" class="text-right">
                                    Fecha
                                    <span class="required-label">*</span>
                                </label>
                                <input type="date" class="form-control" id="fecha" name="fecha" required="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger text-danger" id="error" style="display: none">
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="guardar" class="btn btn-primary">Guardar</button>
                <button type="button" id="actualizar" class="btn btn-primary">Actualizar</button>
                <button type="button" id="eliminar" class="btn btn-warning">Eliminar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
