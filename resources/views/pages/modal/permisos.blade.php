<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="accion-modal">Nuevo</span>
                    <span class="fw-light">Permiso</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="permisos-form">
                    @csrf
                    <input type="hidden" id="id">
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="name" class="text-right">
                                    Nombre
                                    <span class="required-label">*</span>
                                </label>
                                <input type="text" class="form-control" id="nombre" name="nombre"
                                       placeholder="Nombre del rol" required="">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="name" class="text-right">
                                    Descripcion
                                    <span class="required-label">*</span>
                                </label>
                                <input type="text" class="form-control" id="descripcion" name="descripcion"
                                       placeholder="Descripcion" required="">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="alert alert-danger text-danger" id="error" style="display: none">
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="guardar" class="btn btn-primary">Guardar</button>
                <button type="button" id="actualizar" class="btn btn-primary">Actualizar</button>
                <button type="button" id="eliminar" class="btn btn-warning">Eliminar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
