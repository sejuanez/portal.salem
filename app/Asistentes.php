<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Asistentes extends Model
{
    //
    use Notifiable;

    protected $fillable = [
        'reunion_id',
        'persona_id',
        'old_id',
    ];

    public function reunion()
    {
        return $this->belongsTo(Reuniones::class, 'reunion_id');
    }

    public function persona()
    {
        return $this->belongsTo(Personas::class, 'persona_id');
    }

    public function persona_old()
    {
        return $this->belongsTo(Slm_people::class, 'old_id');
    }
}
