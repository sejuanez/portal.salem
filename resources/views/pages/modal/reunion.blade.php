<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="accion-modal">Informacion de </span>
                    <span class="fw-light">Reunion</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="nombre" class="text-right">
                                    Nombre
                                </label>
                                <input type="text" class="form-control" id="modal_nombre" disabled>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="tipo" class="text-right">
                                    Tipo
                                </label>
                                <input type="text" class="form-control" id="modal_tipo" disabled>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="fecha_realizado">Fecha de realización</label>
                                <input type="date" class="form-control" id="modal_fecha_realizado"
                                       name="fecha_realizado"
                                       required="" disabled>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="num_adultos" class="text-right">
                                    Numero de adultos
                                </label>
                                <input type="number" class="form-control" id="modal_num_adultos" name="num_adultos"
                                       autocomplete="nio"
                                       placeholder="Numero de adultos" required="" disabled>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="num_ninios" class="text-right">
                                    Numero de niños
                                </label>
                                <input type="number" class="form-control" id="modal_num_ninios" name="num_ninios"
                                       placeholder="Numero de niños" required="" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="comment">Nota:</label>
                                <textarea class="form-control" id="modal_descripcion" name="descripcion"
                                          rows="3" disabled></textarea>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <table id="modal-tabla-asistentes" class="display table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Cedula</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Cedula</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
