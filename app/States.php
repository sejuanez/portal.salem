<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    //
    protected $fillable = [
        'name', 'country_id'
    ];

    public function cities()
    {
        return $this->hasMany(Cities::class);
    }

    public function countries()
    {
        return $this->belongsTo(Countries::class, 'country_id');
    }
}
