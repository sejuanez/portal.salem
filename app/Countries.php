<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    //
    protected $fillable = [
        'id', 'sortname', 'name', 'phonecode'
    ];

    public function states()
    {
        return $this->hasMany(States::class);
    }
}
