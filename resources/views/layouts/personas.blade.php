<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>Registro - ProMonteria</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport'/>
    <link rel="icon" href="/assets/img/favicon.ico" type="image/x-icon"/>

    <!-- Scripts -->
{{--    <script src="{{ asset('js/app.js') }}" defer></script>--}}

<!-- Fonts and icons -->
    <script src="/assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
        WebFont.load({
            google: {"families": ["Lato:300,400,700,900"]},
            custom: {
                "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"],
                urls: ['../assets/css/fonts.min.css']
            },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!-- CSS Files -->
    {{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/atlantis.min.css">
    <link rel="stylesheet" href="/assets/css/custom.css">

    <style>
        .wizard-container .wizard-menu.nav-primary .moving-tab {
            background-color: #7b11bc !important;
        }

        .wizard-container .wizard-menu.nav-primary li a {
            color: #7b11bc !important;
        }

        .btn-secondary {
            background: #7b11bc !important;
            border-color: #7b11bc !important;
        }

        body {
            background-image: url("assets/img/bg-salem.jpg");
        }

        .wizard-container .wizard-action {
            padding: 30px;
            padding-top: 0px;
            background-color: transparent;
            line-height: 30px;
            font-size: 14px;
        }

        .wizard-container .wizard-body .tab-content {
            padding: 25px 15px;
            padding-bottom: 0px;
        }

    </style>
</head>
<body class="">
<div id="app">

    <div class="content" id="formWizzard">
        <div class="page-inner">
            <!--
            <div class="page-header">
                <h4 class="page-title">Formulario</h4>
            </div>
            -->
            <div class="row">
                <div class="wizard-container wizard-round col-md-9">
                    <div class="wizard-header text-center">

                        <div class="row">


                            <div class="col-sm-8">
                                <h3 class="wizard-title">
                                    <b style="color: #7b11bc;">Nuevo</b>
                                    Registro</h3>
                                <small>
                                    Diligencie este formulario únicamente si es Líder, Pastor o Coordinador de este
                                    Ministerio.
                                    Si tiene alguna dificultad escriba a salemnew@live.com su inquietud, escribir en el
                                    asunto SOPORTE REGISTRO.
                                </small>
                            </div>

                            <div class="col-sm-4">
                                <img src="/assets/img/logo-salem.jpg" alt="" style="width: 200px;">
                            </div>

                        </div>


                    </div>
                    <form novalidate="novalidate" autocomplete="off" id="form">
                        @csrf
                        <div class="wizard-body">
                            <div class="row">

                                <ul class="wizard-menu nav nav-pills nav-primary">
                                    <li class="step" style="width: 33.3333%;">
                                        <a class="nav-link active" href="#about" data-toggle="tab" aria-expanded="true"><i
                                                class="fa fa-user mr-0"></i> Datos Personales</a>
                                    </li>
                                    <li class="step" style="width: 33.3333%;">
                                        <a class="nav-link" href="#account" data-toggle="tab"><i
                                                class="fa fa-file mr-2"></i> Datos de la Cuenta </a>
                                    </li>

                                    {{--                                    --}}
                                    {{--                                    <li class="step" style="width: 33.3333%;">--}}
                                    {{--                                        <a class="nav-link" href="#address" data-toggle="tab"><i--}}
                                    {{--                                                class="fa fa-map-signs mr-2"></i> Datos de la Obra</a>--}}
                                    {{--                                    </li>--}}

                                    <li class="step" style="width: 33.3333%;">
                                        <a class="nav-link" href="#finalizar" data-toggle="tab"><i
                                                class="fa fa-check-double mr-2"></i> Finalizar</a>
                                    </li>

                                    <div class="moving-tab"
                                         style="width: 308.133px; transform: translate3d(0px, 0px, 0px); transition: all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1) 0s;">
                                        <i class="fa fa-user mr-0"></i> About
                                    </div>
                                </ul>

                            </div>

                            <div class="tab-content">


                                <div class="tab-pane active" id="about">

                                    <div class="row">
                                        <!--
                                        <div class="col-md-12">
                                            <h4 class="info-text">Tell us who you are.</h4>
                                        </div>
                                        -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Nombres:</label>
                                                <input name="nombres" autocomplete="nio" type="text"
                                                       class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Apellidos:</label>
                                                <input name="apellidos" autocomplete="nio" type="text"
                                                       class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Correo:</label>
                                                <input name="email" id="email" autocomplete="nio" type="email"
                                                       class="form-control" onkeypress="usuarioEmail()">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Cedula:</label>
                                                <input name="cedula" autocomplete="nio" type="number"
                                                       class="form-control">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Telefono: </label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <select name="indicativo" id="indicativo" class="form-control"
                                                                required>
                                                            <option selected value="+57"> +57 Colombia</option>
                                                            <option value="+58"> +58 Venezuela</option>
                                                            <option value="+507"> +507 Panama</option>
                                                            <option value="+593"> +593 Ecuador</option>
                                                            <option value="+54"> +54 Argentida</option>
                                                            <option value="+1"> +1 Estados Unidos</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input name="telefono" autocomplete="nio" type="number"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Fecha de Nacimiento</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="fechaNacimiento"
                                                           name="fechaNacimiento">
                                                    <div class="input-group-append">
														<span class="input-group-text">
															<i class="fa fa-calendar-check"></i>
														</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Año Nacimiento Hijos:</label>
                                                <input name="nacimientoHijos" id="nacimientoHijos"
                                                       data-toggle="tooltip" data-html="true"
                                                       title="
                                                       <br>
                                                <span>
                                                Si NO tiene Hijos escriba NINGUNO
                                                <br><br>
                                                Si tiene hijos escriba el AÑO de nacimiento (con 4 digitos) de cada uno de ellos
                                                separado por comas y sin espacios. No escriba comas ni puntos al final del campo.
                                                <br><br>
                                                <u>Ejemplo: 1997,2005</u>
                                                <br><br>
                                                </span>
                                                "
                                                       autocomplete="new-password"
                                                       type="text"
                                                       class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Estado Civil: </label>

                                                <select name="estadoCivil" id="estadoCivil" class="form-control"
                                                        required>
                                                    <option selected value="Soltero">Soltero</option>
                                                    <option value="Casado">Casado</option>
                                                    <option value="Viudo">Viudo</option>
                                                    <option value="Divorciado">Divorciado</option>
                                                </select>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Grado de escolaridad: </label>

                                                <select name="escolaridad" id="escolaridad" class="form-control"
                                                        required>
                                                    <option value="Primaria">Primaria</option>
                                                    <option selected value="Bachiller">Bachiller</option>
                                                    <option value="Tecnólogo">Tecnólogo</option>
                                                    <option value="Profesional">Profesional</option>
                                                    <option value="Postgrado">Postgrado</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Estudios/Profesión:</label>
                                                <input name="estudios" autocomplete="nio" type="text"
                                                       class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Trabajo Actual u Oficio:</label>
                                                <input name="trabajo" autocomplete="nio" type="text"
                                                       class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Idiomas:</label>
                                                <input name="idiomas" id="idiomas"
                                                       data-toggle="tooltip" data-html="true"
                                                       title="
                                                       <br>
                                                <span>
                                                Escriba los idiomas que conoce INCLUIDO EL NATIVO separado por comas (,) indicando el
                                                nivel entre parentesis (Bajo, Medio, Alto).
                                                <br><br>
                                                Ejemplo:
                                                <br><br>
                                                <u>Español (Alto), Ingles (Bajo)</u>
                                                <br><br>
                                                </span>
                                                "
                                                       autocomplete="new-password"
                                                       type="text"
                                                       class="form-control">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Formacion Musical:</label>
                                                <input name="formacionMusical" id="formacionMusical"
                                                       data-toggle="tooltip" data-html="true"
                                                       title="
                                                       <br>
                                                <span>
                                                Si NO toca instrumentos escriba NINGUNO
                                                <br><br>
                                                Escriba los instrumentos que toca separado por comas (,) indicando el
                                                nivel entre parentesis (Bajo, Medio, Alto).
                                                <br><br>
                                                Ejemplo:
                                                <br><br>
                                                <u>Guitarra (Medio), Canto (Alto)</u>
                                                <br><br>
                                                </span>
                                                "
                                                       autocomplete="new-password"
                                                       type="text"
                                                       class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Fecha de Ingreso a Iglesia</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="fechaIngreso"
                                                           name="fechaIngreso">
                                                    <div class="input-group-append">
														<span class="input-group-text">
															<i class="fa fa-calendar-check"></i>
														</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Recorrido en la Iglesia:</label>
                                                <input name="recorridoIglesia" id="recorridoIglesia"
                                                       data-toggle="tooltip" data-html="true"
                                                       title="
                                                       <br>
                                                <span>
                                                Escriba los instrumentos que toca separado por comas (,) indicando el
                                                nivel entre parentesis (Bajo, Medio, Alto).
                                                <br><br>
                                                Ejemplo:
                                                <br><br>
                                                <u>Líder (Grupo familiar Aquine),Pastor (Iglesia Bogotá Norte) </u>
                                                <br><br>
                                                </span>
                                                "
                                                       autocomplete="new-password"
                                                       type="text"
                                                       class="form-control">
                                            </div>
                                        </div>

                                    </div>


                                </div>
                                <div class="tab-pane" id="account">
                                    <!--
                                    <h4 class="info-text">Set up your account </h4>
                                    -->
                                    <div class="row">
                                        <div class="col-md-4 offset-4">
                                            <div class="form-group">
                                                <label>Nombre de usuario (ID)</label>
                                                <input name="nombre_usuario" disabled id="nombre_usuario"
                                                       data-toggle="tooltip" data-html="true"
                                                       title="
                                                       <br>
                                                <span>
                                                Elija un nombre de usuario,
                                                le permitirá ingresar en el
                                                sistema de informes mas
                                                adelante. No puede contener espacios
                                                en blanco, únicamente letras, números
                                                o los símbolos _.-
                                                <br><br>
                                                </span>
                                                "
                                                       autocomplete="new-password"
                                                       type="text"
                                                       class="form-control">
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 offset-4">
                                            <div class="form-group">
                                                <label>Contraseña</label>
                                                <input name="password" id="password"
                                                       data-toggle="tooltip" data-html="true"
                                                       title="
                                                       <br>
                                                <span>
                                                Elija una contraseña, debe contener
                                                al menos 8 caracteres.
                                                <br><br>
                                                </span>
                                                "
                                                       autocomplete="new-password"
                                                       type="text"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 offset-4">
                                            <div class="form-group">
                                                <label>Servicio: </label>

                                                <select name="tipoUsuario" id="tipoUsuario" class="form-control"
                                                        required>
                                                    <option value="Lider">Líder de G. Familiar</option>
                                                    <option value="Pastor">Pastor</option>
                                                    <option value="Coordinador">Coordinador</option>
                                                </select>

                                            </div>
                                        </div>
                                    </div>


                                </div>
                                {{--                                --}}
                                {{--                                <div class="tab-pane" id="address">--}}

                                {{--                                    <h5 class="">--}}
                                {{--                                        <strong>OBRA QUE DIRIGE ACTUALMENTE</strong>--}}
                                {{--                                        <br>--}}
                                {{--                                        (Pastores y Líderes únicamente)--}}
                                {{--                                        <br>--}}
                                {{--                                        Diligencie aquí únicamente la obra principal o mas grande que esta a su cargo.--}}
                                {{--                                        Si esta dirigiendo mas obras debe enviar un correo electrónico a--}}
                                {{--                                        salemnew@live.com con los datos de obra tal y como se solicitan en este espacio.--}}
                                {{--                                        Colocar en el asunto del correo: "Tengo mas obras a cargo"--}}
                                {{--                                    </h5>--}}

                                {{--                                    <div class="row">--}}

                                {{--                                        <div class="col-md-3">--}}
                                {{--                                            <div class="form-group">--}}
                                {{--                                                <label>Coordinador o Supervisor:</label>--}}
                                {{--                                                <input name="coordinadorObra" id="coordinadorObra" autocomplete="nio"--}}
                                {{--                                                       type="text"--}}
                                {{--                                                       class="form-control">--}}
                                {{--                                            </div>--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-4">--}}
                                {{--                                            <div class="form-group">--}}
                                {{--                                                <label>Ciudad/Población y departamento:</label>--}}
                                {{--                                                <input name="ciudadObra" id="ciudadObra" autocomplete="nio" type="text"--}}
                                {{--                                                       class="form-control">--}}
                                {{--                                            </div>--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-2">--}}
                                {{--                                            <div class="form-group">--}}
                                {{--                                                <label>Dirección:</label>--}}
                                {{--                                                <input name="direccionObra" id="direccionObra" autocomplete="nio"--}}
                                {{--                                                       type="text"--}}
                                {{--                                                       class="form-control">--}}
                                {{--                                            </div>--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-3">--}}
                                {{--                                            <div class="form-group">--}}
                                {{--                                                <label>Barrio:</label>--}}
                                {{--                                                <input name="barrioObra" id="barrioObra" autocomplete="nio" type="text"--}}
                                {{--                                                       class="form-control">--}}
                                {{--                                            </div>--}}
                                {{--                                        </div>--}}

                                {{--                                    </div>--}}

                                {{--                                    <div class="row">--}}

                                {{--                                        <div class="col-md-5">--}}
                                {{--                                            <div class="form-group">--}}
                                {{--                                                <label>Nombre de la Obra:</label>--}}
                                {{--                                                <input name="nombreObra" id="nombreObra" autocomplete="nio" type="text"--}}
                                {{--                                                       class="form-control">--}}
                                {{--                                            </div>--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-3">--}}
                                {{--                                            <div class="form-group">--}}
                                {{--                                                <label>Cuando Recibio la Obra</label>--}}
                                {{--                                                <div class="input-group">--}}
                                {{--                                                    <input type="text" class="form-control" id="fechaObra"--}}
                                {{--                                                           name="fechaObra" id="fechaObra">--}}
                                {{--                                                    <div class="input-group-append">--}}
                                {{--														<span class="input-group-text">--}}
                                {{--															<i class="fa fa-calendar-check"></i>--}}
                                {{--														</span>--}}
                                {{--                                                    </div>--}}
                                {{--                                                </div>--}}
                                {{--                                            </div>--}}
                                {{--                                        </div>--}}

                                {{--                                        <div class="col-md-4">--}}
                                {{--                                            <div class="form-group">--}}
                                {{--                                                <label>Lider Anterior:</label>--}}
                                {{--                                                <input name="liderAnterior" id="liderAnterior" autocomplete="nio"--}}
                                {{--                                                       type="text"--}}
                                {{--                                                       class="form-control">--}}
                                {{--                                            </div>--}}
                                {{--                                        </div>--}}

                                {{--                                    </div>--}}

                                {{--                                </div>--}}
                                {{--                                --}}
                                <div class="tab-pane" id="finalizar">
                                    <!--
                                    <h4 class="info-text">Tell us where you live.</h4>
                                    -->
                                    <div class="wizard-header text-center">
                                        <h3 class="wizard-title"> Formulario Completado</h3>
                                        <small>Por favor pulsa en el boton <b>Finalizar</b> para guardar la
                                            información
                                        </small>
                                        <br/>
                                        <em>Sus datos estan protegidos por la ley 1581 de 2012</em>
                                    </div>


                                </div>

                            </div>

                        </div>

                        <div class="wizard-action">
                            <div class="pull-left">
                                <input type="button" class="btn btn-previous btn-fill btn-default disabled"
                                       name="previous" value="Anterior">
                            </div>
                            <div class="pull-right">
                                <input type="button" class="btn btn-next btn-secondary" name="next" value="Siguiente"
                                >
                                <input type="button" class="btn btn-finish btn-secondary" name="finish"
                                       value="Finalizar"
                                       onclick="guardar()"
                                       style="display: none;">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="container " id="card" style="display: none; height: 100vh;padding-top: 35vh;">

        <div class="card text-center">
            <div class="card-header">
                <h2>
                    Ok!
                </h2>
            </div>
            <div class="card-body">
                <h5 class="card-title">
                    <spam id="titleEmpresa"></spam>
                </h5>
                <p class="card-text">
                    <spam id="labelMsg"></spam>
                </p>
            </div>
            <div class="card-footer text-muted">
                <a href="/" class="btn btn-primary">ir a portal Salem </a>
            </div>
        </div>

    </div>

</div>
<script src="/assets/js/core/jquery.3.2.1.min.js"></script>
<script src="/assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
<script src="/assets/js/core/popper.min.js"></script>
<script src="/assets/js/core/bootstrap.min.js"></script>

<script src="/assets/js/plugin/form/bootstrapwizard.js"></script>
<script src="/assets/js/plugin/select2/select2.full.min.js"></script>
<script src="/assets/js/plugin/form/jquery.validate.js"></script>
<script src="/assets/js/plugin/form/sweetalert.min.js"></script>

<script>
    // Code for the Validator
    var $validator = $('.wizard-container form').validate({

        validClass: "success",
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        }
    });

    function disableNombre() {
        //input disable
        $("#nombreObra").prop('disabled', true)
    }

    function enableTodo() {
        $("#coordinadorObra").prop('disabled', false);
        $("#ciudadObra").prop('disabled', false);
        $("#direccionObra").prop('disabled', false);
        $("#barrioObra").prop('disabled', false);
        $("#nombreObra").prop('disabled', false);
        $("#fechaObra").prop('disabled', false);
        $("#liderAnterior").prop('disabled', false);
    }

    function disableTodo() {
        $("#coordinadorObra").prop('disabled', true);
        $("#ciudadObra").prop('disabled', true);
        $("#direccionObra").prop('disabled', true);
        $("#barrioObra").prop('disabled', true);
        $("#nombreObra").prop('disabled', true);
        $("#fechaObra").prop('disabled', true);
        $("#liderAnterior").prop('disabled', true);
    }

    function cargarDepartamentos() {

        let STATES = [];

        $.ajax({
            url: "/getStates",
            async: false
        }).done(function (data) {
            console.log(data);
            for (var item in data.msg) {
                var itemSelect2 = {
                    id: data.msg[item].id,
                    text: data.msg[item].name
                };
                STATES.push(itemSelect2)
            }
            $('#departamento').select2({
                theme: "bootstrap",
                placeholder: "Selecciona un departamento...",
                allowClear: true,
                data: STATES
            });

        });

        $('#departamento').val('788'); // Select the option with a value of '1'
        $('#departamento').trigger('change'); // Notify any JS components that the value changed
        cargarMunicipios(788)

    }

    function cargarMunicipios(codDepartamento) {


        var cities = [];
        $("#city_id").html('').select2();
        $.get(
            "/getCities", {'state': codDepartamento}
        ).done(function (data) {
            console.log(data);
            for (var item in data.msg) {
                var itemSelect2 = {
                    id: data.msg[item].id,
                    text: data.msg[item].name
                };
                cities.push(itemSelect2)
            }
            $('#city_id').select2({
                allowClear: true,
                theme: "bootstrap",
                placeholder: "Selecciona una ciudad...",
                data: cities
            });


        });

    }

    function guardar() {

        let formData = new FormData(document.getElementById("form"));

        console.log('printing data...');
        let csrfToken = '{{ csrf_token() }}';


        $.ajax({
            url: '/guardar',
            type: 'POST',
            headers: {
                'X-CSRF-Token': csrfToken
            },
            data: formData,
            processData: false,
            contentType: false,
            cache: false
        }).done(function (response) {

            console.log(response);

            if (response.status !== 'Error') {

                $('#card').css("display", "");
                $('#formWizzard').css("display", "none");

                $('#titleEmpresa').text(response.persona.nombres);
                $('#labelMsg').text('Usuario registrado satisfactoriamente')

            }

        }).fail(function (error) {
            console.log(error);

            let obj = error.responseJSON.errors;
            $.each(obj, function (key, value) {

                swal("Error!", value[0], {
                    icon: "error",
                    buttons: {
                        confirm: {
                            className: 'btn btn-danger'
                        }
                    },
                });

            });

        });


    }

    $(document).ready(function () {


        $('#fechaNacimiento').datetimepicker({
            format: 'YYYY/MM/DD',
        });

        $('#fechaIngreso').datetimepicker({
            format: 'YYYY/MM/DD',
        });

        $('#fechaObra').datetimepicker({
            format: 'YYYY/MM/DD',
        });

        $('#departamento').on('change', function (e) {
            cargarMunicipios($("#departamento").val());
        });

        $(".select2-container").removeAttr("style");


        /*
        $('#pais').select2({
            theme: "bootstrap",
            placeholder: "Selecciona un pais...",
            allowClear: true
        });
       */

        // Ocultar Carta
        $("#card").css("display", "none");

        //EVENTO TIPO DE USUARIO
        $("#nombreObra").prop('disabled', true);
        $("#tipoUsuario").change(function () {
            let tipo = $("#tipoUsuario").val();

            if (tipo == "Lider") {
                enableTodo();
                disableNombre();
            } else if (tipo == "Pastor") {
                enableTodo()
            } else {
                disableTodo()
            }

        });

    });

    function usuarioEmail() {
        $("#nombre_usuario").val($("#email").val())
    }
</script>
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7o0dvz8VLcNGtEld2hnV58UNugeFaIcs"></script>
<script src="/assets/js/atlantis.js"></script>

<!-- Moment -->
<script src="/assets/js/plugin/moment/moment.min.js"></script>
<!-- Bootstrap datetimepicker -->
<script src="/assets/js/plugin/datepicker/bootstrap-datetimepicker.min.js"></script>
</body>
</html>
