<?php

use Illuminate\Database\Seeder;

class IglesiasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Cl. 9a. no. 31a-50',
            'direccion' => 'Cl. 9a. no. 31a-50',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //2
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Carrera 19 # 32 N 0-2 Condominio Castelamar MZ. C - C 6',
            'direccion' => 'Carrera 19 # 32 N 0-2 Condominio Castelamar MZ. C - C 6',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //3
        $iglesia = \App\Iglesias::create([
            'nombre' => 'av. Colombia no 17-06',
            'direccion' => 'av. Colombia no 17-06',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //4
        $iglesia = \App\Iglesias::create([
            'nombre' => 'AVDA COLOMBIA N° 17-03',
            'direccion' => 'AVDA COLOMBIA N° 17-03',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //5
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Avenida 30 de Agosto Carrere 13 # 63-95 la Glorieta Cuba',
            'direccion' => 'Avenida 30 de Agosto Carrere 13 # 63-95 la Glorieta Cuba',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //6
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Avenida de los Estudiantes, casa #00-77 la Y',
            'direccion' => 'Avenida de los Estudiantes, casa #00-77 la Y',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //7
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Avenida La Playa María Aux. casa 493',
            'direccion' => 'Avenida La Playa María Aux. casa 493',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //8
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Avenida Los Estudiantes Casa 328',
            'direccion' => 'Avenida Los Estudiantes Casa 328',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //9
        $iglesia = \App\Iglesias::create([
            'nombre' => 'B/ Niza 3, Bloque 2, apto 101',
            'direccion' => 'B/ Niza 3, Bloque 2, apto 101',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //10
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Barrio Centro Casa Flor Tapia',
            'direccion' => 'Barrio Centro Casa Flor Tapia',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //11
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Calle 113 # 22-18 APTO 201 provenza',
            'direccion' => 'Calle 113 # 22-18 APTO 201 provenza',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //12
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Calle 117 # 25 - 78 Ciudadela del rio',
            'direccion' => 'Calle 117 # 25 - 78 Ciudadela del rio',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //13
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Calle 19 # 17-24 barrio Navarrette',
            'direccion' => 'Calle 19 # 17-24 barrio Navarrette',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //14
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Calle 2 Numero 3-09 La laguna',
            'direccion' => 'Calle 2 Numero 3-09 La laguna',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //15
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Calle 27 No. 14-27 B/. La Avenida',
            'direccion' => 'Calle 27 No. 14-27 B/. La Avenida',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //16
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Calle 29 17-47 barrio majagual',
            'direccion' => 'Calle 29 17-47 barrio majagual',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //17
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Calle 35 # 5-13',
            'direccion' => 'Calle 35 # 5-13',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //18
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Calle 4D 35-02 San Fernando',
            'direccion' => 'Calle 4D 35-02 San Fernando',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //19
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Calle 59 N 22c47',
            'direccion' => 'Calle 59 N 22c47',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //20
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Calle 8A 38-30 El templete',
            'direccion' => 'Calle 8A 38-30 El templete',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //21
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Calle 9 12-34 Corosito',
            'direccion' => 'Calle 9 12-34 Corosito',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //22
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Calle Pedro Arizala casa 237',
            'direccion' => 'Calle Pedro Arizala casa 237',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //23
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Carrera 1 # 12 - 78 Av. Panamericana',
            'direccion' => 'Carrera 1 # 12 - 78 Av. Panamericana',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //24
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Carrera 12 # 11-03',
            'direccion' => 'Carrera 12 # 11-03',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //25
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Carrera 18 10B-73 B/athagualpa',
            'direccion' => 'Carrera 18 10B-73 B/athagualpa',
            'barrio' => '',
            'city_id' => '1',
            'tipo' => 'Iglesia',
        ]);
        //26
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Carrera 21 # 8-63 B/ Villa Lucia',
            'direccion' => 'Carrera 21 # 8-63 B/ Villa Lucia',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //27
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Carrera 24 No. 21-23 Barrio La Esperanza',
            'direccion' => 'Carrera 24 No. 21-23 Barrio La Esperanza',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //28
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Carrera 28 # 20a - 41',
            'direccion' => 'Carrera 28 # 20a - 41',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //29
        $iglesia = \App\Iglesias::create([
            'nombre' => 'carrera 28D #92-04 Mojica',
            'direccion' => 'carrera 28D #92-04 Mojica',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //30
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Carrera 2E # 21-55 B/Santa Bárbara',
            'direccion' => 'Carrera 2E # 21-55 B/Santa Bárbara',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //31
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Carrera 3e #17-58',
            'direccion' => 'Carrera 3e #17-58',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //32
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Carrera 3e #17-58',
            'direccion' => 'Carrera 3e #17-58',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //33
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Carrera 4 #15C23 Barrió Jardín',
            'direccion' => 'Carrera 4 #15C23 Barrió Jardín',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //34
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Carrera 7 # 15-23 Santa Clara',
            'direccion' => 'Carrera 7 # 15-23 Santa Clara',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //35
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Carrera 8 # 23-52 piso 2 Comeva',
            'direccion' => 'Carrera 8 # 23-52 piso 2 Comeva',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //36
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Carrera 88 #35d-37 la toscana apartamento 201 barrio cristobal la america',
            'direccion' => 'Carrera 88 #35d-37 la toscana apartamento 201 barrio cristobal la america',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //37
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Ciudadela El Jardin Bloque 42, casa 1',
            'direccion' => 'Ciudadela El Jardin Bloque 42, casa 1',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //38
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Calle 22 #17-03',
            'direccion' => 'Calle 22 #17-03',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //39
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Calle 48D # 20-33',
            'direccion' => 'Calle 48D # 20-33',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //40
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Calle 51B #8B-04',
            'direccion' => 'Calle 51B #8B-04',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //41
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Club el Nogal torre 9 apartamento 933',
            'direccion' => 'Club el Nogal torre 9 apartamento 933',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //42
        $iglesia = \App\Iglesias::create([
            'nombre' => 'CONDOMINIO MONTERREY APTO 304 TORRE 7',
            'direccion' => 'CONDOMINIO MONTERREY APTO 304 TORRE 7',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //43
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Cra 22 F Número 9-40 (Shalom)',
            'direccion' => 'Cra 22 F Número 9-40 (Shalom)',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //44
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Cra 25 23-35 B/ Tomas Uribe',
            'direccion' => 'Cra 25 23-35 B/ Tomas Uribe',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //45
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Cra 25 23-35 B/ Tomas Uribe',
            'direccion' => 'Cra 25 23-35 B/ Tomas Uribe',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //46
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Cra 25A #53-61 piso 2',
            'direccion' => 'Cra 25A #53-61 piso 2',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //47
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Cra 7 A N° 19A-27 BARRIO CHILE',
            'direccion' => 'Cra 7 A N° 19A-27 BARRIO CHILE',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //48
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Cra 7 A N° 19A-27 BARRIO CHILE',
            'direccion' => 'Cra 7 A N° 19A-27 BARRIO CHILE',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //49
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Cra 7A #1A-10',
            'direccion' => 'Cra 7A #1A-10',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //50
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Cra 94 2oeste 1a 24 Alto Jordan Melendez',
            'direccion' => 'Cra 94 2oeste 1a 24 Alto Jordan Melendez',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //51
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Cra. 1B # 16b - 136 Barrio Doce de Octubre',
            'direccion' => 'Cra. 1B # 16b - 136 Barrio Doce de Octubre',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //52
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Jamomdino bajo, número 111 A 1',
            'direccion' => 'Jamomdino bajo, número 111 A 1',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //53
        $iglesia = \App\Iglesias::create([
            'nombre' => 'M L Casa 5 Ciudad Real Alto',
            'direccion' => 'M L Casa 5 Ciudad Real Alto',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //54
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Ma 19 casa 7 villa flor',
            'direccion' => 'Ma 19 casa 7 villa flor',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //55
        $iglesia = \App\Iglesias::create([
            'nombre' => 'MANAZANA N CASA 11 SIMON BOLIVAR',
            'direccion' => 'MANAZANA N CASA 11 SIMON BOLIVAR',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //56
        $iglesia = \App\Iglesias::create([
            'nombre' => 'MANZANA A CASA 14B SANTA MONICA',
            'direccion' => 'MANZANA A CASA 14B SANTA MONICA',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //57
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Manzana B casa 16 barrio Terrazas del norte',
            'direccion' => 'Manzana B casa 16 barrio Terrazas del norte',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //58
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Manzana D casa 4 Condominio Villa Helena',
            'direccion' => 'Manzana D casa 4 Condominio Villa Helena',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //59
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Manzana F Casa 7 B/ Rincón de Pasto (Aranda)',
            'direccion' => 'Manzana F Casa 7 B/ Rincón de Pasto (Aranda)',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //60
        $iglesia = \App\Iglesias::create([
            'nombre' => 'MANZANA H CASA 5. BARRIO LAS PALMAS',
            'direccion' => 'MANZANA H CASA 5. BARRIO LAS PALMAS',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //61
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Manzana L Casa 20 B / San Carlos',
            'direccion' => 'Manzana L Casa 20 B / San Carlos',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //62
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Manzana P casa 1 B/gualcaloma',
            'direccion' => 'Manzana P casa 1 B/gualcaloma',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //63
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Manzana 3-C9',
            'direccion' => 'Manzana 3-C9',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //64
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Manzana 25 Casa 3 Quintas de San Pedro',
            'direccion' => 'Manzana 25 Casa 3 Quintas de San Pedro',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //65
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Nissa 3 bloque 2 apt 101',
            'direccion' => 'Nissa 3 bloque 2 apt 101',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //66
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Obrero casa 590',
            'direccion' => 'Obrero casa 590',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //67
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Pantano de Vargas, casa 02-56',
            'direccion' => 'Pantano de Vargas, casa 02-56',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //68
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Salem',
            'direccion' => 'Salem',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //69
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Salem principal Teatro Alcazar',
            'direccion' => 'Salem principal Teatro Alcazar',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //70
        $iglesia = \App\Iglesias::create([
            'nombre' => 'San Diego calle 56 Bis # 16-25',
            'direccion' => 'San Diego calle 56 Bis # 16-25',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //71
        $iglesia = \App\Iglesias::create([
            'nombre' => 'San Felipe Universidad casa 81A',
            'direccion' => 'San Felipe Universidad casa 81A',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //72
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Sector La Josefina',
            'direccion' => 'Sector La Josefina',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //73
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Vereda Tangareal - casa Yelena Zambrano',
            'direccion' => 'Vereda Tangareal - casa Yelena Zambrano',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //74
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Villa de los Lagos Mz. 2 casa 3',
            'direccion' => 'Villa de los Lagos Mz. 2 casa 3',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //75
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Villa Santana Mz. 22 Casa 9 sector la Isla',
            'direccion' => 'Villa Santana Mz. 22 Casa 9 sector la Isla',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);
        //76
        $iglesia = \App\Iglesias::create([
            'nombre' => 'Iglesia príncipe de paz',
            'direccion' => 'Parque bolivar, funeraria San José',
            'barrio' => '',
            'city_id' => '12954',
            'tipo' => 'Iglesia',
        ]);


    }
}
