<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>Registro - ProMonteria</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport'/>
    <link rel="icon" href="/assets/img/favicon.ico" type="image/x-icon"/>

    <!-- Scripts -->
{{--    <script src="{{ asset('js/app.js') }}" defer></script>--}}

<!-- Fonts and icons -->
    <script src="/assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
        WebFont.load({
            google: {"families": ["Lato:300,400,700,900"]},
            custom: {
                "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"],
                urls: ['../assets/css/fonts.min.css']
            },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!-- CSS Files -->
    {{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/atlantis.min.css">
    <link rel="stylesheet" href="/assets/css/custom.css">

    <style>
        .wizard-container .wizard-menu.nav-primary .moving-tab {
            background-color: #7b11bc !important;
        }

        .wizard-container .wizard-menu.nav-primary li a {
            color: #7b11bc !important;
        }

        .btn-secondary {
            background: #7b11bc !important;
            border-color: #7b11bc !important;
        }

        body {
            background-image: url("assets/img/pm.png");
        }
    </style>
</head>
<body class="">
<div id="app">

    <div class="content" id="formWizzard">
        <div class="page-inner">
            <!--
            <div class="page-header">
                <h4 class="page-title">Formulario</h4>
            </div>
            -->
            <div class="row">
                <div class="wizard-container wizard-round col-md-9">
                    <div class="wizard-header text-center">

                        <div class="row">


                            <div class="col-sm-8">
                                <h3 class="wizard-title">
                                    <b style="color: #7b11bc;">Registrar</b>
                                    Nueva Empresa</h3>
                                <small>Esta información nos permitirá conocer más acerca de nuestros
                                    asociados.
                                </small>
                            </div>

                            <div class="col-sm-4">
                                <img src="/assets/img/logo-pro.png" alt="" style="width: 88%;">
                            </div>

                        </div>


                    </div>
                    <form novalidate="novalidate" autocomplete="off" id="form">
                        @csrf
                        <div class="wizard-body">
                            <div class="row">

                                <ul class="wizard-menu nav nav-pills nav-primary">
                                    <li class="step" style="width: 33.3333%;">
                                        <a class="nav-link active" href="#about" data-toggle="tab" aria-expanded="true"><i
                                                    class="fa fa-user mr-0"></i> Empresa</a>
                                    </li>
                                    <li class="step" style="width: 33.3333%;">
                                        <a class="nav-link" href="#account" data-toggle="tab"><i
                                                    class="fa fa-file mr-2"></i> Actividades</a>
                                    </li>

                                    <li class="step" style="width: 33.3333%;">
                                        <a class="nav-link" href="#address" data-toggle="tab"><i
                                                    class="fa fa-map-signs mr-2"></i> Web</a>
                                    </li>

                                    <li class="step" style="width: 33.3333%;">
                                        <a class="nav-link" href="#finalizar" data-toggle="tab"><i
                                                    class="fa fa-check-double mr-2"></i> Finalizar</a>
                                    </li>

                                    <div class="moving-tab"
                                         style="width: 308.133px; transform: translate3d(0px, 0px, 0px); transition: all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1) 0s;">
                                        <i class="fa fa-user mr-0"></i> About
                                    </div>
                                </ul>

                            </div>

                            <div class="tab-content">


                                <div class="tab-pane active" id="about">

                                    <div class="row">
                                        <!--
                                        <div class="col-md-12">
                                            <h4 class="info-text">Tell us who you are.</h4>
                                        </div>
                                        -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Razon Social:</label>
                                                <input name="razonSocial" autocomplete="nio" type="text"
                                                       class="form-control" required="">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Nit :
                                                    <span style="font-weight: lighter">(Sin digito de verificación)</span>
                                                </label>
                                                <input name="nit" autocomplete="nope" type="text"
                                                       maxlength="9"
                                                       class="form-control"
                                                       required="">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Telefono :</label>
                                                <input name="telefono" autocomplete="nope" type="text"
                                                       class="form-control" required="">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Pais: </label>
                                                <div class="select2-input">
                                                    <select name="pais" id="pais" class="form-control" required>
                                                        <option></option>
                                                        <option selected value="CO">Colombia</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Departamento:</label>
                                                <div class="select2-input">
                                                    <select name="departamento" id="departamento" class="form-control"
                                                            required>
                                                        <option></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Ciudad:</label>
                                                <div class="select2-input">
                                                    <select name="city_id" id="city_id" class="form-control" required>
                                                        <option></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Direccion:</label>
                                                <input name="direccionEmpresa" id="direccionEmpresa"
                                                       autocomplete="new-password"
                                                       type="text"
                                                       class="form-control" required="">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Representante Legal:</label>
                                                <input name="representante" autocomplete="new-password" type="text"
                                                       class="form-control" required="">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Año de creación:</label>
                                                <input name="anio" autocomplete="new-password" type="number"
                                                       class="form-control" required="">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Numero de empleados:</label>
                                                <input name="empleados" autocomplete="new-password" type="number"
                                                       class="form-control" required="">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Sector de la empresa:</label>
                                                <div class="select2-input ">
                                                    <select name="sector" id="sector" class="form-control width-100"
                                                            required>
                                                        <option></option>
                                                        <option value="Servicios">Servicios</option>
                                                        <option value="Comercio">Comercio</option>
                                                        <option value="Turismo">Turismo</option>
                                                        <option value="Agroindustria">Agroindustria</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>


                                    </div>


                                </div>
                                <div class="tab-pane" id="account">
                                    <!--
                                    <h4 class="info-text">Set up your account </h4>
                                    -->

                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Actividad Economica Principal:</label>
                                                <div class="select2-input">
                                                    <select name="actividadPrincipal" id="actividadPrincipal"
                                                            class="form-control"
                                                            required
                                                            style="width: 100%">
                                                        <option></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Actividad Economica Secundaria:</label>
                                                <div class="select2-input">
                                                    <select name="actividadSecundaria" id="actividadSecundaria"
                                                            class="form-control"
                                                            required
                                                            style="width: 100%">
                                                        <option></option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Promedio de ventas:
                                                    <span style="font-weight: lighter">(en USD)</span>
                                                </label>
                                                <div class="select2-input">
                                                    <select name="promedioVentas" id="promedioVentas"
                                                            class="form-control"
                                                            required
                                                            style="width: 100%">
                                                        <option></option>
                                                        <option value="Menos de $500">Menos de $500</option>
                                                        <option value="Entre $1000 y $3000">Entre $1000 y $3000</option>
                                                        <option value="Entre $3000 y $6000">Entre $3000 y $6000</option>
                                                        <option value="Más de $6000">Más de $6000</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Total Activos:</label>
                                                <div class="select2-input">
                                                    <select name="totalActivos" id="totalActivos" class="form-control"
                                                            required
                                                            style="width: 100%">
                                                        <option></option>
                                                        <option value="Menos de $500">Menos de $500</option>
                                                        <option value="Entre $1000 y $3000">Entre $1000 y $3000</option>
                                                        <option value="Entre $3000 y $6000">Entre $3000 y $6000</option>
                                                        <option value="Más de $6000">Más de $6000</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Realiza Operaciones Internacionales?</label>
                                                <div class="select2-input">
                                                    <select name="operacionesInternacionales"
                                                            id="operacionesInternacionales"
                                                            class="form-control"
                                                            required
                                                            style="width: 100%">
                                                        <option></option>
                                                        <option value="S">Si</option>
                                                        <option value="N">No</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Tipo Operaciones:</label>
                                                <div class="select2-input">
                                                    <select name="tipoOperaciones" class="form-control"
                                                            id="tipoOperaciones"
                                                            required
                                                            style="width: 100%">
                                                        <option></option>
                                                        <option value="N">Nacionales</option>
                                                        <option value="I">Internacionales</option>
                                                        <option value="A">Ambas</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Sucursales en otras Ciudades?</label>
                                                <div class="select2-input">
                                                    <select name="tieneSucursales" class="form-control"
                                                            id="tieneSucursales"
                                                            required
                                                            style="width: 100%">
                                                        <option></option>
                                                        <option value="S">Si</option>
                                                        <option value="N">No</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Ciudades:</label>
                                                <div class="select2-input">
                                                    <input name="sucursales" id="sucursales" autocomplete="nio"
                                                           type="text"
                                                           class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Comentarios adicionales: </label>

                                                <textarea name="comentarios" rows="1"
                                                          class="form-control"
                                                ></textarea>
                                            </div>
                                        </div>


                                    </div>


                                </div>
                                <div class="tab-pane" id="address">
                                    <!--
                                    <h4 class="info-text">Tell us where you live.</h4>
                                    -->
                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Pagina Web:</label>
                                                <input name="web" autocomplete="nio" type="text"
                                                       class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Link Portafolio:</label>
                                                <input name="portafolio" autocomplete="nio" type="text"
                                                       class="form-control">
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Logo :</label>
                                                <div class="input-file input-file-image">
                                                    <img class="img-upload-preview img-circle" width="100"
                                                         height="100"
                                                         src="https://placehold.it/100x100" alt="preview">
                                                    <input type="file" class="form-control form-control-file"
                                                           id="uploadImg" name="uploadImg" accept="image/*"
                                                    >
                                                    <label for="uploadImg"
                                                           class=" label-input-file btn btn-primary">Subir
                                                        Imagen</label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                                <div class="tab-pane" id="finalizar">
                                    <!--
                                    <h4 class="info-text">Tell us where you live.</h4>
                                    -->
                                    <div class="wizard-header text-center">
                                        <h3 class="wizard-title"> Formulario Completado</h3>
                                        <small>Por favor pulsa en el boton <b>Finalizar</b> para guardar la
                                            información
                                        </small>
                                        <br/>
                                        <em>Sus datos estan protegidos por la ley 1581 de 2012</em>
                                    </div>


                                </div>

                            </div>

                        </div>

                        <div class="wizard-action">
                            <div class="pull-left">
                                <input type="button" class="btn btn-previous btn-fill btn-default disabled"
                                       name="previous" value="Anterior">
                            </div>
                            <div class="pull-right">
                                <input type="button" class="btn btn-next btn-secondary" name="next" value="Siguiente"
                                >
                                <input type="button" class="btn btn-finish btn-secondary" name="finish"
                                       value="Finalizar"
                                       onclick="guardar()"
                                       style="display: none;">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="container " id="card" style="display: none; height: 100vh;padding-top: 35vh;">

        <div class="card text-center">
            <div class="card-header">
                <h2>
                    Ok!
                </h2>
            </div>
            <div class="card-body">
                <h5 class="card-title">
                    <spam id="titleEmpresa"></spam>
                </h5>
                <p class="card-text">
                    <spam id="labelMsg"></spam>
                </p>
            </div>
            <div class="card-footer text-muted">
                <a href="https://promonteria.co/" class="btn btn-primary">ir a ProMonteria</a>
            </div>
        </div>

    </div>

</div>
<script src="/assets/js/core/jquery.3.2.1.min.js"></script>
<script src="/assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
<script src="/assets/js/core/popper.min.js"></script>
<script src="/assets/js/core/bootstrap.min.js"></script>

<script src="/assets/js/plugin/form/bootstrapwizard.js"></script>
<script src="/assets/js/plugin/select2/select2.full.min.js"></script>
<script src="/assets/js/plugin/form/jquery.validate.js"></script>
<script src="/assets/js/plugin/form/sweetalert.min.js"></script>

<script>
    // Code for the Validator
    var $validator = $('.wizard-container form').validate({

        validClass: "success",
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        }
    });

    // Ocultar Carta
    $("#card").css("display", "none");


    function cargarDepartamentos() {

        let STATES = [];

        $.ajax({
            url: "/getStates",
            async: false
        }).done(function (data) {
            console.log(data);
            for (var item in data.msg) {
                var itemSelect2 = {
                    id: data.msg[item].id,
                    text: data.msg[item].name
                };
                STATES.push(itemSelect2)
            }
            $('#departamento').select2({
                theme: "bootstrap",
                placeholder: "Selecciona un departamento...",
                allowClear: true,
                data: STATES
            });

        });

        $('#departamento').val('788'); // Select the option with a value of '1'
        $('#departamento').trigger('change'); // Notify any JS components that the value changed
        cargarMunicipios(788)

    }

    function cargarMunicipios(codDepartamento) {


        var cities = [];
        $("#city_id").html('').select2();
        $.get(
            "/getCities", {'state': codDepartamento}
        ).done(function (data) {
            console.log(data);
            for (var item in data.msg) {
                var itemSelect2 = {
                    id: data.msg[item].id,
                    text: data.msg[item].name
                };
                cities.push(itemSelect2)
            }
            $('#city_id').select2({
                allowClear: true,
                theme: "bootstrap",
                placeholder: "Selecciona una ciudad...",
                data: cities
            });


        });

    }

    function cargarActividades() {

        let actividades = [];

        $.get(
            "/getActividades",
        ).done(function (data) {
            console.log(data);

            for (let item in data.msg) {
                let itemSelect2 = {
                    id: data.msg[item].codigo,
                    text: data.msg[item].descripcion
                };
                actividades.push(itemSelect2)
            }
            $('#actividadPrincipal').select2({
                theme: "bootstrap",
                placeholder: "Selecciona una Actividad...",
                allowClear: true,
                data: actividades
            });

            $('#actividadSecundaria').select2({
                theme: "bootstrap",
                placeholder: "Selecciona una Actividad...",
                allowClear: true,
                data: actividades
            });

        });

    }

    function guardar() {

        let formData = new FormData(document.getElementById("form"));

        console.log('printing data...');
        let csrfToken = '{{ csrf_token() }}';


        $.ajax({
            url: '/guardar',
            type: 'POST',
            headers: {
                'X-CSRF-Token': csrfToken
            },
            data: formData,
            processData: false,
            contentType: false,
            cache: false
        }).done(function (response) {

            console.log(response);

            if (response.status !== 'Error') {

                $('#card').css("display", "");
                $('#formWizzard').css("display", "none");

                $('#titleEmpresa').text(response.msg.razonSocial);
                $('#labelMsg').text('Empresa registrada satisfactoriamente')

            }

        }).fail(function (error) {
            console.log(error);

            let obj = error.responseJSON.errors;
            $.each(obj, function (key, value) {

                swal("Error!", value[0], {
                    icon: "error",
                    buttons: {
                        confirm: {
                            className: 'btn btn-danger'
                        }
                    },
                });

            });

        });


    }

    $(document).ready(function () {

        cargarDepartamentos();
        cargarActividades();

        $('#departamento').on('change', function (e) {
            cargarMunicipios($("#departamento").val());
        });

        $(".select2-container").removeAttr("style");

        $('#pais').select2({
            theme: "bootstrap",
            placeholder: "Selecciona un pais...",
            allowClear: true
        });


        $('#city_id').select2({
            theme: "bootstrap",
            placeholder: "Selecciona una ciudad...",
            allowClear: true
        });

        $('#sector').select2({
            theme: "bootstrap",
            placeholder: "Selecciona un sector...",
            allowClear: true
        });

        $('#actividadPrincipal').select2({
            theme: "bootstrap",
            placeholder: "Selecciona una actividad...",
            allowClear: true
        });

        $('#actividadSecundaria').select2({
            theme: "bootstrap",
            placeholder: "Selecciona una actividad...",
            allowClear: true
        });

        $('#promedioVentas').select2({
            theme: "bootstrap",
            placeholder: "Selecciona un promedio...",
            allowClear: true
        });

        $('#totalActivos').select2({
            theme: "bootstrap",
            placeholder: "Selecciona el total de activos...",
            allowClear: true
        });

        $('#operacionesInternacionales').select2({
            theme: "bootstrap",
            placeholder: "Selecciona una opcion...",
            allowClear: true
        });

        $('#tipoOperaciones').select2({
            theme: "bootstrap",
            placeholder: "Selecciona un tipo...",
            allowClear: true
        });

        $('#tieneSucursales').select2({
            theme: "bootstrap",
            placeholder: "Selecciona una opcion...",
            allowClear: true
        });

        /*
        $('#sucursales').select2({
            theme: "bootstrap",
            placeholder: "Seleccionar...",
            allowClear: true
        });
*/

    });


</script>
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7o0dvz8VLcNGtEld2hnV58UNugeFaIcs"></script>
<script src="/assets/js/atlantis.js"></script>
</body>
</html>
