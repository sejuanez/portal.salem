<?php

namespace App\Http\Controllers;

use App\Cities;
use App\Countries;
use App\empresas;
use App\Iglesias;
use App\Miembros;
use App\Ofrendasydiezmos;
use App\Reuniones;
use App\Mail\EnvioCorreos;
use App\States;
use App\User;
use App\Visitas;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        $pastores = array();
        $coordinadores = array();
        if (session()->get('id_iglesia', function () {
                return 'default';
            }) != "default") {
            $reuniones = Reuniones::where("iglesia_id", "=", session()->get('id_iglesia'))
                ->where("estado", "=", 0)
                ->orderBy('created_at', 'desc')
                ->take(5)
                ->get();

            $visitasPendientes = Visitas::where('estado', false)->where('iglesia_id', session()->get('id_iglesia'))->take(5)->get();

            $visitasTerminadas = Visitas::where('estado', true)->where('iglesia_id', session()->get('id_iglesia'))->take(5)->get();

            $reunionesCompletadas = Reuniones::where("iglesia_id", "=", session()->get('id_iglesia'))
                ->where("estado", "=", 1)
                ->orderBy('created_at', 'desc')
                ->take(5)
                ->get();

            $miembros = Miembros::where('iglesia_id', "=", session()->get('id_iglesia'))
                ->get()
                ->count();

            $numeroReunionesPendientes = Reuniones::where("iglesia_id", "=", session()->get('id_iglesia'))
                ->where("estado", "=", 0)
                ->get()
                ->count();

            $ofrenda = Ofrendasydiezmos::whereMonth('created_at', date("m"))
                ->where("iglesia_id", "=", session()->get('id_iglesia'))
                ->sum('ofrenda');

            $diezmo = Ofrendasydiezmos::whereMonth('created_at', date("m"))
                ->where("iglesia_id", "=", session()->get('id_iglesia'))
                ->sum('diezmo');

            $miembrosIglesia = Miembros::where('iglesia_id', "=", session()->get('id_iglesia'))->get();
            //$iglesia = Iglesias::findOrFail(session()->get('id_iglesia'));
            foreach ($miembrosIglesia as $miembro) {
                if ("Pastor" === $miembro->rol) {
                    array_push($pastores, $miembro->persona);
                } else if ("Coordinador" === $miembro->rol) {
                    array_push($coordinadores, $miembro->persona);
                }
            }
        } else {

            $reuniones = Reuniones::where("estado", "=", 0)
                ->orderBy('created_at', 'desc')
                ->take(5)
                ->get();

            $reunionesCompletadas = Reuniones::where("estado", "=", 1)
                ->orderBy('created_at', 'desc')
                ->take(5)
                ->get();

            $miembros = Miembros::all()->count();

            $numeroReunionesPendientes = Reuniones::where("estado", "=", 0)
                ->get()
                ->count();

            $ofrenda = Ofrendasydiezmos::whereMonth('created_at', date("m"))
                ->sum('ofrenda');

            $diezmo = Ofrendasydiezmos::whereMonth('created_at', date("m"))
                ->sum('diezmo');

            $visitasPendientes = Visitas::where('estado', false)->orderBy('created_at', 'desc')->take(5)->get();

            $visitasTerminadas = Visitas::where('estado', true)->orderBy('created_at', 'desc')->take(5)->get();

        }
        //dd($reuniones);

        /*
        $numeroVisitas = Visitas::where("iglesia_id", "=", session()->get('id_iglesia'))
            ->where("fecha_visita", "!=", "")
            ->get()
            ->count();
        */

        //dd(session()->get('id_iglesia'));

        return view('pages.dashboard', compact('reuniones', 'reunionesCompletadas', 'miembros', 'numeroReunionesPendientes', 'ofrenda', 'diezmo', 'pastores', 'coordinadores', 'visitasPendientes', 'visitasTerminadas'));
    }

    public
    function form()
    {
        return view('layouts.form');
    }

    public
    function registroPersonas()
    {
        return view('layouts.personas');
    }

    public
    function usuarios($id = null)
    {

        if ($id == null) {
            $roles = array();
            $role = Role::get();
            foreach ($role as $rol) {

                if (Auth::user()->getRoleNames()[0] == 'Super Admin' && $rol->name == 'Super Admin') {
                    array_push($roles, $rol);
                }
                if ($rol->name != 'Super Admin') {
                    array_push($roles, $rol);
                }
            }
            return view('pages.usuarios', compact('roles'));
        } else {
            $usuario = User::findOrFail($id);
            return view('pages.perfil.usuario', compact('usuario'));

        }

    }

    public
    function iglesias($id = null)
    {

        if ($id == null) {
            $roles = array();
            $role = Role::get();
            foreach ($role as $rol) {

                if (Auth::user()->getRoleNames()[0] == 'Super Admin' && $rol->name == 'Admin') {
                    array_push($roles, $rol);
                }
                if ($rol->name != 'Super Admin') {
                    array_push($roles, $rol);
                }
            }
            return view('pages.iglesias', compact('roles'));
        } else {
            $iglesia = Iglesias::findOrFail($id);
            return view('pages.perfil.iglesia', compact('iglesia'));

        }

    }

    public
    function visitas()
    {
        if (session()->get('id_iglesia', function () {
                return 'default';
            }) != "default" or (Auth::user()->getRoleNames()[0] == 'Super Admin' or Auth::user()->getRoleNames()[0] == 'Admin')) {
            return view('pages.visitas');

        }
        return redirect()->action('HomeController@index');

    }

    public
    function reuniones()
    {
        if (session()->get('id_iglesia', function () {
                return 'default';
            }) != "default" or (Auth::user()->getRoleNames()[0] == 'Super Admin' or Auth::user()->getRoleNames()[0] == 'Admin')) {
            return view('pages.reuniones');

        }
        return redirect()->action('HomeController@index');
    }

    public
    function miembros()
    {
        if (session()->get('id_iglesia', function () {
                return 'default';
            }) != "default") {
            $roles = array();
            $role = Role::get();
            foreach ($role as $rol) {

                if (in_array("Super Admin", Auth::user()->getRoleNames()->toArray()) && $rol->name == 'Super Admin') {
                    array_push($roles, $rol);
                }
                if ($rol->name != 'Super Admin' and $rol->name != 'Admin') {
                    array_push($roles, $rol);
                }
            }
            return view('pages.miembros', compact('roles'));

        }

        return redirect()->action('HomeController@index');

    }

    public
    function ofrendasYDiezmos()
    {
        if (session()->get('id_iglesia', function () {
                return 'default';
            }) != "default") {
            return view('pages.ofrendasYDiezmos');
        }
        return redirect()->action('HomeController@index');
    }

    public
    function perfil()
    {
        $usuario = Auth::user();
        return view('pages.perfil.usuario', compact('usuario'));
    }

    public
    function roles()
    {
        $permissions = Permission::get();
        return view('pages.roles', compact('permissions'));
    }

    public
    function permisos()
    {
        return view('pages.permisos');
    }

    public
    function miIglesia()
    {
        $iglesia = Iglesias::findOrFail(Auth::user()->persona->iglesia_id);
        return view('pages.perfil.iglesia', compact('iglesia'));

    }

    public
    function email()
    {
        set_time_limit(0);
        /* $usuarios = User::where('id', '>=', 84)->get();
         foreach ($usuarios as $usuario) {
             Mail::to($usuario->email)->send(new EnvioCorreos($usuario));
         }*/
        return response(
            ['message' => 'Mensajes Enviados con exito']);
    }

    public
    function estadisticas()
    {
        return view('pages.estadisticas');
    }

    public
    function cuestionario($id = null)
    {
        //validar si es asignada a su id
        if ($id == null) {
            return redirect()->action('HomeController@index');
        } else {
            $visita = Visitas::findOrFail($id);
            return view('pages.cuestionario', compact('visita'));
        }
    }

    public
    function reportes()
    {
        return view('pages.reportes');
    }

    public
    function pdf()
    {
        set_time_limit(0);
        $iglesias = Iglesias::where('estado', true)->get();
        $data = [
            'iglesias' => $iglesias,
            'filtro' => 'Pais',
            'filtroValor' => 45
        ];

        $pdf = PDF::loadView('pages/pdf/reporte_iglesias', $data);
        return $pdf->stream();
    }

    public
    function reporte_iglesias($pais, $departamento, $ciudad)
    {
        set_time_limit(0);
        // iglesia con id 69 no aparece
        $iglesias = Iglesias::where('estado', true)->where('id', '!=', 69)->where('tipo', 'Iglesia')->get();
        $paisR = Countries::find($pais);
        $departamentoR = States::find($departamento);
        $ciudadR = Cities::find($ciudad);
        $data = [
            'iglesias' => $iglesias,
            'pais' => $pais,
            'departamento' => $departamento,
            'ciudad' => $ciudad,
            'paisR' => $paisR,
            'departamentoR' => $departamentoR,
            'ciudadR' => $ciudadR,
        ];

        $pdf = PDF::loadView('pages/pdf/reporte_iglesias', $data);
        return $pdf->stream();
    }

    public
    function reporte_asistencia_ingresos($fecha)
    {
        if (session()->get('id_iglesia', function () {
                return 'default';
            }) != "default") {


            set_time_limit(0);
            $iglesias = Iglesias::where('id', session()->get('id_iglesia'))->where('estado', true)->where('tipo', 'Iglesia')->get();
            $reuniones = Reuniones::where('iglesia_id', session()->get('id_iglesia'))->where('fecha_realizado', 'like', $fecha . '%')->get();
            $ingresos = Ofrendasydiezmos::where('iglesia_id', session()->get('id_iglesia'))->where('fecha', 'like', $fecha . '%')->get();

            $data = [
                'iglesias' => $iglesias[0],
                'reuniones' => $reuniones,
                'ingresos' => $ingresos,
                'fecha' => $fecha
            ];

            $pdf = PDF::loadView('pages/pdf/reporte_ingresos_asistencia', $data);
            return $pdf->stream();
        } else {
            return null;
        }
    }

    public
    function reporte_iglesias_asistencia_ingresos($pais, $departamento, $ciudad, $fecha)
    {
        set_time_limit(0);
        $iglesias = Iglesias::where('estado', true)->where('id', '!=', 69)->where('tipo', 'Iglesia')->get();
        $paisR = Countries::find($pais);
        $departamentoR = States::find($departamento);
        $ciudadR = Cities::find($ciudad);
        $data = [
            'iglesias' => $iglesias,
            'pais' => $pais,
            'departamento' => $departamento,
            'ciudad' => $ciudad,
            'paisR' => $paisR,
            'departamentoR' => $departamentoR,
            'ciudadR' => $ciudadR,
            'fecha' => $fecha
        ];

        $pdf = PDF::loadView('pages/pdf/reporte_iglesias_ingresos_asistencia', $data);
        return $pdf->stream();

    }
}
