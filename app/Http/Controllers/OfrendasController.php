<?php

namespace App\Http\Controllers;

use App\Ofrendasydiezmos;
use Illuminate\Http\Request;

class OfrendasController extends Controller
{
    //
    function verOfrendasYDiezmos(Request $request)
    {

        if (session()->get('id_iglesia') == null) {
            $respuesta = Ofrendasydiezmos::all();

        } else {
            $respuesta = Ofrendasydiezmos::where('iglesia_id', session()->get('id_iglesia'))->get();
        }

        $response = array(
            'status' => 'success',
            'msg' => $respuesta,
        );

        return response()->json($response);
    }

    function guardarOfrendasYDiezmos(Request $request)
    {

        $this->validate($request, [
            'fecha' => 'required|date',
            'ofrenda' => 'required|numeric',
        ], [
            'fecha.required' => 'Seleccione una fecha',
            'ofrenda.required' => 'Ingrese el valor total de los ingresos',
        ]);

        $diezmoyofrenda = Ofrendasydiezmos::create([
            'descripcion' => $request->descripcion,
            'fecha' => $request->fecha,
            'ofrenda' => $request->ofrenda,
            'diezmo' => 0,
            'iglesia_id' => session()->get('id_iglesia'),
            'user_id' => auth()->user()->id,
        ]);

        $response = array(
            'status' => 'success',
            'msg' => 'Diezmo y ofrenda agregado con exito',
        );
        return response()->json($response);

    }

    function buscarOfrendasYDiezmos()
    {
    }

    function eliminarOfrendaYDiezmo(Request $request)
    {
        $respuesta = Ofrendasydiezmos::find($request->id);
        $respuesta->delete();
        $response = array(
            'status' => 'success',
            'msg' => 'Diezmo y ofrenda Eliminado con exito',
        );
        return response()->json($response);
    }

    function actualizarOfrendaYDiezmo(Request $request)
    {
        $this->validate($request, [
            'fecha' => 'required|string',
            'ofrenda' => 'required|string',
        ], [
            'fecha.required' => 'Seleccione una fecha',
            'ofrenda.required' => 'Ingrese el valor total de los ingresos',
        ]);
        $respuesta = Ofrendasydiezmos::findOrFail($request->id);
        $respuesta->update([
            'descripcion' => $request->descripcion,
            'fecha' => $request->fecha,
            'ofrenda' => $request->ofrenda,
            'diezmo' => 0,
        ]);
        $response = array(
            'status' => 'success',
            'msg' => 'Diezmo y ofrenda actualizado con exito',
        );
        return response()->json($response);
    }

}
