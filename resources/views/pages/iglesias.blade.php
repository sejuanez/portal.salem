@extends('layouts.dashboard')

@section('content')

    <div class="content container-full">

        <div class="page-navs bg-white">
            <div class="nav-scroller">
                <div
                    class="nav nav-tabs nav-line nav-color-secondary d-flex align-items-center justify-contents-center w-100">
                    <a class="nav-link active show" data-toggle="tab" href="#tab1">Iglesias
                    </a>
                    {{--                    <a class="nav-link mr-5" data-toggle="tab" href="#tab2">General</a>--}}
                    <div class="ml-auto">
                        @can('crear.iglesias')
                            <button class="btn btn-primary btn-round ml-auto" id="nueva-iglesia">
                                <i class="fa fa-plus"></i>
                                Nueva Iglesia
                            </button>
                        @endcan
                    </div>
                </div>
            </div>
        </div>

        <div class="page-inner">
            <div class="tab-content mt-2 mb-3" id="card-iglesias">
                <div class="tab-pane fade active show" id="tab1" role="tabpanel" aria-labelledby="pills-home-tab">

                    <div class="row">
                        <div class="col-md-12"></div>
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">

                                    <div class="table-responsive">
                                        <table id="tablaEmpleados" class="display table table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Direccion</th>
                                                <th>Depart</th>
                                                <th>Tipo</th>
                                                <th>Estado</th>
                                                <th>Actualizado</th>
                                                <th class="text-right">Opciones</th>
                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Direccion</th>
                                                <th>Depart</th>
                                                <th>Tipo</th>
                                                <th>Estado</th>
                                                <th>Actualizado</th>
                                                <th class="text-right">Opciones</th>
                                            </tr>
                                            </tfoot>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div>
                        <div class="card">
                            <div class="card-body">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="card-registro" style="display: none">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <h4 class="card-title">
                                <span class="fw-mediumbold" id="accion-modal">Nueva</span>
                                <span class="fw-light">Iglesia</span>
                            </h4>
                        </div>
                    </div>
                    <div class="card-body">

                        <form id="iglesia-form">
                            @csrf
                            <input type="hidden" id="id">
                            <div class="row">

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="name" class="text-right">
                                            Nombre
                                            <span class="required-label">*</span>
                                        </label>
                                        <input type="text" class="form-control" id="nombre" name="nombre"
                                               placeholder="Nombre de la iglesia" required="">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="name" class="text-right">
                                            Direccion
                                            <span class="required-label">*</span>
                                        </label>
                                        <input type="text" class="form-control" id="direccion" name="direccion"
                                               autocomplete="nio"
                                               placeholder="Direccion de la iglesia" required="">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="name" class="text-right">
                                            Barrio
                                            <span class="required-label">*</span>
                                        </label>
                                        <input type="text" class="form-control" id="barrio" name="barrio"
                                               placeholder="Barrio de la iglesia" required="">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="select-pais">Pais</label>
                                        <select class="form-control" name="select-pais" id="select-pais"
                                                style="width: 100%">
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="select-departamento">Departamento/Estado</label>
                                        <select class="form-control" name="select-departamento"
                                                id="select-departamento"
                                                style="width: 100%">
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="city_id">Ciudad</label>
                                        <select class="form-control" name="city_id" id="city_id"
                                                style="width: 100%">
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="select-tipo">Tipo de iglesia</label>
                                        <select class="form-control" id="tipo" name="tipo">
                                            <option value="Grupo">Grupo</option>
                                            <option value="Iglesia">Iglesia</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="name" class="text-right">
                                            Latitud
                                            <span class="required-label">*</span>
                                        </label>
                                        <input type="text" class="form-control" id="latitud" name="latitud"
                                               placeholder="Latitud de la iglesia" required="">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="name" class="text-right">
                                            Longitud
                                            <span class="required-label">*</span>
                                        </label>
                                        <input type="text" class="form-control" id="longitud" name="longitud"
                                               placeholder="Longitud de la iglesia" required="">
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="comment">Nota:</label>
                                        <textarea class="form-control" id="nota" name="nota" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger text-danger" id="error" style="display: none">
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>

                    <div class="card-footer text-right">
                        <button type="button" id="guardar" class="btn btn-primary">Guardar</button>
                        <button type="button" id="actualizar" class="btn btn-primary">Actualizar</button>
                        <button type="button" id="eliminar" class="btn btn-warning">Eliminar</button>
                        <button type="button" id="cancelar-registro" class="btn btn-danger">Cancelar</button>
                    </div>

                </div>
            </div>

        </div>

    </div>
    @include('pages.modal.iglesia')

@endsection

@section('scripts')
    <script type="text/javascript">

        var ID_IGLESIA = 0;

        var PAIS_ID = 0;
        var DEPARTAMENTO_ID = 0;
        var CIUDAD_ID = 0;

        $('#card-registro').hide();

        $(document).ready(function () {

            $('#select-pais').select2({
                theme: "bootstrap",
                placeholder: "Selecciona un pais...",
                allowClear: true,
            });

            $("#select-departamento").select2({
                theme: "bootstrap",
                placeholder: "Selecciona un departamento...",
                allowClear: true,
            });

            $("#city_id").select2({
                allowClear: true,
                theme: "bootstrap",
                placeholder: "Selecciona una ciudad...",
            });

            getPaises();

            $('#select-pais').on('change', function (e) {
                getDepartamentos($("#select-pais").val());
                $('#select-departamento').empty().trigger("change");
                $('#city_id').empty().trigger("change");
            });
            $('#select-departamento').on('change', function (e) {
                $('#city_id').empty().trigger("change");
                getMunicipios($("#select-departamento").val());
            });

        });

        var TABLA = $('#tablaEmpleados').DataTable({

            scrollY: 400,
            scrollCollapse: true,
            autofill: true,

            "ajax": {
                "url": "recurso/verIglesias",
                "type": "GET",
                "dataSrc": function (data) {

                    var json = [];
                    console.log(data);
                    for (var item in data.msg) {

                        var estado = (data.msg[item].iglesia.estado == 0 ? '<span class="badge badge-danger">Inactiva</span>' : '<span class="badge badge-success">Activa</span>');

                        var itemJson = {
                            Id: data.msg[item].iglesia.id,
                            Nombre: data.msg[item].iglesia.nombre,
                            Direccion: data.msg[item].iglesia.direccion,
                            Departamento: data.msg[item].state ? data.msg[item].state.name : '',
                            Barrio: data.msg[item].iglesia.barrio,
                            Citi_id: data.msg[item].iglesia.city_id,
                            Tipo: data.msg[item].iglesia.tipo,
                            Nota: data.msg[item].iglesia.nota,
                            Estado: estado,
                            Activo: data.msg[item].iglesia.estado == 0,
                            Latitud: data.msg[item].iglesia.latitud,
                            Longitud: data.msg[item].iglesia.longitud,
                            Actualizado: data.msg[item].iglesia.updated_at,
                            Opciones: opciones(data.msg[item].iglesia.id, data.msg[item].iglesia.estado)
                        };
                        json.push(itemJson)
                    }
                    return json;
                    $('#card-iglesias .card').removeClass("is-loading");
                }
            },
            columns: [
                {data: "Nombre"},
                {data: "Direccion"},
                {data: "Departamento"},
                {data: "Tipo"},
                {data: "Estado"},
                {data: "Actualizado"},
                {data: "Opciones"},
            ],
        });

        function opciones(id, estado) {
            var opciones = '';
            @can('editar.iglesias')
                opciones += '<button type="button" class="btn btn-primary btn-xs editar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Actualizar" data-original-title="Edit"' +
                '           style="margin-right: 5px;">\n' +
                '           <i class="fas fa-pen"></i>\n' +
                ' </button>';
            @endcan
                opciones += '' +
                '<button type="button" class="btn btn-success btn-xs ver" ' +
                '           data-toggle="tooltip" data-placement="top" title="Ver Perfil" data-original-title="Edit"' +
                '           style="margin-right: 5px;">\n' +
                '           <i class="fas fa-eye"></i>\n' +
                ' </button>';

            @can('eliminar.iglesias')
            if (estado == 1) {
                opciones += '' +
                    '<button type="button" class="btn btn-danger btn-xs cambiarEstado" ' +
                    '           data-toggle="tooltip" data-placement="top" title="Deshabilitar" data-original-title="Edit">' +
                    '           <i class="fas fa-trash"></i>\n' +
                    ' </button>';
            } else {
                opciones += '' +
                    '<button type="button" class="btn btn-success btn-xs cambiarEstado" ' +
                    '           data-toggle="tooltip" data-placement="top" title="Habilitar" data-original-title="Edit">' +
                    '           <i class="fas fa-check"></i>\n' +
                    ' </button>';
            }
            @endcan
                return opciones;
        }

        $("#cancelar-registro").on('click', function () {

            $('#card-registro').hide();
            $('#card-iglesias').show();
            $('#card-iglesias').addClass('animated fadeIn');

            $('#iglesia-form')[0].reset();

        });

        TABLA.on('click', '.ver', function () {

            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            console.log(data);

            window.location.href = '{{url('iglesias')}}/' + data.Id;

        });

        @can('crear.iglesias')

        $("#nueva-iglesia").on('click', function () {

            getPaises(47);

            $('#card-iglesias').hide();
            $('#card-registro').show();
            $('#card-registro').addClass('animated fadeIn');

            $('#iglesia-form')[0].reset();
            $('#actualizar').hide();
            $('#eliminar').hide();
            $('#guardar').show();
            $("#error").hide();
            $("#accion-modal").html('Registrar');

        });

        $("#guardar").on('click', function () {

            let formData = new FormData(document.getElementById("iglesia-form"));
            //formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
            let csrfToken = '{{ csrf_token() }}';
            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#guardar").prop('disabled', true);
            $.ajax({
                url: '/guardar-iglesia',
                type: 'POST',
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                data: formData,
                processData: false,
                contentType: false,
                cache: false

            }).done(function (response) {
                $('#iglesia-form')[0].reset();
                $('#modal').modal('hide');
                $("#guardar").prop('disabled', false);
                TABLA.ajax.reload();

                $('#card-registro').hide();
                $('#card-iglesias').show();
                $('#card-iglesias').addClass('animated fadeIn');

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: 'Iglesia Guardada Correctamente',
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#guardar").prop('disabled', false);
            });


        });

        @endcan

        @can('editar.iglesias')

        TABLA.on('click', '.editar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();

            $("#actualizar").prop('disabled', false);
            $('#iglesia-form')[0].reset();
            getPaises(47);

            $('#actualizar').show();
            $('#eliminar').hide();
            $('#guardar').hide();
            $("#error").hide();
            $("#accion-modal").html('Editar');

            $('#card-iglesias').hide();
            $('#card-registro').show();
            $('#card-registro').addClass('animated fadeIn');

            ID_IGLESIA = data.Id;

            $("#id").val(data.Id);
            $('#nombre').val(data.Nombre);
            $('#direccion').val(data.Direccion);
            $('#barrio').val(data.Barrio);
            $('#tipo').val(data.Tipo);
            $('#nota').val(data.Nota);
            $('#latitud').val(data.Latitud);
            $('#longitud').val(data.Longitud);

            getStateofCity(data.Citi_id);

            CIUDAD_ID = data.Citi_id;

        });

        $("#actualizar").on('click', function () {

            console.log(ID_IGLESIA)

            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#actualizar").prop('disabled', true);
            $.ajax({
                url: "{{ url('recurso/iglesias/') }}/" + ID_IGLESIA,
                type: 'PUT',
                data: {
                    'nombre': $('#nombre').val(),
                    'direccion': $('#direccion').val(),
                    'barrio': $('#barrio').val(),
                    'city_id': $('#city_id').val(),
                    'tipo': $('#tipo').val(),
                    'nota': $('#nota').val(),
                    'latitud': $('#latitud').val(),
                    'longitud': $('#longitud').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
            }).done(function (response) {

                console.log(response)

                TABLA.ajax.reload();

                $('#card-registro').hide();
                $('#card-iglesias').show();
                $('#card-iglesias').addClass('animated fadeIn');

                $('#iglesia-form')[0].reset();

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.message,
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#actualizar").prop('disabled', false);
            });

        });

        @endcan

        @can('eliminar.iglesias')

        TABLA.on('click', '.cambiarEstado', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            let opcion = "";

            data.Activo == 0 ? opcion = "Deshabilitar" : opcion = "Habilitar";

            swal({
                title: 'Estas seguro?',
                text: "Vas a " + opcion + " la iglesia: " + data.Nombre,
                icon: 'warning',
                buttons: {
                    confirm: {
                        text: 'Si, ' + opcion,
                        className: 'btn btn-success'
                    },
                    cancel: {
                        text: 'Cancelar',
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((Delete) => {
                if (Delete) {

                    $.ajax({
                            url: '/verificarIglesia',
                            type: 'POST',
                            data: {
                                id: data.Id,
                                _token: $('meta[name="csrf-token"]').attr('content')
                            },

                        }
                    ).done(function (response) {
                        console.log(response);
                        if (response.status == "Error") {
                            //error
                        } else {

                            let msg = "";

                            response.msg.estado ? msg = "habilitada" : msg = "deshabilitada";

                            TABLA.ajax.reload();
                            swal({
                                title: 'Ok!',
                                text: 'La iglesia ha sido ' + msg,
                                icon: 'success',
                                buttons: {
                                    confirm: {
                                        className: 'btn btn-success'
                                    }
                                }
                            });
                        }
                        //return response;
                    }).fail(function (error) {

                        console.log(error);

                    });


                } else {
                    swal.close();
                }
            });

        });

        @endcan

        function getPaises(codPais) {

            let COUNTRIES = [];

            //$('#modal .modal-content').addClass("is-loading");

            $.ajax({
                url: "/getCountries",
                async: false
            }).done(function (data) {

                $('#modal .modal-content').removeClass("is-loading");
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    COUNTRIES.push(itemSelect2)
                }
                $('#select-pais').select2({
                    theme: "bootstrap",
                    placeholder: "Selecciona un pais...",
                    allowClear: true,
                    data: COUNTRIES
                });

                if (codPais != null) {
                    $("#select-pais").val(47).trigger('change');
                }

            });

        }

        function getDepartamentos(codPais) {

            let STATES = [];
            $.get(
                "/getStates", {'countrie': codPais},
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    STATES.push(itemSelect2)
                }

                $("#select-departamento").select2({
                    theme: "bootstrap",
                    placeholder: "Selecciona un departamento...",
                    allowClear: true,
                    data: STATES
                });

                $("#select-departamento").trigger('change')


            });

        }

        function getMunicipios(codDepartamento) {

            var cities = [];
            $('#modal .modal-content').addClass("is-loading");

            $('#city_id').empty().trigger("change");

            $.get(
                "/getCities", {'state': codDepartamento}
            ).done(function (data) {
                console.log(data);
                $('#modal .modal-content').removeClass("is-loading");
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    cities.push(itemSelect2)
                }

                $("#city_id").select2({
                    allowClear: true,
                    theme: "bootstrap",
                    placeholder: "Selecciona una ciudad...",
                    data: cities
                });


            });

        }

        function getStateofCity(id) {

            console.log(id);

            $.ajax({
                url: "/getStatesOfCity",
                data: {
                    city: id
                },
                async: false,

                success: function (departameto) {
                    getCountrieOfState(departameto, id);
                }
            });


        }

        function getCountrieOfState(departamento, ciudad) {

            $('.card .card-body').addClass("is-loading");

            $.ajax({
                url: "/getCountrieOfState",
                data: {
                    state: departamento.msg
                },
                async: false
            }).done(function (data) {
                PAIS_ID = data.msg.country_id;
                console.log(PAIS_ID)
                $("#select-pais").val(PAIS_ID).trigger('change');

                // ---------------- DEPARTAMENTOS -------------------------//

                let STATES = [];
                $.get(
                    "/getStates", {'countrie': PAIS_ID},
                ).done(function (data) {
                    for (var item in data.msg) {
                        var itemSelect2 = {
                            id: data.msg[item].id,
                            text: data.msg[item].name
                        };
                        STATES.push(itemSelect2)
                    }

                    $("#select-departamento").select2({
                        theme: "bootstrap",
                        placeholder: "Selecciona un departamento...",
                        allowClear: true,
                        data: STATES
                    });

                    $("#select-departamento").val(departamento.msg).trigger('change')

                    // ---------------- CIUDADES -------------------------//


                    var cities = [];


                    $.get(
                        "/getCities", {'state': departamento.msg}
                    ).done(function (data) {


                        for (var item in data.msg) {
                            var itemSelect2 = {
                                id: data.msg[item].id,
                                text: data.msg[item].name
                            };
                            cities.push(itemSelect2)
                        }

                        $('#city_id').empty().trigger("change");

                        $("#city_id").select2({
                            allowClear: true,
                            theme: "bootstrap",
                            placeholder: "Selecciona una ciudad...",
                            data: cities
                        });

                        $("#city_id").val(ciudad).trigger('change')

                        $('.card .card-body').removeClass("is-loading");

                    });


                });


            });

        }

        function getPastores() {

            let PASTORES = [];

            $.ajax({
                url: "/getPastores",
                async: false
            }).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    COUNTRIES.push(itemSelect2)
                }
                $('#select-pais').select2({
                    theme: "bootstrap",
                    dropdownParent: $('#modal'),
                    placeholder: "Selecciona un pais...",
                    allowClear: true,
                    data: COUNTRIES
                });

                $('#select-pais').val('47'); // Select the option with a value of '1'
                $('#select-pais').trigger('change'); // Notify any JS components that the value changed
                getDepartamentos(47)

            });

        }

    </script>
@endsection
