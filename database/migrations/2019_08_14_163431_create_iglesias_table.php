<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIglesiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iglesias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('direccion');
            $table->string('barrio');
            $table->string('city_id');
            $table->string('tipo');
            $table->string('nota')->nullable();
            $table->integer('colaborador_id')->unsigned()->nullable();
            $table->integer('coordinador_id')->unsigned()->nullable();
            $table->integer('pastor_id')->unsigned()->nullable();
            $table->string('latitud')->nullable();
            $table->string('longitud')->nullable();
            $table->boolean('estado')->default(1);

            $table->foreign('colaborador_id')->references('id')->on('users');
            $table->foreign('coordinador_id')->references('id')->on('users');
            $table->foreign('pastor_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('iglesias');
    }
}
