<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Personas extends Model
{
    //
    use Notifiable;

    protected $fillable = [
        'nombres',
        'apellidos',
        'cedula',
        'telefono',
        'fechaNacimiento',
        'nacimientoHijos',
        'estadoCivil',
        'escolaridad',
        'estudios',
        'trabajo',
        'idiomas',
        'formacionMusical',
        'fechaIngreso',
        'recorridoIglesia',
        'usuario',
        'contrasenia',
        'tipoUsuario',
        'user_id',
    ];

    public function miembro()
    {
        return $this->hasMany(Miembros::class, 'persona_id');
    }

    public function reuniones()
    {
        return $this->hasMany(Reuniones::class, 'persona_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
