<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('cedula')->unique()->nullable();
            $table->string('telefono')->nullable();
            $table->date('fechaNacimiento')->nullable();
            $table->string('nacimientoHijos')->nullable();
            $table->string('estadoCivil')->nullable();
            $table->string('escolaridad')->nullable();
            $table->string('estudios')->nullable();
            $table->string('trabajo')->nullable();
            $table->string('idiomas')->nullable();
            $table->string('formacionMusical')->nullable();
            $table->date('fechaIngreso')->nullable();
            $table->string('recorridoIglesia')->nullable();
            $table->string('coordinadorObra')->nullable();
            $table->string('ciudadObra')->nullable();
            $table->string('direccionObra')->nullable();
            $table->string('barrioObra')->nullable();
            $table->string('nombreObra')->nullable();
            $table->string('fechaObra')->nullable();
            $table->string('liderAnterior')->nullable();

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
