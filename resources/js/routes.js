import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'dashboard',
            component: require('./views/Dashboard').default
        },
        {
            path: '/producido',
            name: 'producido',
            component: require('./views/producido').default
        },
        {
            path: '/pacientes',
            name: 'pacientes',
            component: require('./views/Pacientes').default
        },
        {
            path: '/empleados',
            name: 'empleados',
            component: require('./views/Empleados').default
        },
        {
            path: '/historico',
            name: 'historico',
            component: require('./views/Historico').default
        },
        {
            path: '/servicios',
            name: 'servicios',
            component: require('./views/Servicios').default
        },
        {
            path: '/perfil',
            name: 'perfil',
            component: require('./views/MiPerfil').default
        },
        {
            path: '*',
            component: require('./views/404').default
        }
    ],
    mode: 'history',

})