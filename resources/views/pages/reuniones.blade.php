@extends('layouts.dashboard')

@section('content')

    <div class="content container-full">

        <div class="page-navs bg-white">
            <div class="nav-scroller">
                <div
                    class="nav nav-tabs nav-line nav-color-secondary d-flex align-items-center justify-contents-center w-100">
                    <a class="nav-link mr-5 active show" data-toggle="tab" href="#tab1" id="tabR1">
                        Reuniones Pendientes
                    </a>
                    <a class="nav-link mr-4" data-toggle="tab" href="#tab2" id="tabR2">
                        Reuniones Terminadas
                    </a>
                    {{--                    <a class="nav-link mr-4" data-toggle="tab" href="#tab4" id="tabR3">--}}
                    {{--                        Reuniones Antiguas--}}
                    {{--                    </a>--}}
                    <div class="ml-auto">

                        @if(session()->get('id_iglesia', function () {return 'default';}) !="default")
                            @can('crear.reuniones')
                                <button class="btn btn-primary btn-round ml-auto" id="nueva-reuniom">
                                    <i class="fa fa-plus"></i>
                                    Nueva Reunion
                                </button>
                            @endcan
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="page-inner">

            <div class="tab-content mt-2 mb-3" id="card-reuniones">
                <div class="tab-pane fade active show" id="tab1" role="tabpanel" aria-labelledby="pills-home-tab">

                    <div>
                        <div class="card">
                            <div class="card-body">

                                <div class="table-responsive">
                                    <table id="tablaReunionesPendientes"
                                           class="display table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Adultos</th>
                                            <th>Niños</th>
                                            <th>Fecha</th>
                                            <th>Actualizado</th>
                                            <th class="text-right">Opciones</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Adultos</th>
                                            <th>Niños</th>
                                            <th>Fecha</th>
                                            <th>Actualizado</th>
                                            <th class="text-right">Opciones</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="pills-home-tab">

                    <div>
                        <div class="card">
                            <div class="card-body">

                                <div class="table-responsive">
                                    <table id="tablaReunionesTerminadas"
                                           class="display table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>#Adultos</th>
                                            <th>#Niños</th>
                                            <th>fecha</th>
                                            <th>Actualizado</th>
                                            <th class="text-right">Opciones</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>#Adultos</th>
                                            <th>#Niños</th>
                                            <th>fecha</th>
                                            <th>Actualizado</th>
                                            <th class="text-right">Opciones</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div id="card-registro" style="display: none">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <h4 class="card-title">
                                <span class="fw-mediumbold" id="accion-modal">Nueva</span>
                                <span class="fw-light">Reunión</span>
                            </h4>
                        </div>
                    </div>
                    <div class="card-body">

                        <form id="reuniones-form">
                            @csrf
                            <input type="hidden" id="id">

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="nombre" class="text-right">
                                            Nombre
                                            <span class="required-label">*</span>
                                        </label>
                                        <input type="text" class="form-control" id="nombre" name="nombre"
                                               autocomplete="nio"
                                               placeholder="Nombre de la reunion" required="">
                                    </div>
                                </div>

                                <div class="col-sm-3" style="display: none">
                                    <div class="form-group">
                                        <label for="tipo" class="text-right">
                                            Tipo
                                            <span class="required-label">*</span>
                                        </label>
                                        <select class="form-control" id="tipo" name="tipo">
                                            <option value="Diario">Diario</option>
                                            <option value="Semanal">Semanal</option>
                                            <option value="Mensual">Mensual</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="num_adultos" class="text-right">
                                            Numero de adultos
                                            <span class="required-label">*</span>
                                        </label>
                                        <input type="number" class="form-control" id="num_adultos" name="num_adultos"
                                               autocomplete="nio"
                                               placeholder="Numero de adultos" required="">
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="num_ninios" class="text-right">
                                            Numero de niños
                                            <span class="required-label">*</span>
                                        </label>
                                        <input type="number" class="form-control" id="num_ninios" name="num_ninios"
                                               placeholder="Numero de niños" required="">
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="fecha_realizado">Fecha de realización</label>
                                        <input type="text" class="form-control" id="fecha_realizado"
                                               name="fecha_realizado"
                                               required="">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="hora_realizado">Hora de realización</label>
                                        <input type="text" class="form-control" id="hora_realizado"
                                               name="hora_realizado"
                                               required="">
                                    </div>
                                </div>


                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="comment">Nota:</label>
                                        <textarea class="form-control" id="descripcion" name="descripcion"
                                                  rows="3"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger text-danger" id="error" style="display: none">
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>

                    <div class="card-footer text-right">
                        <button type="button" id="guardar" class="btn btn-primary">Guardar</button>
                        <button type="button" id="actualizar" class="btn btn-primary">Actualizar</button>
                        <button type="button" id="eliminar" class="btn btn-warning">Eliminar</button>
                        <button type="button" id="cancelar-registro" class="btn btn-danger">Cancelar</button>
                    </div>

                </div>
            </div>

            <div id="card-miembros" style="display: none">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-items-center">
                            <h4 class="card-title">
                                <span class="fw-mediumbold" id="accion-modal">Gestionar </span>
                                <span class="fw-light">Miembros</span>
                            </h4>
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="row" id="registroPersona">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="select-miembros">Miembro</label>
                                    <select id="select-miembros" name="select-miembros" class="form-control">
                                        <option value="">Seleccionar...</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-4">
                                <button id="btn-agregar-miembro" name="btn-agregar-miembro" class="btn btn-primary"
                                        style="margin-top: 35px;">
                                    <span class="btn-label"><i class="fa fa-arrow-down"></i></span> Agregar
                                </button>
                            </div>
                        </div>

                        <div>
                            <div class="">
                                <div class="">

                                    <div class="table-responsive">
                                        <table id="tabla-asistentes" class="display table table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Cedula</th>
                                                <th>Agregado el</th>
                                                <th class="text-center" width="50">Opciones</th>
                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Cedula</th>
                                                <th>Agregado el</th>
                                                <th class="text-center" width="50">Opciones</th>
                                            </tr>
                                            </tfoot>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer text-right">
                        <button type="button" id="btn-atras-miembro" class="btn btn-danger">Atras</button>
                    </div>

                </div>
            </div>

        </div>

    </div>
    @include('pages.modal.reunion')

@endsection

@section('scripts')
    <script type="text/javascript">

        var ID_IGLESIA = 0;

        let ID_REUNION = null;
        let ID_REUNION_TERMINADO = null;
        var PAIS_ID = 0;
        var DEPARTAMENTO_ID = 0;
        var CIUDAD_ID = 0;

        $('#card-registro').hide();

        $(document).ready(function () {

            $("#select-miembros").select2({
                theme: "bootstrap",
            });


        });

        var TABLA_REUNIONES = $('#tablaReunionesPendientes').DataTable({
            scrollY: 400,
            scrollCollapse: true,
            autofill: true,
            "ajax": {
                "url": "recurso/getReuniones",
                "type": "GET",
                "dataSrc": function (data) {
                    $('#card-reuniones').addClass("is-loading");
                    var json = [];
                    console.log(data);
                    for (var item in data.msg) {
                        var itemJson = {
                            Id: data.msg[item].id,
                            Nombre: data.msg[item].nombre,
                            Tipo: data.msg[item].tipo,
                            NumAdultos: data.msg[item].num_adultos,
                            NumNinios: data.msg[item].num_ninios,
                            Fecha_realizado: data.msg[item].fecha_realizado,
                            Fecha_Y_Hora: data.msg[item].fecha_realizado + ' <br> ' + data.msg[item].hora_realizado,
                            Hora_realizado: data.msg[item].hora_realizado,
                            Actualizado: data.msg[item].updated_at,
                            Descripcion: data.msg[item].descripcion,
                            Opciones: opciones(data.msg[item].id, data.msg[item].estado)
                        };
                        json.push(itemJson)
                    }
                    $('#card-reuniones').removeClass("is-loading");
                    return json;
                }
            },
            columns: [
                {data: "Nombre"},
                {data: "NumAdultos"},
                {data: "NumNinios"},
                {data: "Fecha_Y_Hora"},
                {data: "Actualizado"},
                {data: "Opciones"},
            ],
        });

        var TABLA_REUNIONES_TERMINADAS = $('#tablaReunionesTerminadas').DataTable({
            scrollY: 400,
            scrollCollapse: true,
            autofill: true,
            "ajax": {
                "url": "recurso/getReunionesCompletadas",
                "type": "GET",
                "dataSrc": function (data) {

                    $('#card-reuniones').addClass("is-loading");

                    var json = [];
                    console.log(data);
                    for (var item in data.msg) {

                        var itemJson = {
                            Id: data.msg[item].id,
                            Nombre: data.msg[item].nombre,
                            Tipo: data.msg[item].tipo,
                            NumAdultos: data.msg[item].num_adultos,
                            NumNinios: data.msg[item].num_ninios,
                            Fecha_realizado: data.msg[item].fecha_realizado + ' <br> ' + data.msg[item].hora_realizado,
                            Actualizado: data.msg[item].updated_at,
                            Descripcion: data.msg[item].descripcion,
                            Opciones: opcionesTerminadas(data.msg[item].id, data.msg[item].estado)
                        };
                        json.push(itemJson)
                    }
                    $('#card-reuniones').removeClass("is-loading");
                    return json;
                }
            },
            columns: [
                {data: "Nombre"},
                {data: "NumAdultos"},
                {data: "NumNinios"},
                {data: "Fecha_realizado"},
                {data: "Actualizado"},
                {data: "Opciones"},
            ],
        });

        var TABLA_ASISTENTES = $('#tabla-asistentes').DataTable({
            scrollY: 400,
            scrollCollapse: true,
            autofill: true,
            "ajax": {
                "url": "recurso/asistentesReunion",
                "type": "POST",
                "data": function (d) {
                    d.id = ID_REUNION;
                    d._token = $('meta[name="csrf-token"]').attr('content')
                },
                "dataSrc": function (data) {

                    var json = [];
                    console.log(data);
                    for (var item in data.msg) {

                        var itemJson = {
                            id: data.msg[item].id,
                            Nombre: data.msg[item].nombre + " " + data.msg[item].apellido,
                            Cedula: data.msg[item].cedula,
                            Agregado: data.msg[item].creado,
                            Opciones: opcionesAsistentes(data.msg[item].id, data.msg[item].estado)
                        };
                        json.push(itemJson)
                    }
                    $('#card-reuniones').removeClass("is-loading");
                    $('#card-miembros').removeClass('is-loading is-loading-lg');

                    return json;
                }
            },
            columns: [
                {data: "Nombre"},
                {data: "Cedula"},
                {data: "Agregado"},
                {data: "Opciones"},
            ],
        });

        var TABLA_ASISTENTES_TERMINADO = $('#modal-tabla-asistentes').DataTable({
            scrollY: 400,
            scrollCollapse: true,
            autofill: true,
            "ajax": {
                "url": "recurso/asistentesReunion",
                "type": "POST",
                "data": function (d) {
                    d.id = ID_REUNION_TERMINADO;
                    d._token = $('meta[name="csrf-token"]').attr('content')
                },
                "dataSrc": function (data) {

                    var json = [];
                    console.log(data);
                    for (var item in data.msg) {

                        var itemJson = {
                            id: data.msg[item].id,
                            Nombre: data.msg[item].nombre + " " + data.msg[item].apellido,
                            Cedula: data.msg[item].cedula,
                            Agregado: data.msg[item].creado,
                            Opciones: opcionesAsistentes(data.msg[item].id, data.msg[item].estado)
                        };
                        json.push(itemJson)
                    }
                    $('#card-reuniones').removeClass("is-loading");

                    return json;
                }
            },
            columns: [
                {data: "Nombre"},
                {data: "Cedula"},
            ],
        });

        // var TABLA_REUNIONES_OLD = $('#tablaReunionesAntiguas').DataTable({
        //     scrollY: 400,
        //     scrollCollapse: true,
        //     autofill: true,
        //     "ajax": {
        //         "url": "recurso/getReunionesOld",
        //         "type": "GET",
        //         "dataSrc": function (data) {
        //             $('#card-reuniones').addClass("is-loading");
        //             var json = [];
        //             console.log(data);
        //             for (var item in data.msg) {
        //                 var itemJson = {
        //                     Id: data.msg[item].id,
        //                     Nombre: data.msg[item].rep_date,
        //                     Tipo: data.msg[item].type,
        //                     NumAdultos: data.msg[item].num_adult,
        //                     NumNinios: data.msg[item].num_child,
        //                     Fecha_realizado: data.msg[item].reg_date,
        //                     income: data.msg[item].income,
        //                     expense: data.msg[item].expense,
        //                     Estado: 'Terminado',
        //                     Nota: data.msg[item].notes,
        //                 };
        //                 json.push(itemJson)
        //             }
        //             $('#card-reuniones').removeClass("is-loading");
        //             return json;
        //         }
        //     },
        //     columns: [
        //         {data: "Nombre"},
        //         {data: "Tipo"},
        //         {data: "NumAdultos"},
        //         {data: "NumNinios"},
        //         {data: "Fecha_realizado"},
        //         {data: "income"},
        //         {data: "expense"},
        //     ],
        // });

        function opciones(id, estado) {
            var opciones = '';

            @can('editar.reuniones')

                opciones += '<button type="button" class="btn btn-primary btn-xs editar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Actualizar" data-original-title="Edit"' +
                '           style="margin-right: 5px;">\n' +
                '           <i class="fas fa-pen"></i>\n' +
                ' </button>';
            @endcan

                @can('agregar.asistente')
                opciones += '<button type="button" class="btn btn-primary btn-xs agregarAsistentes" ' +
                '           data-toggle="tooltip" data-placement="top" title="Agregar Miembros" data-original-title="Edit"' +
                '           style="margin-right: 5px;">\n' +
                '           <i class="fas fa-user-plus"></i>\n' +
                ' </button>';
            @endcan
                @can('terminar.reuniones')
                opciones += '<button type="button" class="btn btn-primary btn-xs terminarReunion" ' +
                '           data-toggle="tooltip" data-placement="top" title="Terminar Reuniones" data-original-title="Edit"' +
                '           style="margin-right: 5px;">\n' +
                '           <i class="fas fas fa-check"></i>\n' +
                ' </button>';
            @endcan

            //     can('eliminar.reuniones')
            //     opciones += '' +
            //     '<button type="button" class="btn btn-danger btn-xs eliminarReunion" ' +
            //     '           data-toggle="tooltip" data-placement="top" title="Deshabilitar" data-original-title="Edit">' +
            //     '           <i class="fas fa-trash"></i>\n' +
            //     ' </button>';
            //     endcan
            return opciones;
        }

        function opcionesTerminadas(id, estado) {
            var opciones = '';

            @can('ver.reuniones')
                opciones += '<button type="button" class="btn btn-primary btn-xs verReuniones" ' +
                '           data-toggle="tooltip" data-placement="top" title="Actualizar" data-original-title="Edit"' +
                '           style="margin-right: 5px;">\n' +
                '           <i class="fas fa-eye"></i>\n' +
                ' </button>';
            @endcan
                @can('cambiarEstado.reuniones')
                opciones += '<button type="button" class="btn btn-primary btn-xs terminarReunion" ' +
                '           data-toggle="tooltip" data-placement="top" title="Cambiar a Pendiente" data-original-title="Edit"' +
                '           style="margin-right: 5px;">\n' +
                '           <i class="fas fas fa-backspace"></i>\n' +
                ' </button>';
            @endcan

            /*@ can('eliminar.reuniones')
            if (estado === 0) {
                opciones += '' +
                    '<button type="button" class="btn btn-danger btn-xs cambiarEstado" ' +
                    '           data-toggle="tooltip" data-placement="top" title="Deshabilitar" data-original-title="Edit">' +
                    '           <i class="fas fa-trash"></i>\n' +
                    ' </button>';
            } else {
                opciones += '' +
                    '<button type="button" class="btn btn-success btn-xs cambiarEstado" ' +
                    '           data-toggle="tooltip" data-placement="top" title="Habilitar" data-original-title="Edit">' +
                    '           <i class="fas fa-check"></i>\n' +
                    ' </button>';
            }

            @ endcan*/

            return opciones;
        }

        function opcionesAsistentes(id, estado) {
            var opciones = '';

            @can('editar.reuniones')

                opciones += '' +
                '<button type="button" class="btn btn-danger btn-xs eliminarAsistente" ' +
                '           data-toggle="tooltip" data-placement="top" title="Deshabilitar" data-original-title="Edit">' +
                '           <i class="fas fa-trash"></i>\n' +
                ' </button>';

            @endcan

                return opciones;
        }

        TABLA_REUNIONES_TERMINADAS.on('click', '.verReuniones', function () {
            $tr = $(this).closest('tr');
            var data = TABLA_REUNIONES_TERMINADAS.row($tr).data();
            console.log(data);
            ID_REUNION_TERMINADO = data.Id;
            TABLA_ASISTENTES_TERMINADO.ajax.reload();
            $('#modal_nombre').val(data.Nombre);
            $('#modal_tipo').val(data.Tipo);
            $('#modal_fecha_realizado').val(data.Fecha_realizado);
            $('#modal_num_adultos').val(data.NumAdultos);
            $('#modal_num_ninios').val(data.NumNinios);
            $('#modal_descripcion').val(data.Descripcion);
            $('#modal').modal('show');

        });
        @can('cambiarEstado.reuniones')
        TABLA_REUNIONES_TERMINADAS.on('click', '.terminarReunion', function () {
            $tr = $(this).closest('tr');
            var data = TABLA_REUNIONES_TERMINADAS.row($tr).data();

            swal({
                title: 'Estas seguro?',
                text: "Vas a marcar pendiente la reunion " + data.Nombre,
                icon: 'warning',
                buttons: {
                    confirm: {
                        text: 'Si',
                        className: 'btn btn-success'
                    },
                    cancel: {
                        text: 'Cancelar',
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((Delete) => {
                if (Delete) {

                    $.ajax({
                            url: 'recurso/cambiarEstadoReunion',
                            type: 'POST',
                            data: {
                                id: data.Id,
                                _token: $('meta[name="csrf-token"]').attr('content')
                            },

                        }
                    ).done(function (response) {
                        console.log(response);
                        if (response.status == "Error") {
                            //error
                        } else {

                            TABLA_REUNIONES.ajax.reload();
                            TABLA_REUNIONES_TERMINADAS.ajax.reload();
                            swal({
                                title: 'Ok!',
                                text: 'La Reunión se ha cambiado correctamente',
                                icon: 'success',
                                buttons: {
                                    confirm: {
                                        className: 'btn btn-success'
                                    }
                                }
                            });
                        }
                        //return response;
                    }).fail(function (error) {
                        console.log(error);
                    });
                } else {
                    swal.close();
                }
            });

        });
        @endcan

        TABLA_ASISTENTES.on('click', '.eliminarAsistente', function () {
            $tr = $(this).closest('tr');
            var data = TABLA_ASISTENTES.row($tr).data();
            console.log(data);
            $('#card-miembros').addClass('is-loading is-loading-lg');

            $.post(
                "/recurso/eliminarAsistente", {
                    'persona_id': data.id,
                    'reunion_id': ID_REUNION,
                    '_token': $('meta[name="csrf-token"]').attr('content')
                }
            ).done(function (data) {
                console.log(data);
                TABLA_ASISTENTES.ajax.reload();
                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: data.message,
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                $('#card-miembros').removeClass('is-loading is-loading-lg');

            });
        });

        $("#cancelar-registro").on('click', function () {

            $('#card-registro').hide();
            $('#card-reuniones').show();
            $('#card-reuniones').addClass('animated fadeIn');

            $('#reuniones-form')[0].reset();

        });

        TABLA_REUNIONES.on('click', '.ver', function () {

            $tr = $(this).closest('tr');
            var data = TABLA_REUNIONES.row($tr).data();
            console.log(data);

            window.location.href = '{{url('iglesias')}}/' + data.Id;

        });

        @can('crear.reuniones')

        $("#nueva-reuniom").on('click', function () {

            $('#card-reuniones').hide();
            $('#card-registro').show();
            $('#card-registro').addClass('animated fadeIn');
            $('#reuniones-form')[0].reset();
            $('#actualizar').hide();
            $('#eliminar').hide();
            $('#guardar').show();
            $("#error").hide();
            $("#accion-modal").html('Registrar');

        });

        $("#guardar").on('click', function () {

            let formData = new FormData(document.getElementById("reuniones-form"));
            //formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
            let csrfToken = '{{ csrf_token() }}';
            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#guardar").prop('disabled', true);
            $.ajax({
                url: 'recurso/guardarReunion',
                type: 'POST',
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                data: formData,
                processData: false,
                contentType: false,
                cache: false

            }).done(function (response) {
                $('#reuniones-form')[0].reset();
                $('#modal').modal('hide');
                $("#guardar").prop('disabled', false);
                TABLA_REUNIONES.ajax.reload();

                $('#card-registro').hide();
                $('#card-reuniones').show();
                $('#card-reuniones').addClass('animated fadeIn');

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: 'Reunion Creada Correctamente',
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#guardar").prop('disabled', false);
            });


        });

        @endcan

        @can('editar.reuniones')

        TABLA_REUNIONES.on('click', '.editar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA_REUNIONES.row($tr).data();

            $("#actualizar").prop('disabled', false);
            $('#reuniones-form')[0].reset();

            $('#actualizar').show();
            $('#eliminar').hide();
            $('#guardar').hide();
            $("#error").hide();
            $("#accion-modal").html('Editar');

            $('#card-reuniones').hide();
            $('#card-registro').show();
            $('#card-registro').addClass('animated fadeIn');

            ID_IGLESIA = data.Id;

            $("#id").val(data.Id);
            $('#nombre').val(data.Nombre);
            $('#tipo').val(data.Tipo);
            $('#num_adultos').val(data.NumAdultos);
            $('#num_ninios').val(data.NumNinios);
            $('#fecha_realizado').val(data.Fecha_realizado);
            $('#hora_realizado').val(data.Hora_realizado);
            $('#descripcion').val(data.Descripcion);


            CIUDAD_ID = data.Citi_id;

        });

        $("#actualizar").on('click', function () {

            console.log(ID_IGLESIA)

            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#actualizar").prop('disabled', true);
            $.ajax({
                url: "recurso/actualizarReuniones",
                type: 'POST',
                data: {
                    'id': ID_IGLESIA,
                    'nombre': $('#nombre').val(),
                    'tipo': $('#tipo').val(),
                    'num_adultos': $('#num_adultos').val(),
                    'num_ninios': $('#num_ninios').val(),
                    'fecha_realizado': $('#fecha_realizado').val(),
                    'hora_realizado': $('#hora_realizado').val(),
                    'descripcion': $('#descripcion').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
            }).done(function (response) {

                console.log(response)

                TABLA_REUNIONES.ajax.reload();

                $('#card-registro').hide();
                $('#card-reuniones').show();
                $('#card-reuniones').addClass('animated fadeIn');

                $('#reuniones-form')[0].reset();

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.message,
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#actualizar").prop('disabled', false);
            });

        });

        $('#btn-agregar-miembro').on('click', function () {

            $('#card-miembros').addClass('is-loading is-loading-lg');

            $.ajax({
                url: "recurso/agregarAsistente",
                type: 'POST',
                data: {
                    'reunion_id': ID_REUNION,
                    'persona_id': $('#select-miembros').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
            }).done(function (response) {

                console.log(response);

                TABLA_ASISTENTES.ajax.reload();

                $('#select-miembros').val("").trigger('change.select2');

                $.notify({
                    icon: 'flaticon-success',
                    title: response.status === 'success' ? 'Felicidades' : 'Ups',
                    message: response.message,
                }, {
                    type: response.status,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });


                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });

            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#actualizar").prop('disabled', false);
            });


        });

        @endcan

        @can('eliminar.reuniones')

        TABLA_REUNIONES.on('click', '.eliminarReunion', function () {
            $tr = $(this).closest('tr');
            var data = TABLA_REUNIONES.row($tr).data();
            let opcion = "";

            swal({
                title: 'Estas seguro?',
                text: "Vas a eliminar la reunion" + data.Nombre,
                icon: 'warning',
                buttons: {
                    confirm: {
                        text: 'Si, ' + opcion,
                        className: 'btn btn-success'
                    },
                    cancel: {
                        text: 'Cancelar',
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((Delete) => {
                if (Delete) {

                    $.ajax({
                            url: 'recurso/eliminarReunion',
                            type: 'POST',
                            data: {
                                id: data.Id,
                                _token: $('meta[name="csrf-token"]').attr('content')
                            },

                        }
                    ).done(function (response) {
                        console.log(response);
                        if (response.status == "Error") {
                            //error
                        } else {

                            TABLA_REUNIONES.ajax.reload();
                            swal({
                                title: 'Ok!',
                                text: 'La Reunión ha sido eliminada',
                                icon: 'success',
                                buttons: {
                                    confirm: {
                                        className: 'btn btn-success'
                                    }
                                }
                            });
                        }
                        //return response;
                    }).fail(function (error) {

                        console.log(error);

                    });


                } else {
                    swal.close();
                }
            });

        });

        $("#eliminar").on('click', function () {
            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#eliminar").prop('disabled', true);
            $.ajax({
                url: "recurso/cambiarEstadoReunion",
                type: 'POST',
            }).done(function (response) {
                $('#permisos-form')[0].reset();
                $('#modal').modal('hide');
                TABLA_REUNIONES.ajax.reload();

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.message,
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#actualizar").prop('disabled', false);
            });

        });

        @endcan

        @can('terminar.reuniones')
        TABLA_REUNIONES.on('click', '.terminarReunion', function () {
            $tr = $(this).closest('tr');
            var data = TABLA_REUNIONES.row($tr).data();

            swal({
                title: 'Estas seguro?',
                text: "Vas a marcar terminada la reunion " + data.Nombre,
                icon: 'warning',
                buttons: {
                    confirm: {
                        text: 'Si',
                        className: 'btn btn-success'
                    },
                    cancel: {
                        text: 'Cancelar',
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((Delete) => {
                if (Delete) {

                    $.ajax({
                            url: 'recurso/cambiarEstadoReunion',
                            type: 'POST',
                            data: {
                                id: data.Id,
                                _token: $('meta[name="csrf-token"]').attr('content')
                            },

                        }
                    ).done(function (response) {
                        console.log(response);
                        if (response.status == "Error") {
                            //error
                        } else {

                            TABLA_REUNIONES.ajax.reload();
                            TABLA_REUNIONES_TERMINADAS.ajax.reload();
                            swal({
                                title: 'Ok!',
                                text: 'La Reunión se ha termiado correctamente',
                                icon: 'success',
                                buttons: {
                                    confirm: {
                                        className: 'btn btn-success'
                                    }
                                }
                            });
                        }
                        //return response;
                    }).fail(function (error) {
                        console.log(error);
                    });
                } else {
                    swal.close();
                }
            });

        });
        @endcan

        @can('agregar.asistente')

        TABLA_REUNIONES.on('click', '.agregarAsistentes', function () {
            $tr = $(this).closest('tr');
            var data = TABLA_REUNIONES.row($tr).data();

            $('#card-reuniones').hide();
            $('#card-miembros').show();
            $('#card-miembros').addClass('animated fadeIn');

            getMiembrosIglesia()
            getAsistentesReunion(data.Id);

        });

        $("#btn-atras-miembro").on('click', function () {

            $('#card-miembros').hide();
            $('#card-reuniones').show();
            $('#card-reuniones').addClass('animated fadeIn');

            $('#reuniones-form')[0].reset();

        });

        function getMiembrosIglesia() {

            $('#card-miembros').addClass('is-loading is-loading-lg');

            let MIEMBROS = [];
            $.get(
                "/recurso/miembros",
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id == null ? data.msg[item].id_old + ':old' : data.msg[item].id + ':new',
                        text: data.msg[item].nombres + " " + data.msg[item].apellidos
                    };
                    MIEMBROS.push(itemSelect2)
                }

                $("#select-miembros").select2({
                    theme: "bootstrap",
                    allowClear: true,
                    data: MIEMBROS
                });

                $('#card-miembros').removeClass('is-loading is-loading-lg');

            });

        }

        function getAsistentesReunion(id) {

            ID_REUNION = id;
            TABLA_ASISTENTES.ajax.reload();

        }

        @endcan


        //graficos//
        $('#fecha_realizado').datetimepicker({
            format: 'YYYY-MM-DD',
        });

        $('#hora_realizado').datetimepicker({
            format: 'h:mm A',
        });

        $('#tabR1').on('click', function () {
            setTimeout(function () {
                TABLA_REUNIONES.draw();
                console.log('draw');
            }, 500);
        });
        $('#tabR2').on('click', function () {
            setTimeout(function () {
                TABLA_REUNIONES_TERMINADAS.draw();
                console.log('draw');
            }, 500);
        });

        // $('#tabR3').on( 'click', function () {
        //     setTimeout(function(){
        //         TABLA_REUNIONES_OLD.draw();
        //         console.log('draw');
        //     }, 500);
        // } );
    </script>
@endsection
