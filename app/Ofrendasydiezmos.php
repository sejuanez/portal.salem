<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ofrendasydiezmos extends Model
{
    //
    protected $fillable = [
        'descripcion', 'fecha', 'ofrenda', 'diezmo', 'iglesia_id', 'user_id'
    ];

    public function iglesia()
    {
        return $this->belongsTo(Iglesias::class, 'iglesia_id');
    }

    public function registrado()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
