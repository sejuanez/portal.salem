<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Reporte Salem</title>
    <style>
        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #5D6975;
            text-decoration: underline;
        }

        body {
            position: relative;
            width: 21cm;
            height: 29.7cm;
            margin: 0 auto;
            color: #001028;
            background: #FFFFFF;
            font-family: Arial, sans-serif;
            font-size: 12px;
            font-family: Arial;
            width: 100%;
        }

        header {
            padding: 10px 0;
            margin-bottom: 30px;
        }

        #logo {
            text-align: center;
            margin-bottom: 10px;
        }

        #logo img {
            width: 150px;
        }

        h1 {
            border-top: 1px solid #222B54;
            border-bottom: 1px solid #222B54;
            color: #222B54;
            font-size: 2.4em;
            line-height: 1.4em;
            font-weight: normal;
            text-align: center;
            margin: 0 0 20px 0;
            background: url(assets/img/dimension.png);
        }

        #project {
            float: left;
        }

        #project span {
            color: #5D6975;
            text-align: right;
            width: 52px;
            margin-right: 10px;
            display: inline-block;
            font-size: 0.8em;
        }

        #company {
            float: right;
            text-align: right;
        }

        #project div,
        #company div {
            white-space: nowrap;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px;
        }

        table tr:nth-child(2n-1) td {
            background: #F5F5F5;
        }

        table th,
        table td {
            text-align: center;
        }

        table th {
            padding: 5px 20px;
            color: #5D6975;
            border-bottom: 1px solid #C1CED9;
            white-space: nowrap;
            font-weight: normal;
        }

        table .service,
        table .desc {
            text-align: left;
        }

        table td {
            padding: 20px;
            text-align: right;
        }

        table td.service,
        table td.desc {
            vertical-align: top;
        }

        table td.unit,
        table td.qty,
        table td.total {
            font-size: 1.2em;
        }

        table td.grand {
            border-top: 1px solid #5D6975;;
        }

        #notices .notice {
            color: #5D6975;
            font-size: 1.2em;
        }

        footer {
            color: #5D6975;
            width: 100%;
            height: 30px;
            position: absolute;
            bottom: 0;
            border-top: 1px solid #C1CED9;
            padding: 8px 0;
            text-align: center;
        }
    </style>
</head>
<body>
<header class="clearfix">
    <div id="logo">
        <img
            src="https://www.ministerioapostolicosalem.com/wp-content/uploads/2019/10/cropped-LOGO-SALEM_2018-1-245x91.png">
    </div>
    <h1>Iglesias Aliadas</h1>
    <div id="project">
        @if($pais!='null')
            <div><span>PAIS</span>{{$paisR->name}}</div>
        @endif
        @if($departamento!='null')
            <div><span>DEPARTAMENTO</span>{{$departamentoR->name}}</div>
        @endif
        @if($ciudad!='null')
            <div><span>CIUDAD</span>{{$ciudadR->name}}</div>
        @endif
    </div>
</header>
<main>
    <table>
        <thead>
        <tr>
            <th class="service">IGLESIA</th>
            <th class="service">LUGAR</th>
            <th class="desc">CARGO</th>
            <th class="desc">NOMBRE - TELEFONO</th>
        </tr>
        </thead>
        <tbody>
        @foreach($iglesias as $iglesia)
            @if($iglesia->ciudad->state->countries->id==$pais or $pais!='null')
                @if($iglesia->ciudad->state->id==$departamento or $departamento=='null')
                    @if($iglesia->ciudad->id==$ciudad or $ciudad=='null')
                        <tr>
                            <td class="service">{{$iglesia->nombre}} </td>
                            <td class="desc">{{$iglesia->ciudad->state->name}} - {{$iglesia->ciudad->name}}</td>
                            <td class="desc">Pastor</td>
                            <td class="desc">
                                @foreach($iglesia->miembros as $miembro)
                                    @if("Pastor" === $miembro->rol)
                                        {{$miembro->persona->nombres}} {{$miembro->persona->apellidos}}
                                        - {{$miembro->persona->telefono}}
                                    @endif
                                @endforeach
                            </td>
                        </tr>
                    @endif
                @endif
            @endif
        @endforeach
        </tbody>
    </table>
</main>
</body>
</html>
