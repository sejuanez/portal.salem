<?php

namespace App\Http\Controllers;

use App\Visitas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VisitasController extends Controller
{
    //

    public function getVisitasPendientes()
    {
        if (session()->get('id_iglesia', function () {
                return 'default';
            }) != "default") {
            $visitas = Visitas::where('estado', false)->where('iglesia_id', session()->get('id_iglesia'))->get();
        } else {
            $visitas = Visitas::where('estado', false)->get();
        }

        $array = array();
        foreach ($visitas as $visita) {
            if (auth()->user()->id==$visita->coordinador_id or in_array("Super Admin", auth()->user()->getRoleNames()->toArray()) Or in_array("Admin", auth()->user()->getRoleNames()->toArray())) {
                $datos = [
                    'visita' => $visita,
                    'coordinador' => $visita->coordinador->persona,
                    'coordinador_id' => $visita->coordinador_id,
                    'iglesia' => $visita->iglesia->nombre,
                    'fecha' => $visita->fecha_visita,
                ];
                array_push($array, $datos);
            }
        }

        $response = array(
            'status' => 'success',
            'msg' => $array
        );

        return response()->json($response);
    }

    public function getVisitasTerminadas()
    {
        if (session()->get('id_iglesia', function () {
                return 'default';
            }) != "default") {
            $visitas = Visitas::where('estado', true)->where('iglesia_id', session()->get('id_iglesia'))->get();

        } else {
            $visitas = Visitas::where('estado', true)->get();
        }

        $array = array();
        foreach ($visitas as $visita) {
            if (auth()->user()->id==$visita->coordinador_id or in_array("Super Admin", auth()->user()->getRoleNames()->toArray()) Or in_array("Admin", auth()->user()->getRoleNames()->toArray())) {
                $datos = [
                    'visita' => $visita,
                    'coordinador' => $visita->coordinador->persona,
                    'coordinador_id' => $visita->coordinador_id,
                    'iglesia' => $visita->iglesia->nombre,
                    'fecha' => $visita->fecha_visita,
                ];
                array_push($array, $datos);
            }
        }

        $response = array(
            'status' => 'success',
            'msg' => $array
        );

        return response()->json($response);
    }

    public function crearVisita(Request $request)
    {

        $this->validate($request, [
            'select_coordinador' => 'required',
            'fecha_realizado' => 'required',
        ], [
            'select_coordinador.required' => 'Seleccione un cordinador',
            'fecha_realizado.required' => 'Seleccione la fecha de la visita',
        ]);
        $visita = Visitas::create([
            'coordinador_id' => $request->select_coordinador,
            'fecha_visita' => $request->fecha_realizado,
            'iglesia_id' => session()->get('id_iglesia'),

        ]);

        $response = array(
            'status' => 'success',
            'msg' => $visita,
        );
        return response()->json($response);

    }

    public function guardarVisita(Request $request)
    {
        $visita = Visitas::find($request->id);
        $visita->update($request->all());
        $visita->hora_inicio = date('Ymd H:i:s');
        $visita->save();
        $response = array(
            'status' => 'success'
        );
        return response()->json($response);
    }

    public function eliminarVisita(Request $request)
    {
        $visita = Visitas::find($request->id);
        if ($visita->estado == false) {
            $visita->delete();
            $response = array(
                'status' => 'success',
                'msg' => 'Visita eliminada con exito'
            );
        }
        return response()->json($response);
    }

    public function terminarVisita(Request $request)
    {
        $visita = Visitas::find($request->id);
        $visita->estado = true;
        $visita->hora_fin = date('Ymd H:i:s');
        $visita->save();
        $response = array(
            'status' => 'success'
        );
        return response()->json($response);
    }

    public function cambioPendienteVisita(Request $request)
    {
        $visita = Visitas::find($request->id);
        $visita->estado = false;
        $visita->save();
        $response = array(
            'status' => 'success'
        );
        return response()->json($response);
    }

    public function actualizarVisita(Request $request)
    {
        $visita = Visitas::find($request->id);
        if ($visita->estado == false) {
            $visita->update([
                'coordinador_id' => $request->select_coordinador,
                'fecha_visita' => $request->fecha_realizado,
            ]);
        }
        $response = array(
            'status' => 'success',
            'msg' => 'Visita actualizada con exito'
        );
        return response()->json($response);
    }
}
