<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Miembros extends Model
{
    //
    protected $fillable = [
        'persona_id',
        'iglesia_id',
        'rol'
    ];

    public function persona()
    {
        return $this->belongsTo(Personas::class, 'persona_id');
    }

    public function iglesia()
    {
        return $this->belongsTo(Iglesias::class, 'iglesia_id');
    }
}
