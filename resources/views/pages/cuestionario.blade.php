@extends('layouts.dashboard')

@section('content')
    @php
        $contador=0;
        $preguntas=['limpio',
                'cartelera',
                'alfoli',
                'host_atten',
                'host_people_loc',
                'music',
                'dance',
                'distr',
                'music_love',
                'music_rep',
                'bible_use',
                'reflex',
                'pray_time',
                'leader_vision',
                'balance',
                'unction_preach',
                'jesus_center',
                'pulpit_admin',
                'preach_ability',
                'god_depend',
                'leader_love_work',
                'personal_presentation',
                'op_sex_behavior',
                'child_site',
                'child_atten',
                'teacher_prep',
                'minis_unction',
                'minis_music',
                'minis_child',
                'leader_atten',
                'people_close_to_leader',
                'spouse_behavior',
                'subjection',
                'intersec',
                'inversiones',
                'sat_assist',
                'inicio',
                'alabanza',
                'oracion',
                'predicacion',
                'maestro_ninos',
                'ministracion'];
        foreach ($preguntas as $pregunta){
            if ($visita[$pregunta]!=null){
            $contador++;
            }
        }
        $promedio=round(($contador*100)/42);

    @endphp
    <div class="content container-full">

        <div class="page-navs bg-white">
            <div class="nav-scroller">
                <div
                    class="nav nav-tabs nav-line nav-color-secondary d-flex align-items-center justify-contents-center w-100">
                    <a class="nav-link active show" data-toggle="tab" href="#tab1">Cuestionario
                    </a>
                    <a class="nav-link">Encargado: {{$visita->coordinador->persona->nombres." ".$visita->coordinador->persona->apellidos}}</a>
                    <a class="nav-link">Fecha Visita: {{$visita->fecha_visita}}</a>
                </div>

            </div>
        </div>

        <div class="page-inner">
            <div class="tab-content mt-2 mb-3" id="card-iglesias">
                <div class="tab-pane fade active show" id="tab1" role="tabpanel" aria-labelledby="pills-home-tab">
                    <form id="cuestionario-form">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <h5><b>Progreso de la visita</b></h5>
                                                <p class="text-muted">Todo el proceso de visita completado</p>
                                            </div>
                                            <h3 class="text-{{$promedio<100?"info":'success'}} fw-bold">{{$contador}}
                                                /42</h3>
                                        </div>
                                        <div class="progress progress-sm">
                                            <div class="progress-bar bg-{{$promedio<100?"info":'success'}}"
                                                 role="progressbar" aria-valuenow="{{$promedio}}"
                                                 aria-valuemin="0" aria-valuemax="100"
                                                 style="width: {{$promedio}}%!important;"></div>
                                        </div>
                                        <div class="d-flex justify-content-between mt-2">
                                            <p class="text-muted mb-0">Completado</p>
                                            <p class="text-muted mb-0">{{$promedio}}%</p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="id" name="id" value="{{$visita->id}}">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Aseo en la instalación</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="limpio"
                                                            value="Si, Bueno"
                                                            {{$visita->limpio=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="limpio"
                                                            value="Regular"
                                                            {{$visita->limpio=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="limpio"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->limpio=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="limpio"
                                                            value="No Aplica"
                                                            {{$visita->limpio=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="limpio"
                                                            value="No Evaluado"
                                                            {{$visita->limpio=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Hay cartelera con la información general de Iglesias y grupos familiares del
                                            ministerio</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="cartelera"
                                                            value="Si, Bueno"
                                                            {{$visita->cartelera=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="cartelera"
                                                            value="Regular"
                                                            {{$visita->cartelera=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="cartelera"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->cartelera=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="cartelera"
                                                            value="No Aplica"
                                                            {{$visita->cartelera=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="cartelera"
                                                            value="No Evaluado"
                                                            {{$visita->cartelera=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Hay Alfolí</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="alfoli"
                                                            value="Si, Bueno"
                                                            {{$visita->alfoli=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="alfoli"
                                                            value="Regular"
                                                            {{$visita->alfoli=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="alfoli"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->alfoli=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="alfoli"
                                                            value="No Aplica"
                                                            {{$visita->alfoli=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="alfoli"
                                                            value="No Evaluado"
                                                            {{$visita->alfoli=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>El anfitrión o ujier atiende a la gente con amabilidad</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="host_atten"
                                                            value="Si, Bueno"
                                                            {{$visita->host_atten=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="host_atten"
                                                            value="Regular"
                                                            {{$visita->host_atten=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="host_atten"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->host_atten=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="host_atten"
                                                            value="No Aplica"
                                                            {{$visita->host_atten=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="host_atten"
                                                            value="No Evaluado"
                                                            {{$visita->host_atten=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>El anfitrión o ujier ubica a las personas</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="host_people_loc"
                                                            value="Si, Bueno"
                                                            {{$visita->host_people_loc=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="host_people_loc"
                                                            value="Regular"
                                                            {{$visita->host_people_loc=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="host_people_loc"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->host_people_loc=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="host_people_loc"
                                                            value="No Aplica"
                                                            {{$visita->host_people_loc=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="host_people_loc"
                                                            value="No Evaluado"
                                                            {{$visita->host_people_loc=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Ejecución musical</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="music"
                                                            value="Si, Bueno"
                                                            {{$visita->music=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="music"
                                                            value="Regular"
                                                            {{$visita->music=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="music"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->music=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="music"
                                                            value="No Aplica"
                                                            {{$visita->music=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="music"
                                                            value="No Evaluado"
                                                            {{$visita->music=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Idoneidad danzas</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="dance"
                                                            value="Si, Bueno"
                                                            {{$visita->dance=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="dance"
                                                            value="Regular"
                                                            {{$visita->dance=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="dance"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->dance=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="dance"
                                                            value="No Aplica"
                                                            {{$visita->dance=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="dance"
                                                            value="No Evaluado"
                                                            {{$visita->dance=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Distribución espacial del equipo</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="distr"
                                                            value="Si, Bueno"
                                                            {{$visita->distr=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="distr"
                                                            value="Regular"
                                                            {{$visita->distr=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="distr"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->distr=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="distr"
                                                            value="No Aplica"
                                                            {{$visita->distr=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="distr"
                                                            value="No Evaluado"
                                                            {{$visita->distr=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>disposición para hacer la obra con amor (Alabanza)</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="music_love"
                                                            value="Si, Bueno"
                                                            {{$visita->music_love=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="music_love"
                                                            value="Regular"
                                                            {{$visita->music_love=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="music_love"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->music_love=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="music_love"
                                                            value="No Aplica"
                                                            {{$visita->music_love=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="music_love"
                                                            value="No Evaluado"
                                                            {{$visita->music_love=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Repertorio musical conforme al ministerio</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="music_rep"
                                                            value="Si, Bueno"
                                                            {{$visita->music_rep=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="music_rep"
                                                            value="Regular"
                                                            {{$visita->music_rep=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="music_rep"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->music_rep=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="music_rep"
                                                            value="No Aplica"
                                                            {{$visita->music_rep=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="music_rep"
                                                            value="No Evaluado"
                                                            {{$visita->music_rep=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Al orar usa la Bíblia</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="bible_use"
                                                            value="Si, Bueno"
                                                            {{$visita->bible_use=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="bible_use"
                                                            value="Regular"
                                                            {{$visita->bible_use=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="bible_use"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->bible_use=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="bible_use"
                                                            value="No Aplica"
                                                            {{$visita->bible_use=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="bible_use"
                                                            value="No Evaluado"
                                                            {{$visita->bible_use=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>La reflexión se hace conforme al versículo leído</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="reflex"
                                                            value="Si, Bueno"
                                                            {{$visita->reflex=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="reflex"
                                                            value="Regular"
                                                            {{$visita->reflex=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="reflex"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->reflex=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="reflex"
                                                            value="No Aplica"
                                                            {{$visita->reflex=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="reflex"
                                                            value="No Evaluado"
                                                            {{$visita->reflex=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>El tiempo de oración es adecuado</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="pray_time"
                                                            value="Si, Bueno"
                                                            {{$visita->pray_time=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="pray_time"
                                                            value="Regular"
                                                            {{$visita->pray_time=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="pray_time"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->pray_time=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="pray_time"
                                                            value="No Aplica"
                                                            {{$visita->pray_time=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="pray_time"
                                                            value="No Evaluado"
                                                            {{$visita->pray_time=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Está identificado con la Visión de la Iglesia (Id y hacer discípulos a todas
                                            las naciones)</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="leader_vision"
                                                            value="Si, Bueno"
                                                            {{$visita->leader_vision=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="leader_vision"
                                                            value="Regular"
                                                            {{$visita->leader_vision=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="leader_vision"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->leader_vision=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="leader_vision"
                                                            value="No Aplica"
                                                            {{$visita->leader_vision=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="leader_vision"
                                                            value="No Evaluado"
                                                            {{$visita->leader_vision=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>La predicación es balanceada, enseñanza centrada en la Bíblia, testimonios,
                                            ejémplos Bíblicos, revelación</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="balance"
                                                            value="Si, Bueno"
                                                            {{$visita->balance=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="balance"
                                                            value="Regular"
                                                            {{$visita->balance=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="balance"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->balance=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="balance"
                                                            value="No Aplica"
                                                            {{$visita->balance=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="balance"
                                                            value="No Evaluado"
                                                            {{$visita->balance=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Mueve la unción (Predicación)</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="unction_preach"
                                                            value="Si, Bueno"
                                                            {{$visita->unction_preach=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="unction_preach"
                                                            value="Regular"
                                                            {{$visita->unction_preach=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="unction_preach"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->unction_preach=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="unction_preach"
                                                            value="No Aplica"
                                                            {{$visita->unction_preach=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="unction_preach"
                                                            value="No Evaluado"
                                                            {{$visita->unction_preach=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>El centro es el Señor Jesús</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="jesus_center"
                                                            value="Si, Bueno"
                                                            {{$visita->jesus_center=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="jesus_center"
                                                            value="Regular"
                                                            {{$visita->jesus_center=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="jesus_center"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->jesus_center=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="jesus_center"
                                                            value="No Aplica"
                                                            {{$visita->jesus_center=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="jesus_center"
                                                            value="No Evaluado"
                                                            {{$visita->jesus_center=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Sabe manejar el púlpito y la congregación</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="pulpit_admin"
                                                            value="Si, Bueno"
                                                            {{$visita->pulpit_admin=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="pulpit_admin"
                                                            value="Regular"
                                                            {{$visita->pulpit_admin=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="pulpit_admin"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->pulpit_admin=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="pulpit_admin"
                                                            value="No Aplica"
                                                            {{$visita->pulpit_admin=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="pulpit_admin"
                                                            value="No Evaluado"
                                                            {{$visita->pulpit_admin=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Hay habilidad en la predicación (Dicción y Elocución)</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="preach_ability"
                                                            value="Si, Bueno"
                                                            {{$visita->preach_ability=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="preach_ability"
                                                            value="Regular"
                                                            {{$visita->preach_ability=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="preach_ability"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->preach_ability=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="preach_ability"
                                                            value="No Aplica"
                                                            {{$visita->preach_ability=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="preach_ability"
                                                            value="No Evaluado"
                                                            {{$visita->preach_ability=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Se nota dependencia de Dios y sujeción a las Autoridades</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="god_depend"
                                                            value="Si, Bueno"
                                                            {{$visita->god_depend=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="god_depend"
                                                            value="Regular"
                                                            {{$visita->god_depend=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="god_depend"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->god_depend=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="god_depend"
                                                            value="No Aplica"
                                                            {{$visita->god_depend=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="god_depend"
                                                            value="No Evaluado"
                                                            {{$visita->god_depend=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Hace su labor con amor y diligencia</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="leader_love_work"
                                                            value="Si, Bueno"
                                                            {{$visita->leader_love_work=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="leader_love_work"
                                                            value="Regular"
                                                            {{$visita->leader_love_work=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="leader_love_work"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->leader_love_work=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="leader_love_work"
                                                            value="No Aplica"
                                                            {{$visita->leader_love_work=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="leader_love_work"
                                                            value="No Evaluado"
                                                            {{$visita->leader_love_work=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Presentación personal</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="personal_presentation"
                                                            value="Si, Bueno"
                                                            {{$visita->personal_presentation=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="personal_presentation"
                                                            value="Regular"
                                                            {{$visita->personal_presentation=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="personal_presentation"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->personal_presentation=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="personal_presentation"
                                                            value="No Aplica"
                                                            {{$visita->personal_presentation=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="personal_presentation"
                                                            value="No Evaluado"
                                                            {{$visita->personal_presentation=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Relación con el sexo opuesto</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="op_sex_behavior"
                                                            value="Si, Bueno"
                                                            {{$visita->op_sex_behavior=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="op_sex_behavior"
                                                            value="Regular"
                                                            {{$visita->op_sex_behavior=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="op_sex_behavior"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->op_sex_behavior=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="op_sex_behavior"
                                                            value="No Aplica"
                                                            {{$visita->op_sex_behavior=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="op_sex_behavior"
                                                            value="No Evaluado"
                                                            {{$visita->op_sex_behavior=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Adecuación del sitio (Cuarto niños)</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="child_site"
                                                            value="Si, Bueno"
                                                            {{$visita->child_site=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="child_site"
                                                            value="Regular"
                                                            {{$visita->child_site=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="child_site"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->child_site=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="child_site"
                                                            value="No Aplica"
                                                            {{$visita->child_site=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="child_site"
                                                            value="No Evaluado"
                                                            {{$visita->child_site=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Dinamismo del maestro, atención y participación de los niños</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="child_atten"
                                                            value="Si, Bueno"
                                                            {{$visita->child_atten=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="child_atten"
                                                            value="Regular"
                                                            {{$visita->child_atten=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="child_atten"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->child_atten=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="child_atten"
                                                            value="No Aplica"
                                                            {{$visita->child_atten=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="child_atten"
                                                            value="No Evaluado"
                                                            {{$visita->child_atten=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Preparación del maestro</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="teacher_prep"
                                                            value="Si, Bueno"
                                                            {{$visita->teacher_prep=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="teacher_prep"
                                                            value="Regular"
                                                            {{$visita->limpio=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="teacher_prep"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->limpio=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="teacher_prep"
                                                            value="No Aplica"
                                                            {{$visita->limpio=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="teacher_prep"
                                                            value="No Evaluado"
                                                            {{$visita->limpio=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Unción y respaldo del Espiritu Santo en lo que declara</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="minis_unction"
                                                            value="Si, Bueno"
                                                            {{$visita->minis_unction=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="minis_unction"
                                                            value="Regular"
                                                            {{$visita->minis_unction=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="minis_unction"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->minis_unction=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="minis_unction"
                                                            value="No Aplica"
                                                            {{$visita->minis_unction=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="minis_unction"
                                                            value="No Evaluado"
                                                            {{$visita->minis_unction=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Acompañamiento musical</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="minis_music"
                                                            value="Si, Bueno"
                                                            {{$visita->minis_music=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="minis_music"
                                                            value="Regular"
                                                            {{$visita->minis_music=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="minis_music"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->minis_music=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="minis_music"
                                                            value="No Aplica"
                                                            {{$visita->minis_music=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="minis_music"
                                                            value="No Evaluado"
                                                            {{$visita->minis_music=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Comportamiento niños</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="minis_child"
                                                            value="Si, Bueno"
                                                            {{$visita->minis_child=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="minis_child"
                                                            value="Regular"
                                                            {{$visita->minis_child=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="minis_child"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->minis_child=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="minis_child"
                                                            value="No Aplica"
                                                            {{$visita->minis_child=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="minis_child"
                                                            value="No Evaluado"
                                                            {{$visita->minis_child=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Disposición del Pastor/Líder para atender al pueblo</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="leader_atten"
                                                            value="Si, Bueno"
                                                            {{$visita->leader_atten=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="leader_atten"
                                                            value="Regular"
                                                            {{$visita->leader_atten=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="leader_atten"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->leader_atten=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="leader_atten"
                                                            value="No Aplica"
                                                            {{$visita->leader_atten=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="leader_atten"
                                                            value="No Evaluado"
                                                            {{$visita->leader_atten=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Acercamiento del pueblo al Pastor/Líder al final de la reunión</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="people_close_to_leader"
                                                            value="Si, Bueno"
                                                            {{$visita->people_close_to_leader=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="people_close_to_leader"
                                                            value="Regular"
                                                            {{$visita->people_close_to_leader=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="people_close_to_leader"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->people_close_to_leader=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="people_close_to_leader"
                                                            value="No Aplica"
                                                            {{$visita->people_close_to_leader=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="people_close_to_leader"
                                                            value="No Evaluado"
                                                            {{$visita->people_close_to_leader=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Comportamiento del conyugue frente a la congregación</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="spouse_behavior"
                                                            value="Si, Bueno"
                                                            {{$visita->spouse_behavior=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="spouse_behavior"
                                                            value="Regular"
                                                            {{$visita->spouse_behavior=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="spouse_behavior"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->spouse_behavior=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="spouse_behavior"
                                                            value="No Aplica"
                                                            {{$visita->spouse_behavior=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="spouse_behavior"
                                                            value="No Evaluado"
                                                            {{$visita->spouse_behavior=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Sujeción del equipo de trabajo al Pastor/Lider</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="subjection"
                                                            value="Si, Bueno"
                                                            {{$visita->subjection=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="subjection"
                                                            value="Regular"
                                                            {{$visita->subjection=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="subjection"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->subjection=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="subjection"
                                                            value="No Aplica"
                                                            {{$visita->subjection=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="subjection"
                                                            value="No Evaluado"
                                                            {{$visita->subjection=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Equipo de trabajo en interseción</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="intersec"
                                                            value="Si, Bueno"
                                                            {{$visita->intersec=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="intersec"
                                                            value="Regular"
                                                            {{$visita->intersec=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="intersec"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->intersec=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="intersec"
                                                            value="No Aplica"
                                                            {{$visita->intersec=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="intersec"
                                                            value="No Evaluado"
                                                            {{$visita->intersec=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Inversiones</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="inversiones"
                                                            value="Si, Bueno"
                                                            {{$visita->inversiones=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="inversiones"
                                                            value="Regular"
                                                            {{$visita->inversiones=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="inversiones"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->inversiones=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="inversiones"
                                                            value="No Aplica"
                                                            {{$visita->inversiones=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="inversiones"
                                                            value="No Evaluado"
                                                            {{$visita->inversiones=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Asistencia del grupo de trabajo a discipulado (sábado)</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="sat_assist"
                                                            value="Si, Bueno"
                                                            {{$visita->sat_assist=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="sat_assist"
                                                            value="Regular"
                                                            {{$visita->sat_assist=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="sat_assist"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->sat_assist=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="sat_assist"
                                                            value="No Aplica"
                                                            {{$visita->sat_assist=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="sat_assist"
                                                            value="No Evaluado"
                                                            {{$visita->sat_assist=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Inicio</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="inicio"
                                                            value="Si, Bueno"
                                                            {{$visita->inicio=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="inicio"
                                                            value="Regular"
                                                            {{$visita->inicio=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="inicio"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->inicio=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="inicio"
                                                            value="No Aplica"
                                                            {{$visita->inicio=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="inicio"
                                                            value="No Evaluado"
                                                            {{$visita->inicio=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Alabanza</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="alabanza"
                                                            value="Si, Bueno"
                                                            {{$visita->alabanza=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="alabanza"
                                                            value="Regular"
                                                            {{$visita->alabanza=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="alabanza"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->alabanza=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="alabanza"
                                                            value="No Aplica"
                                                            {{$visita->alabanza=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="alabanza"
                                                            value="No Evaluado"
                                                            {{$visita->alabanza=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Oración</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="oracion"
                                                            value="Si, Bueno"
                                                            {{$visita->oracion=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="oracion"
                                                            value="Regular"
                                                            {{$visita->oracion=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="oracion"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->oracion=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="oracion"
                                                            value="No Aplica"
                                                            {{$visita->oracion=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="oracion"
                                                            value="No Evaluado"
                                                            {{$visita->oracion=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Predicación</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="predicacion"
                                                            value="Si, Bueno"
                                                            {{$visita->predicacion=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="predicacion"
                                                            value="Regular"
                                                            {{$visita->predicacion=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="predicacion"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->predicacion=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="predicacion"
                                                            value="No Aplica"
                                                            {{$visita->predicacion=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="predicacion"
                                                            value="No Evaluado"
                                                            {{$visita->predicacion=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Maestro de niños</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="maestro_ninos"
                                                            value="Si, Bueno"
                                                            {{$visita->maestro_ninos=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="maestro_ninos"
                                                            value="Regular"
                                                            {{$visita->maestro_ninos=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="maestro_ninos"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->maestro_ninos=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="maestro_ninos"
                                                            value="No Aplica"
                                                            {{$visita->maestro_ninos=="No Aplica"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="maestro_ninos"
                                                            value="No Evaluado"
                                                            {{$visita->maestro_ninos=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Ministración</p>
                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="ministracion"
                                                            value="Si, Bueno"
                                                            {{$visita->ministracion=="Si, Bueno"?'checked':''}}>
                                                        <span class="form-radio-sign">Si, Bueno</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="ministracion"
                                                            value="Regular"
                                                            {{$visita->ministracion=="Regular"?'checked':''}}>
                                                        <span class="form-radio-sign">Regular</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="ministracion"
                                                            value="No, Insatisfactorio"
                                                            {{$visita->ministracion=="No, Insatisfactorio"?'checked':''}}>
                                                        <span class="form-radio-sign">No, Insatisfactorio</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="ministracion"
                                                            value="No Evaluado"
                                                            {{$visita->ministracion=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Aplica</span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <label class="form-radio-label ml-3">
                                                        <input
                                                            {{$visita->estado?'disabled':''}} class="form-radio-input"
                                                            type="radio" name="ministracion"
                                                            value="No Evaluado"
                                                            {{$visita->ministracion=="No Evaluado"?'checked':''}}>
                                                        <span class="form-radio-sign">No Evaluado</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Recomendaciones</p>

                                        <div class="form-check">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="comment">Nota:</label>
                                                        <textarea {{$visita->estado?'disabled':''}} class="form-control"
                                                                  id="recomendaciones"
                                                                  name="recomendaciones"
                                                                  rows="3">{{$visita->recomendaciones}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="alert alert-danger text-danger" id="error" style="display: none">
                                        </div>
                                        <div class="wizard-action">
                                            <div class="pull-left">
                                                <button type="button" id="cancelar" class="btn btn-previous btn-danger">
                                                    Salir
                                                </button>
                                            </div>
                                            <div class="pull-right">
                                                @if(!$visita->estado)
                                                    @if($promedio==100)
                                                        <button type="button" id="terminar"
                                                                class="btn btn-finish btn-info">
                                                            Terminar Cuestionario
                                                        </button>
                                                    @else
                                                        <button type="button" id="guardar"
                                                                class="btn btn-finish btn-success">
                                                            Guardar
                                                        </button>
                                                    @endif
                                                @endif
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        @can("realizar.visita")
        @if($promedio==100)
        $('#terminar').on('click', function () {
            var formData = new FormData(document.getElementById("cuestionario-form"));
            console.log(formData);
            $.ajax({
                url: '/recurso/terminarVisita',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                cache: false
            }).done(function (response) {
                if (response.status == "success") {
                    console.log(response);
                    $.notify({
                        icon: 'flaticon-success',
                        title: 'Felicidades',
                        message: 'Cuestionario guardado Correctamente',
                    }, {
                        type: 'success',
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        time: 1000,
                    });
                }
                window.location.href = '{{url('visitas')}}';
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });
            });

        });
        @else

        $('#guardar').on('click', function () {
            var formData = new FormData(document.getElementById("cuestionario-form"));
            console.log(formData);
            $.ajax({
                url: '/recurso/guardarVisita',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                cache: false
            }).done(function (response) {
                if (response.status == "success") {
                    console.log(response);
                    $.notify({
                        icon: 'flaticon-success',
                        title: 'Felicidades',
                        message: 'Cuestionario guardado Correctamente',
                    }, {
                        type: 'success',
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        time: 1000,
                    });
                }
                location.reload();
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });
            });
        });
        @endif
        @endcan
        $('#cancelar').on('click', function () {
            window.location.href = '{{url('visitas')}}';
        });


    </script>
@endsection
