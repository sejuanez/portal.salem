<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Reuniones extends Model
{
    //
    use Notifiable;

    protected $fillable = [
        'nombre',
        'tipo',
        'num_adultos',
        'num_ninios',
        'descripcion',
        'user_id',
        'iglesia_id',
        'fecha_realizado',
        'hora_realizado',
        'estado',
    ];


    public function iglesia()
    {
        return $this->belongsTo(Iglesias::class, 'iglesia_id');
    }

    public function autor()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function miembros()
    {
        return $this->hasMany(Asistentes::class, 'persona_id');
    }
}
