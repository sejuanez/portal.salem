@extends('layouts.dashboard')
@section('content')

    <div class="content container-full">

        <div class="page-navs bg-white">
            <div class="nav-scroller">
                <div
                    class="nav nav-tabs nav-line nav-color-secondary d-flex align-items-center justify-contents-center w-100">
                    <a class="nav-link mr-5 active show" data-toggle="tab" href="#tab1">
                        Estadisticas
                    </a>
                </div>
            </div>
        </div>

        <div class="page-inner">

            <div class="tab-content mt-2 mb-3" id="card-reuniones">
                <div class="tab-pane fade active show" id="tab1" role="tabpanel" aria-labelledby="pills-home-tab">

                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-header-text">Filtros De Busqueda</h5>
                                </div>
                                <div class="card-block">
                                    <ul class="nav nav-pills nav-secondary" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#home3"
                                               role="tab"
                                               id="select_anio">año</a>
                                            <div class="slide"></div>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#profile3"
                                               role="tab"
                                               id="select_mes">Mes</a>
                                            <div class="slide"></div>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#messages3"
                                               role="tab"
                                               id="select_rango">Rango</a>
                                            <div class="slide"></div>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="home3" role="tabpanel">
                                            <div style="overflow:hidden;">
                                                <div class="form-group">
                                                    <div id="datetimepicker12"></div>
                                                </div>
                                            </div>
                                            <div class="card-footer bg-primary">
                                                <div class="rowtext-center">
                                                    <div class="col">
                                                        <div class="title" style="color: white;">Meses del año</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="profile3" role="tabpanel">
                                            <div style="overflow:hidden;">
                                                <div class="form-group">
                                                    <div id="fechaMes"></div>
                                                </div>
                                                <div class="card-footer bg-primary">
                                                    <div class="rowtext-center">
                                                        <div class="col">
                                                            <div class="title" style="color: white;">Semanas del mes
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="messages3" role="tabpanel">
                                            <div style="overflow:hidden;">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="daterange"
                                                           id="rango"/>

                                                </div>
                                                <div class="card-footer bg-primary">
                                                    <div class="rowtext-center">
                                                        <div class="col">
                                                            <div class="title" style="color: white;">Dias
                                                                seleccionados
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-9">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="card-title">Grafica de Asistencia VS Ingresos</div>
                                    </div>
                                    <div class="card-body">
                                        <div class="chart-container" id="grafica5">
                                            <div id="chart-container">Cargando..</div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{--                            <div class="col-md-12">--}}
                            {{--                                <div class="card">--}}
                            {{--                                    <div class="card-header">--}}
                            {{--                                        <div class="card-title">Grafica de Asistencias</div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="card-body">--}}
                            {{--                                        <div class="chart-container" id="grafica1">--}}
                            {{--                                            <canvas id="multipleBarChart"></canvas>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="card-title">Grafica de Asistencias</div>
                                    </div>
                                    <div class="card-body">
                                        <div class="chart-container" id="grafica2">
                                            <canvas id="pieChart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{--                            <div class="col-md-12">--}}
                            {{--                                <div class="card">--}}
                            {{--                                    <div class="card-header">--}}
                            {{--                                        <div class="card-title">Grafica de Ingresos</div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="card-body">--}}
                            {{--                                        <div class="chart-container" id="grafica4">--}}
                            {{--                                            <canvas id="multipleBarChart4"></canvas>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}

                        </div>

                    </div>

                </div>
            </div>

        </div>

    </div>
    @include('pages.modal.reunion')

@endsection

@section('scripts')
    <script type="text/javascript">

        //graficos//

        //cargar numero de reuniones y miembros totales por año
        var NUM_REUNIONES = [];
        var NUM_MIEMBROS = [];
        var NUM_NINOS = 0;
        var NUM_ADULTOS = 0;
        var MESES = [];
        var MESES2 = [];
        var INGRESOS = [];
        var INGRESOS2 = [];

        var INGRESOS_FUSION = [];
        var MIEMBROS_FUSION = [];
        var MESES_FUSION = [];
        $('#datetimepicker12').datetimepicker({
            inline: true,
            sideBySide: true,
            viewMode: 'years',
            format: 'YYYY'
        });

        $('#fechaMes').datetimepicker({
            inline: true,
            sideBySide: true,
            viewMode: 'months',
            format: 'YYYY-MM'
        });

        $('#rango').daterangepicker({
            timePicker: false,
            startDate: moment().startOf('hour'),
            endDate: moment().startOf('hour').add(32, 'hour'),
        }, function (start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            consultarDatosGrafica(null, start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'));
        });

        $("#select_anio").click(function () {
            consultarDatosGrafica($("#datetimepicker12").data("date"), null, null);
        });

        $("#select_mes").click(function () {
            consultarDatosGrafica($("#fechaMes").data("date"), null, null);
        });

        function consultarDatosGrafica(ano, fechaI, fechaF) {
            NUM_REUNIONES = [];
            NUM_MIEMBROS = [];
            MESES = [];
            MESES2 = [];
            INGRESOS = [];
            INGRESOS2 = [];

            MESES_FUSION = [];
            MIEMBROS_FUSION = [];
            INGRESOS_FUSION = [];
            NUM_NINOS = 0;
            NUM_ADULTOS = 0;
            $.get(
                "/recurso/graficaReuniones", {'anio': ano, 'fechaInicio': fechaI, 'fechaFin': fechaF},
            ).done(function (data) {
                console.log(data);
                for (var item in data.msg) {
                    MESES.push(data.msg[item].mes);

                    MESES_FUSION.push({'label': data.msg[item].mes});
                    var sumMiembros = parseInt(data.msg[item].num_adult) + parseInt(data.msg[item].num_child);
                    MIEMBROS_FUSION.push({'value': sumMiembros / data.msg[item].reuniones});

                    NUM_REUNIONES.push(data.msg[item].reuniones);
                    NUM_MIEMBROS.push((parseInt(data.msg[item].num_adult) + parseInt(data.msg[item].num_child)) / data.msg[item].reuniones);
                    INGRESOS.push(data.msg[item].ingresos);
                    INGRESOS_FUSION.push({'value': data.msg[item].ingresos});

                    if (data.msg2 == null) {
                        MESES2.push(data.msg[item].mes);
                        INGRESOS2.push(parseInt(data.msg[item].ingresos));
                    }
                    NUM_NINOS += parseInt(data.msg[item].num_child / data.msg[item].reuniones);
                    NUM_ADULTOS += parseInt(data.msg[item].num_adult / data.msg[item].reuniones);
                }

                for (var item in data.msg2) {
                    if (data.msg2 != null) {
                        MESES2.push(data.msg2[item].mes);
                        INGRESOS2.push(parseInt(data.msg2[item].ingresos));
                    }

                }

                graficar();
            });
        }

        consultarDatosGrafica($("#datetimepicker12").data("date"));

        $('#datetimepicker12').on('dp.change', function (e) {
            console.log($("#datetimepicker12").data("date"));
            consultarDatosGrafica($("#datetimepicker12").data("date"), null, null);
        });

        $('#fechaMes').on('dp.change', function (e) {
            console.log($("#fechaMes").data("date"));
            consultarDatosGrafica($("#fechaMes").data("date"), null, null);
        });

        //graficar

        function graficar() {
            $('#multipleBarChart').remove();
            $('#pieChart').remove();
            $('#multipleBarChart3').remove();
            $('#multipleBarChart4').remove();
            $('#multipleBarChart5').remove();
            $('#grafica1').html('<canvas id="multipleBarChart"></canvas>');
            $('#grafica2').html('<canvas id="pieChart"></canvas>');
            $('#grafica3').html('<canvas id="multipleBarChart3"></canvas>');
            $('#grafica4').html('<canvas id="multipleBarChart4"></canvas>');
            $('#grafica5').html('<div id="chart-container">Cargando..</div>');

            // var multipleBarChart = document.getElementById('multipleBarChart').getContext('2d');
            // var multipleBarChart2 = document.getElementById('multipleBarChart2').getContext('2d');
            //var ctx = document.getElementById('multipleBarChart3').getContext('2d');
            //var multipleLineChart = document.getElementById('multipleBarChart4').getContext('2d');
            var pieChart = document.getElementById('pieChart').getContext('2d');

            // new Chart(multipleBarChart, {
            //     type: 'bar',
            //     data: {
            //         labels: MESES,
            //         datasets: [{
            //             label: "Reuniones",
            //             backgroundColor: '#59d05d',
            //             borderColor: '#59d05d',
            //             data: NUM_REUNIONES,
            //         }, {
            //             label: "Miembros",
            //             backgroundColor: '#fdaf4b',
            //             borderColor: '#fdaf4b',
            //             data: NUM_MIEMBROS,
            //         }],
            //     },
            //     options: {
            //         responsive: true,
            //         maintainAspectRatio: false,
            //         legend: {
            //             position: 'bottom'
            //         },
            //         title: {
            //             display: true,
            //             text: 'Asistencias Anuales'
            //         },
            //         tooltips: {
            //             mode: 'index',
            //             intersect: false
            //         },
            //         responsive: true,
            //         scales: {
            //             xAxes: [{
            //                 stacked: true,
            //             }],
            //             yAxes: [{
            //                 stacked: true
            //             }]
            //         }
            //     }
            // });
            /*
                        new Chart(multipleBarChart2, {
                            type: 'bar',
                            data: {
                                labels: MESES,
                                datasets : [{
                                    label: "Reuniones",
                                    backgroundColor: '#59d05d',
                                    borderColor: '#59d05d',
                                    data: NUM_REUNIONES,
                                }],
                            },
                            options: {
                                responsive: true,
                                maintainAspectRatio: false,
                                legend: {
                                    position : 'bottom'
                                },
                                title: {
                                    display: true,
                                    text: 'Reuniones'
                                },
                                tooltips: {
                                    mode: 'index',
                                    intersect: false
                                },
                                responsive: true,
                                scales: {
                                    xAxes: [{
                                        stacked: true,
                                    }],
                                    yAxes: [{
                                        stacked: true
                                    }]
                                }
                            }
                        });
            */
            // new Chart(multipleLineChart, {
            //     type: 'line',
            //     data: {
            //         labels: MESES2,
            //         datasets: [{
            //             label: "Ingresos",
            //             borderColor: "#1d7af3",
            //             pointBorderColor: "#FFF",
            //             pointBackgroundColor: "#1d7af3",
            //             pointBorderWidth: 2,
            //             pointHoverRadius: 4,
            //             pointHoverBorderWidth: 1,
            //             pointRadius: 4,
            //             backgroundColor: 'transparent',
            //             fill: true,
            //             borderWidth: 2,
            //             data: INGRESOS2
            //         }]
            //     },
            //     options: {
            //         responsive: true,
            //         maintainAspectRatio: false,
            //         legend: {
            //             position: 'top',
            //         },
            //         tooltips: {
            //             bodySpacing: 4,
            //             mode: "nearest",
            //             intersect: 0,
            //             position: "nearest",
            //             xPadding: 10,
            //             yPadding: 10,
            //             caretPadding: 10
            //         },
            //         layout: {
            //             padding: {left: 15, right: 15, top: 15, bottom: 15}
            //         }
            //     }
            // });
            new Chart(pieChart, {
                type: 'pie',
                data: {
                    datasets: [{
                        data: [NUM_ADULTOS, NUM_NINOS],
                        backgroundColor: ["#1d7af3", "#f3545d"],
                        borderWidth: 0
                    }],
                    labels: ['Adultos', 'Niños']
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        position: 'bottom',
                        labels: {
                            fontColor: 'rgb(154, 154, 154)',
                            fontSize: 11,
                            usePointStyle: true,
                            padding: 20
                        }
                    },
                    pieceLabel: {
                        mode: 'value',
                        fontColor: 'white',
                        fontSize: 14,
                    },
                    tooltips: false,
                    layout: {
                        padding: {
                            left: 20,
                            right: 20,
                            top: 20,
                            bottom: 20
                        }
                    }
                }
            });
            //fusion
            FusionCharts.ready(function () {
                var revenueChart = new FusionCharts({
                    type: 'mscombidy2d',
                    renderAt: 'chart-container',
                    width: '700',
                    height: '400',
                    dataFormat: 'json',
                    dataSource: {
                        "chart": {
                            "caption": "Asistencia Vs Ingresos",
                            "subCaption": "",
                            "xAxisname": "",
                            "pYAxisName": "# Miembros",
                            "sYAxisName": "Ingresos $",
                            "numberPrefix": "",
                            "sNumberSuffix": "$",
                            "sYAxisMaxValue": "50",

                            //Cosmetics
                            "divlineAlpha": "1",
                            "divlineColor": "#999999",
                            "divlineThickness": "1",
                            "divLineIsDashed": "1",
                            "divLineDashLen": "1",
                            "divLineGapLen": "1",
                            "usePlotGradientColor": "0",
                            "anchorRadius": "3",
                            "theme": "fusion"
                        },
                        "categories": [{
                            "category": MESES_FUSION
                        }],
                        "dataset": [{
                            "seriesName": "Miembros",
                            "renderAs": "line",
                            "data": MIEMBROS_FUSION
                        },
                            {
                                "seriesName": "Ingresos $",
                                "parentYAxis": "S",
                                "renderAs": "line",
                                "showValues": "0",
                                "data": INGRESOS_FUSION
                            }
                        ]
                    }
                });

                revenueChart.render();
            });
        }

    </script>
@endsection
